<?php
if(!isset($_SESSION)){session_start();}

class UserMenuHelper{
	var $messagesCount;
	var $itemsMessagesCount;
	var $reservationsCount;

	var $notifications;
	
	function __construct(){
		$this->setMessagesCount();
		$this->setItemsMessagesCount();
		$this->setReservationsCount();
		
		$this->notifications = $this->messagesCount + $this->itemsMessagesCount + $this->reservationsCount;
	}
	
	function getUserMenu() {
		if(isset($_SESSION['user_id'])){
			$notifications = "";
			
			if($this->notifications > 0){
				$notifications = "<div id='settings_has_notification'>$this->notifications</div>";
			}
			
			return '<div class="header_top_link"><img src="/lib/images/settings.png" onclick="fadeIn(\'loged_user_menu\')">' . $notifications . "</div>";
		}
	}
	
	function setMessagesCount(){
		if(isUserLogued()){
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			include_once $path.'/controllers/database/connectionManager.php';
	
			$conn = new ConnectionManager();
			$this->messagesCount = getUnrespondedCommentsPerUser($_SESSION["user_id"], $conn->getConnection());
		}
	}
	
	function showMessagesCount(){
		if(isUserLogued()){
			$text = "<div class='header_comms_count'>".$this->messagesCount."</div>";
			return $text;
		}
	}
	
	function setItemsMessagesCount(){
		if(isUserLogued()){
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			include_once $path.'/controllers/database/connectionManager.php';
	
			$conn = new ConnectionManager();
			$this->itemsMessagesCount = getUnrespondedCommentsPerItems($_SESSION["user_id"], $conn->getConnection());
		}
	}
	
	function showItemsMessagesCount(){
		if(isUserLogued()){
			$text = "<div class='header_comms_count'>".$this->itemsMessagesCount."</div>";
			return $text;
		}
	}
	
	function setReservationsCount(){
		if(isUserLogued()){
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			include_once $path.'/controllers/database/connectionManager.php';
	
			$conn = new ConnectionManager();
			$this->reservationsCount = getReservationsCount($_SESSION["user_id"], $conn->getConnection());
		}
	
	}
	
	function showReservationsCount(){
		if(isUserLogued()){
			$text = "<div class='header_comms_count'>". $this->reservationsCount ."</div>";
			return $text;
		}
	}
	
	function getLoguedUserName() {
		if(isset($_SESSION['user_id'])){
			return $_SESSION['user_name'];
		}else{
			return "Ingresar";
		}
	}
}
	
$userMenuHelper = new UserMenuHelper();
?>

<div id="fb-root"></div>
<script type='text/javascript'>
	$(document).ready(function(){
		createAjaxBox();
	});
	initFbApi();
</script>

	<div id="header_search_container">
	<div id="header_search">
	
		<a class="header_top_link" href="/pages/about/">Ayuda</a>
		
		<div style="margin: 0px 90px"><?php echo $userMenuHelper->getUserMenu()?>
			<div class="bubble-down_wrapper" id="loged_user_menu" style="display: none">
				<div class="bubble-down">
					<div id="bubble_content">
						<div id="header_user_menu">
							<a href="/pages/profile/"><div class="header_user_meun_item">Perfil</div></a>
							<a href="/pages/comments/" ><div class="header_user_meun_item">Mensajes <?php echo $userMenuHelper->showMessagesCount()?></div></a>
							
							<div class="user_menu_divi"></div>
							<a href="/pages/newitem/" ><div class="header_user_meun_item">Publicar!</div></a>

							<div class="user_menu_divi"></div>
							<a href="/pages/myitems/" ><div class="header_user_meun_item">Mis publicaciones <?php echo $userMenuHelper->showItemsMessagesCount()?></div></a>
							<a href="/pages/reservations/" ><div class="header_user_meun_item">Reservas <?php echo $userMenuHelper->showReservationsCount()?></div></a>
							
							<div class="user_menu_divi"></div>
							<a href="/controllers/server/profile/loginController.php?event=logout" ><div class="header_user_meun_item"><?php echo $lang_logout?></div></a>
						</div>
					</div>
	  				<div class="bubble-down_arrow_border"></div>
	  				<div class="bubble-down_arrow"></div>
				</div>
			</div>
		</div>
	
	<a class="header_top_link" href="/pages/profile/"><?php echo $userMenuHelper->getLoguedUserName()?></a>
		
	<div class="header_top_link" id="header_logo">
			<a href="/"><lg>Alquilo</lg>Libre!</a>
	</div>
		<div id="header_search_block">
			<input id="search_input_what" name="search_input_what" placeholder="<?php echo $lang_what?>"
			value="<?php echo getUrlVariableValue('search_input_what', '')?>" onkeyup="clickEnter('search_button_search', event)">
			
			<div id="search_button_search" onclick="goSearchWhat()"></div>
		</div>
	</div>
	</div>

<?php 
	printNotice();
?>

