<html>
<div id="results_filters_container">


		<?php
			if(!isset($_REQUEST['mapview'])){
				echo '<div id="map_canvas_wrap"><div id="map_canvas_result"></div></div>';
				echo "<div id='map_bigger' onclick='mapBigger()'>Agrandar mapa</div>";
			}
		
			//Prints the current applied filters
			$province = printFilter('search_filter_provinceId', 'search_filter_provinceName');
			$city = printFilter('search_filter_cityId', 'search_filter_cityName');
			$category = printFilter('search_filter_categoryId', 'search_filter_categoryName');
			
			if($category != false || $province != false || $city != false){
				
				$mainDiv = "<div class='filter_section'> <div class='filter_section_title'>$lang_filteredBy</div>"; 
					$mainDivEnd = "</div>";
				
				echo "$mainDiv $province $city $category $mainDivEnd";
			}

			$location = Array();
			$locationLimit = 0;
			
			if(sizeof($filters->getCities()) > $locationLimit){
				$location = $filters->getCities();
			}
			
			if(sizeof($filters->getProvinces()) > $locationLimit + 1){
				$location = $filters->getProvinces();
			}

			sort($location);
				
			if(sizeof($location) > $locationLimit){
				echo "<div class='filter_section'> <div class='filter_section_title'>$lang_item_location</div>";
				
				foreach ($location as $value) {
					$id = $value->id;
					$name = $value->name;
					$type = $value->type;
					$count = $value->count;
					
					echo "<div class='filter_section_place'>
								<div class=\"filter_section_place_name\" onclick=\"addFilter('$id', '$name', '$type')\">$name</div>
								<div class='filter_section_place_number'> ($count)</div>
						  </div>";
				}

				echo "</div>";
			}
			
			$categories = $filters->getCategories();
			
			if(sizeof($categories) > 1){
				echo "<div class='filter_section'> <div class='filter_section_title'>$lang_categories</div>";

				$currId = @$_REQUEST['search_filter_categoryId'];

				sort($categories);
				
				foreach ($categories as $value) {
					$id = $value->id;
					
					if($id != 38){
						if($id > $currId){
							$name = $value->name;
							$type = $value->type;
							$count = $value->count;
							
							echo "<div class='filter_section_place'>
									<div class=\"filter_section_place_name\" onclick=\"addFilter('$id', '$name', '$type')\">$name</div>
									<div class='filter_section_place_number'>($count)</div>
							      </div>";
						}
					}
				}
					
				echo "</div>";
			}
					
		?>
	
	
	
	<div class="filter_section" id="results_filters_price">
		<div class="filter_section_title"><?php echo $lang_price?></div>
		
		<form class="filter_action" id="filters_action_price" action="">
			<input class="filter_prices_input" id="filter_input_min" name="search_filter_prices_min" placeholder="<?php echo $lang_min?>" 
			value="<?php echo getUrlVariableValue('search_filter_prices_min', '')?>">
			
			<input class="filter_prices_input" id="filter_input_max" name="search_filter_prices_max" placeholder="<?php echo $lang_max?>" 
			value="<?php echo getUrlVariableValue('search_filter_prices_max', '')?>">
			
			<?php echo getCurrentUrlVars('filter_prices')?>
			<input class="filter_prices_input" id="filter_prices_submit" type="submit" value=">">
		</form>
		
	</div>
	
	
	
	
</div>

</html>












