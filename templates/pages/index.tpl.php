<?php 
	$controller = new IndexController('');
?>
<html>

	<div id="index_container">
		<div id="index_top">
			Alquiler de productos y servicios
			<br>
			<div class="button_green"><a class="index_top_link" href="/pages/introductionrent/">¿C&oacute;mo publicar?</a></div>
		</div>
		
		<div id="index_cats">
			<div class="index_section_title">Ubicaci&oacute;n</div>	
			<?php $controller->showCitiesWithItems()?>
			<br>			
			<br>
			<div class="index_section_title">Categorias</div>	
			<?php $controller->showCategoriesWithItems()?>
		</div>
		
		<div id="index_middle">
			<div class="shd"></div>
			
			<div id="index_awesome_container">
				<div class="index_section_title">Art&iacute;culos destacados</div>
				<?php $controller->showBestProducts()?>
			</div>
		</div>

		<div id="index_middle">
			<div class="shd"></div>
			<div id="index_latest_container">
			
				<br>
				<div class="index_section_title"><a class="general_link_big" href="/pages/find/rental/?">Ver m&aacute;s comercios amigos...</a></div>
				<br>
				
				<?php $controller->showLatestRentals()?>
			</div>
		</div>
		
		<div id="index_bottom">
			<div class="shd"></div>
			<div>
				<img alt="" src="/lib/images/pen.png">
				<div id="index_write">Escribinos tus dudas o comentarios</div>
			</div>
			
			<div id="index_bottom_text">
				¿Tenes un comercio?¿Queres hacer algo de dinero extra alquilando algo que no usas seguido?¿Te interesa publicar tus servicios? 
				Escribinos para arreglar una reuni&oacute;n donde podamos contarte de que se trata <b>AlquiloLibre!</b>
			</div>
	
			<div id="index_bottom_form">
				<input id="feedback_email" type="text" name="sender" onclick="fadeIn('index_email_help')" placeholder="email" onblur="checkMail(this.id)">
				<textarea id="feedback_text" rows="10" cols="10" name="text" placeholder="comentario"></textarea>
			</div>
			
			<div id="index_feedback_send" onclick="sendFeedback()">Enviar</div>
			<div id="feedback_done"></div>
		</div>

	</div>
</html>
