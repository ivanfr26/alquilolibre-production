<?php
	$controler = new ProfileController('');
	$rental = $controler->getLogedRentalModel();
	
	$result = getRentalCategories($rental->rental_id, $controler->manager->getConnection());
	
	$aux = mysqli_fetch_array($result);
	$cat_1 = $aux['category_name'];
	
	$aux = mysqli_fetch_array($result);
	$cat_2 = $aux['category_name'];
?>

<html>


<div id="profile_wrapper">

	<div class="profile_block" id="rental_desc" style="float: right; width: 350px;">
		<form id="rental_desc" class="profile_form" action="/controllers/server/profile/profileController.php" method="GET">
		
			<div class="profile_buttons_panel">
				<div class="profile_button_save" onclick="sendFormAjaxly('rental_desc', 'updateRental', ajaxResponseMessage)">Guardar</div>
			</div>
			
			<div class="profile_box_title">Descripci&oacute;n</div>
			
			<div class="profile_block_data">
				<textarea rows="10" cols="38" name="rental_description"><?php echo $rental->rental_description ?></textarea>
			</div>
		</form>
	</div>
	
	
	
	
	
	<div class="profile_block">
		<form id="rental_cats" class="profile_form" action="/controllers/server/profile/profileController.php" method="GET">
		
			<div class="profile_buttons_panel">
				<div class="profile_button_save" onclick="sendFormAjaxly('rental_cats', 'updateRentalCats', ajaxResponseMessage)">Guardar</div>
			</div>
			
			<div class="profile_box_title">Categorias</div>
		
			<div class="profile_block_data">
				<label class="profile_label" for="rental_cat_1">Categor&iacute;a 1</label>
				<select name="rental_cat_1"><?php printCats($cat_1, $controler->manager->getConnection())?></select>
			</div>

			<div class="profile_block_data">
				<label class="profile_label" for="rental_cat_2">Categor&iacute;a 2</label>
				<select name="rental_cat_2"><?php printCats($cat_2, $controler->manager->getConnection())?></select>
			</div>
		</form>
	</div>
	
	
	
	
	
	
	
	
	
	
	<div class="profile_block" id="rental_basicInfo">
		<form id="rental_basicInfo" class="profile_form" action="/controllers/server/profile/profileController.php" method="GET">
		
			<div class="profile_buttons_panel">
				<div class="profile_button_save" onclick="sendFormAjaxly('rental_basicInfo', 'updateRental', ajaxResponseMessage)">Guardar</div>
				<div class="profile_button_save" onclick="editBlock('rental_basicInfo')">Editar &middot; </div>
			</div>
			
			<div class="profile_box_title">Informaci&oacute;n de tu empresa</div>
			
			<div class="profile_block_data">
				<label class="profile_label" for="rental_name"><?php echo $lang_name?></label>
				<input class="profile_input" name="rental_name" value="<?php echo $rental->rental_name ?>" readonly="readonly">
			</div>
			
			<div class="profile_block_data">
				<label class="profile_label" for="rental_hour">Horario de Atenci&oacute;n</label>
				<input class="profile_input" name="rental_hour" value="<?php echo $rental->rental_hour ?>" readonly="readonly">
			</div>
			
			<div class="profile_block_data">
				<label class="profile_label" for="rental_website">Website</label>
				<input class="profile_input" name="rental_website" value="<?php echo $rental->rental_website ?>" readonly="readonly">
			</div>
		</form>
	</div>

</div>

</html>





