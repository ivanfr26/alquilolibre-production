<html>

 	<div id="login_block">
 		<div id="login_notYet">Datos necesarios para crear tu cuenta</div>
 		<br>
 		<br>
 	
		<form id="register_form" action="/controllers/server/profile/loginController.php" method="GET">
	 	
	 		<div class="login_label">Nombre</div>
			<input class="login_input" id="user_name_input" name="user_name" onblur="checkLenght(this.id, 2, 50)">
			
			<div class="login_label">Apellido</div>
			<input class="login_input" id="user_lastname_input" name="user_lastname" onblur="checkLenght(this.id, 2, 50)">
	 	
	 		<div class="login_label">Email</div>
			<input class="login_input" id="user_email_input" name="user_email" onblur="checkMail(this.id)">
				
			<div class="login_label">Contrase&ntilde;a</div>
			<input class="login_input" id="user_password_input" name="user_password" type="password" onkeyup="clickEnter('login_button', event)">

			<br>
			<div class="button_green" id="login_button" onclick="sendFormAjaxly('register_form', 'register', registerHandler)">Crear!</div>
			<br>
			<br>
		</form>
 	</div>
</html>