<html>


<div id="upgrade_top">
Tenemos que verificar algunos datos m&aacute;s antes de continuar.<br> 
</div>

 	<div id="login_block">
 		
		<form id="register_form" action="/controllers/server/profile/upgradeController.php?event=ask" method="POST">
	 		
	 		<div class="login_label">Nombre comercial</div>
			<input class="login_input" name="rental_name">
	 	
	 		<div class="login_label">DNI o CUIT</div>
			<input class="login_input" name="user_dni">
	 	
	 		<div class="login_label">Tel&eacute;fono</div>
			<input class="login_input" name="user_phone">
			
			<div style="margin-left: 80px">
				<!-- PROVINCE -->
					<div class="button_bubble">
						<input id="province_input" value="0" style="display: none;">
						
						<div class="selectmenu_selected" id="province_selectmenu_selected" onclick="fadeIn('province_select')"> 
							<?php echo $lang_item_province?>
						</div>	
						
						<div class="bubble-left_wrapper" id="province_select" style="display: none;">
							<div class="bubble-down" style="top: 30px;">
								<div id="province_select_bubble_content">
				  					<?php echo showProvinces('1')?>
								</div>
			  					
			  					<div class="bubble-down_arrow_border"></div>
			  					<div class="bubble-down_arrow"></div>
							</div>
						</div>
					</div>
					
					<!-- CITY -->
					<div class="button_bubble">
						<input id="city_input" name="city_id" value="0" style="display: none;">
						
						<div class="selectmenu_selected" id="city_selectmenu_selected" onclick="fadeIn('city_select')">
							<?php echo $lang_item_city?>
						</div>
						
						<div class="bubble-left_wrapper" id="city_select" style="display: none;">
							<div class="bubble-down" id="city_select_bubble" style="top: 30px;">
							
								<div id="city_select_bubble_content" style="padding: 10px;">
				  					<?php echo $lang_help_selecteProvinceFirst?>
								</div>
			  					
			  					<div class="bubble-down_arrow_border"></div>
			  					<div class="bubble-down_arrow"></div>
							</div>
						</div>
					</div>
			</div>			
			
			<br><br><br>
			
			<input class="button_green" id="login_button" type="submit" value="Enviar">
		</form>
 	</div>
</html>