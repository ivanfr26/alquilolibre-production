<?php
if(!isset($_SESSION)){
	session_start();
}

	$updateController = new updateController();

	$item = $updateController->getItem();
	
	if($_SESSION['user_id'] != $item->user_id){
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		header("location: /pages/login/?discl=$lang_logInToContinue&ff=/pages/updateitem/?" . getCurrentUrl(''));
		return;
	}
	
	$itemCompostAddress = $item->item_address . ", " . $item->city_name . ", " . $item->province_name . ", Argentina";
?>

<html>

<div id="editItem_wrapper">

	<div id="updateItem_image_scroller">
		<div id="updateItem_imgs_uploader">
			<form action="/controllers/server/uploaderController.php?type=item&event=upload" method="post" enctype="multipart/form-data">
				
				<output id="imgsSelected" name="imgList[]"></output>
			
				<div id="uploadFilesButton" class="button_green" onclick="getFile()"><?php echo $lang_upImgs?></div>
				
				<div id="updateItem_thumbsLine"><?php showImgsEdit(getItemImages($item->item_id, $item->user_id), 'item')?></div>
				
				<!-- Needed fields for the process -->
				<input type="file" id="updateItem_imgFileExplorer" name="files[]" multiple="multiple" class="div_hidden">
				<input id="updateItem_saveImgs" type="submit" name="submit" value="<?php echo $lang_saveImgs?>" class="div_hidden">				
				<input id="item_id" name="item_id" value="<?php echo $item->item_id?>" contenteditable="false" class="div_hidden">
				<input id="img_type" name="img_type" value="item" contenteditable="false" class="div_hidden">
			</form>
			
			<script type="text/javascript">
				document.getElementById('updateItem_imgFileExplorer').addEventListener('change', handleImgsSelection, false);
			</script>
		</div>
	</div>
	

	<form id="new_item_form" name="new_item_form" action="/controllers/server/item/updateController.php?event=updateItemData&item_id=<?php echo $item->item_id?>" method="POST">
		<?php 
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			include_once $path.'templates/pages/itemData.tpl.php';
		?>
	</form>
	
	
	<script type="text/javascript">
		window.onload = function(){
			document.getElementById('updateItem_save').innerHTML = "<?php echo $lang_save?>";
			document.getElementById('updateitem_category_path').style.display="block";
						
			form = document.forms['new_item_form'];

			setInputValue(form.elements['input_item_name'], 	"<?php echo cleanStringJs($item->item_name)?>");
			setInputValue(form.elements['item_description'],  	"<?php echo cleanStringJs($item->item_description)?>");
			setInputValue(form.elements['item_conditions'],  	"<?php echo cleanStringJs($item->item_conditions)?>");
			setInputValue(form.elements['item_price'],  		"<?php echo $item->item_price?>");
			setInputValue(form.elements['item_priceType'],  	"<?php echo $item->item_priceType?>");
			setInputValue(form.elements['item_address'],  		"<?php echo cleanStringJs($item->item_address)?>");
			setInputValue(form.elements['category_id'],  		"<?php echo $item->category_id?>");
			setInputValue(form.elements['city_id'],  			"<?php echo $item->city_id?>");
			setInputValue(form.elements['province_id'],  		"<?php echo $item->province_id?>");
			setInputValue(form.elements['country_id'],  		'0');
			setInputValue(form.elements['item_geolocation'],  	"<?php echo $item->item_geolocation?>");

			setInner('city_selectmenu_selected', 				"<?php echo "$item->city_name"?>");
			setInner('province_selectmenu_selected', 			"<?php echo $item->province_name?>");
			setInner('country_selectmenu_selected', 			'Argentina');

			
			ShowMapGeocode("<?php echo $item->item_geolocation?>","<?php echo $itemCompostAddress?>");
			
		};

    function updateItem_editCategory()
    {
    	document.getElementById('updateitem_category_path').style.display="none";
    	document.getElementById('item_categories_boxes').style.display="block";
    	
    }
		
	</script>	
	
</div>

</html>
















