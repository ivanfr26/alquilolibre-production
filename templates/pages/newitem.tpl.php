<html>

	<div id="editItem_wrapper">
		<form id="new_item_form" name="new_item_form" action="/controllers/server/item/newitemController.php" method="POST">
			<?php 
				$controller = new ProfileController('');			
				$user = $controller->getLogedUserModel();
			
				$itemCompostAddress = $user->user_address . ", " . $user->city_name . ", " . $user->province_name . ", " . $user->country_name;
			
				$path = $_SERVER['DOCUMENT_ROOT'] . "/";
				include_once $path.'templates/pages/itemData.tpl.php';
			?>
		</form>
	</div>

	<script type="text/javascript">
	window.onload = function(){
		document.getElementById('item_categories_boxes').style.display="block";

		form = document.forms['new_item_form'];
		setInputValue(form.elements['item_address'],  "<?php echo $user->user_address?>");
		setInputValue(form.elements['city_id'],  "<?php echo $user->city_id?>");
		setInputValue(form.elements['province_id'],  "<?php echo $user->province_id?>");
		setInputValue(form.elements['country_id'],  '0');
		setInputValue(form.elements['category_id'],  <?php echo CATEGORY_ID_GENERAL_TOP?>);

		setInner('city_selectmenu_selected', "<?php echo "$user->city_name"?>");
		setInner('province_selectmenu_selected', "<?php echo $user->province_name?>");
		setInner('country_selectmenu_selected', 'Argentina');

		codeAddress('<?php echo $user->user_address?>' + ', ' 
			  + '<?php echo $user->city_name?>' + ', ' 
			  + '<?php echo $user->province_name?>' + ', ' 
			  + 'Argentina', 'item');
	};
	</script>
	
</html>




