<?php
	$controler = new ReservationController("");
?>

<html>


<div id="page_container">

	<!-- HISTORIAL -->
	<div style="float: right; max-width: 300px;">
		<div id="reservations_history">Historial de reservas de <price>mis productos</price></div>
		
		
		<div class="block_container" style="border-bottom: none;">
			<div class="block_title">Pagadas</div>
			<?php $controler->showMyItemsReservationsHistory($_SESSION['rental_id'], RESERVATION_PAYED)?>
		</div>
		
		<div class="block_container" style="border-bottom: none;">
			<div class="block_title">Aceptadas</div>
			<?php $controler->showMyItemsReservationsHistory($_SESSION['rental_id'], RESERVATION_ACEPTED)?>
		</div>
		
		<div class="block_container">
			<div class="block_title">Rechazadas</div>
			<?php $controler->showMyItemsReservationsHistory($_SESSION['rental_id'], RESERVATION_REJECTED)?>
		</div>
	</div>
	
	
	
	<!-- MIS PENDIENTES -->
	<div class="block_container" style="max-width: 650px">
		<div class="block_title">Reservas <price>pendientes</price> de mis productos</div>
		<?php $controler->showMyItemsReservations($_SESSION['rental_id'])?>
	</div>
	
	<br><br>
	<div style="max-width: 670px; padding: 0; border-bottom: 1px solid #eee;"></div>
	<div style="max-width: 670px; padding: 0; border-bottom: 1px solid white;"></div>
	<br><br>
	
	<!-- MIS ALQUILERES -->
	<div class="block_container" style="max-width: 650px">
		<div class="block_title">Mis alquileres</div>
		<?php $controler->showMyReservations($_SESSION['user_id'])?>
	</div>
	
</div>

</html>





