
			<div class="updateItem_section">
				<div class="update_section_title"><?php echo $lang_categories?></div>

				<div id="updateitem_category_path"><?php if($item) showCategories($item,0); ?><br><br>
				     <div id="updateItem_editCategory" class="button_green" onclick="updateItem_editCategory();"><?php echo $lang_change?></div>
			    </div>	
				
				<input id="category_id" value="1" name="category_id" style="display: none;">
				
				<div id="item_categories_boxes">
					<?php printCategoriesOneLevelDown(38, 0)?>
				</div>
			</div>
			
	 		<div class="updateItem_section">
				<div class="update_section_title"><?php echo $lang_updateItem_describe?></div>
				
				<div class="item_block_data">
			 		<div class="item_input_label"><b><?php echo $lang_item_name?></b></div>
		 		
					<input class="item_input" id="input_item_name" name="item_name"
					onfocus="fadeIn(this.id + '_bubble_wrapper')" onblur="fadeOut(this.id + '_bubble_wrapper');">
					
					<div class="bubble-left_wrapper" id="item_name_bubble_wrapper">
						<div class="bubble-left" id="item_name_bubble">
		  					<?php echo $lang_help_itemTitle?>
		  					<div class="bubble-left_arrow_border"></div>
		  					<div class="bubble-left_arrow"></div>
						</div>
		 			</div>
				</div>
					
				<div class="item_block_data">
					<div class="item_input_label"><?php echo $lang_item_description?></div>
					<textarea class="item_input" id="item_description" name="item_description"></textarea>
				</div>
				
			</div>
	 	
				
			<div class="updateItem_section">
				<div class="update_section_title"><?php echo $lang_item_conditions?></div>
				
				<div class="item_block_data">
					<div class="item_input_label"><?php echo $lang_item_conditions_label?></div>
					<textarea class="item_input" id="item_conditions" name="item_conditions"
					onfocus="fadeIn(this.id + '_bubble_wrapper')" onblur="fadeOut(this.id + '_bubble_wrapper')"></textarea>
					
					<div class="bubble-left_wrapper" id="item_conditions_bubble_wrapper">
						<div class="bubble-left" id="item_conditions_bubble">
		  					<?php echo $lang_help_itemConditions?>
		  					<div class="bubble-left_arrow_border"></div>
		  					<div class="bubble-left_arrow"></div>
						</div>
					</div>
				</div>
				
				<div class="item_block_data">
					<div class="item_input_label"><b><?php echo "$lang_price" ?></b> $</div> 
					<input class="item_input" id="item_price" name="item_price"  type="text" onkeyup="checkNum(this)">
					
					<select  id="item_priceType" class="item_input" name="item_priceType">
						<option value="0"><?php echo $lang_item_hour?></option>
						<option value="1"><?php echo $lang_item_day?></option>
						<option value="2"><?php echo $lang_item_week?></option>
						<option value="3"><?php echo $lang_item_month?></option>
					</select>
					
				</div>
			</div>
			
			
			<!-- LOCATION SECTION -->
			
			<div class="updateItem_section" id='itemData_section_location'>
				
				<div class="update_section_title"><?php echo $lang_item_location?></div>
				<div class="item_block_data" id="item_block_data_locationButtons">
				
				<div id="itemDelfaultAddressBlock"><?php echo $itemCompostAddress ?><br><br>
				 	<div id="updateItem_editCategory" class="button_green" onclick="showEditLocation();"><?php echo $lang_change?></div>
				</div>
				
				<div id = "itemLocationBlock">
					<!-- COUNTRY -->
					
					<div class="button_bubble">
						<input id="country_input" name="country_id" value="1" style="display: none;">
						<div class="selectmenu_selected" id="country_selectmenu_selected">
							Argentina
						</div>
					</div>
				
					<!-- PROVINCE -->
					<div class="button_bubble">
						<input id="province_input" name="province_id" value="0" style="display: none;">
						<div class="selectmenu_selected" id="province_selectmenu_selected" 
								onclick="fadeIn('province_select')"><?php echo $lang_item_province?>
						</div>	
						
						<div class="bubble-left_wrapper" id="province_select" style="display: none;">
							<div class="bubble-down">
								<div id="province_select_bubble_content">
				  					<?php echo showProvinces('1')?>
								</div>
			  					
			  					<div class="bubble-down_arrow_border"></div>
			  					<div class="bubble-down_arrow"></div>
							</div>
						</div>
					</div>
					
					<!-- CITY -->
					<div class="button_bubble">
						<input id="city_input" name="city_id" value="0" style="display: none;">
						<div class="selectmenu_selected" id="city_selectmenu_selected" 
						onclick="fadeIn('city_select')"><?php echo $lang_item_city?>
						</div>
						
						<div class="bubble-left_wrapper" id="city_select" style="display: none;">
							<div class="bubble-down" id="city_select_bubble">
							
								<div id="city_select_bubble_content">
				  					<?php echo $lang_help_selecteProvinceFirst?>
								</div>
			  					
			  					<div class="bubble-down_arrow_border"></div>
			  					<div class="bubble-down_arrow"></div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
			
				<!-- STREET -->
				<div class="item_block_data" id="item_block_data_street">
					<div class="item_input_label"><?php echo $lang_item_address?></div>
					<input class="item_input" id="item_address" name="item_address" 
					onblur=" 
							codeAddress(this.value + ', ' 
									  + document.getElementById('city_selectmenu_selected').innerHTML + ', ' 
									  + document.getElementById('province_selectmenu_selected').innerHTML + ', ' 
									  + document.getElementById('country_selectmenu_selected').innerHTML, 'item');">
									  
				<input id="item_geolocation" name="item_geolocation" style="display: none;">
				</div>
				
				<div class="item_map" id="map_canvas"></div>
	
			</div>
			
			<div id="updateItem_buttons">
				<div id="updateItem_save" class="button_green" onclick="newItemValidate();"><?php echo $lang_item_create?></div>
			</div>
			
			<!-- Hidden, called after validate -->
			<input id="new_item_form_submit" type="submit" value="submit" style="display: none;">
			
	<script type="text/javascript">


	
	function showEditLocation(){
    	document.getElementById('itemDelfaultAddressBlock').style.display="none";
    	document.getElementById('itemLocationBlock').style.display="block";
    	document.getElementById('item_block_data_street').style.display="block";

	}
	
	
	</script>	
				
