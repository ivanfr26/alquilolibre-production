<?php
 	$itemAddress = "$item->item_address, $item->city_name, $item->province_name";
 	
 	$rentalManager = new UserManager();
 	$rental = $rentalManager->getRental($item->rental_id);
 	
 	$comment = "event=add_question&item_id=$item->item_id";
 	$price = "";
 	
 	if($item->item_price != 0){
 		$price = "$$item->item_price";
 	}
 	
?>

<html>

<div id="item_wrapper">
	<div id="item_category_path"><?php showCategories($item,1)?></div>

	
	<div class="item_block">
		<div class="item_block_data">
			<div class="item_input" id="item_name"><?php echo $item->item_name ?></div>
		</div>
	
		<div class="item_block_data">
			<div class="item_input_price"><price><?php echo $price ?></price> <?php echo getPriceLabel($item->item_priceType, $item->item_price) ?></div>
		</div>

		<div class="item_block_data">
			<div class="item_input" id="item_description"><?php echo $item->item_description ?></div>
		</div>
		
		<div class="item_block_data">
			<div class="item_input"><?php echo $item->item_conditions ?></div>
		</div>
		
		<div id='rental_scores'>
			<div id="item_show_users_comments" onclick='fadeIn("item_user_info_sum")'>Resumen de comentarios</div>
		
			<div class="bubble-down_wrapper" id="item_user_info_sum" style="display: none">
				<div class="bubble-down">
					<div id="bubble_content">
					
						<div class='item_user_commentsCount'><b>Total de comentarios:</b>
							<div class="item_user_commentsCount_num"><?php echo ($item->comments_all)?></div>
						</div>
							
						<div class='item_user_commentsCount'>Positivos:
							<div class="item_user_commentsCount_num"><?php echo $item->comments_good?></div>
						</div>
						
						<div class='item_user_commentsCount'>Negativos:
							<div class="item_user_commentsCount_num"><?php echo $item->comments_bad?></div>
						</div>
						
						<div class='item_user_commentsCount'>Preguntas:
							<div class="item_user_commentsCount_num"><?php echo $item->comments_neutral?></div>
						</div>
					</div>
  					<div class="bubble-down_arrow_border"></div>
  					<div class="bubble-down_arrow"></div>
				</div>
			</div>
		</div>
		
		<br>
		<br>
		
		<div id="item_image_scroller" style="">
			<?php showImgs($imagesArray)?>
		</div>
	</div>

	<div class="item_block" id="item_block_qa">
		<div class="item_block_title">Hace tu reservaci&oacute;n</div>
		<br>
	
		<form action="/controllers/server/item/reservationController.php?event=add&item_id=<?php echo $item->item_id?>&item_name=<?php echo $item->item_name?>&owner_id=<?php echo $item->rental_id?>" method="POST">
			<script>
			  $(function() {
				  $.datepicker.regional['es'] = {
			                closeText: 'Cerrar',
			                prevText: '&#x3c;Ant',
			                nextText: 'Sig&#x3e;',
			                currentText: 'Hoy',
			                monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
			                'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
			                monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
			                'Jul','Ago','Sep','Oct','Nov','Dic'],
			                dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
			                dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
			                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
			                weekHeader: 'Sm',
			                dateFormat: 'dd/mm/yy',
			                firstDay: 1,
			                isRTL: false,
			                showMonthAfterYear: false,
			                yearSuffix: ''};
			        $.datepicker.setDefaults($.datepicker.regional['es']);
				
			    $( "#reservation_from" ).datepicker({ dateFormat: 'yy-mm-dd' });
			    $( "#reservation_to" ).datepicker({ dateFormat: 'yy-mm-dd' });
			  });
			</script>
			<p> Desde <input type="text" id="reservation_from" name="reservation_from"/> 
				Hasta <input type="text" id="reservation_to" name="reservation_to"/>
				
				<input type="submit" value="Reservar" class="button_green">
			</p>
		</form>
		
		<div id="item_reservation_disclaimer">
			Las reservas son un compromiso de compra. 
			<br>AlquiloLibre.com utiliza los servicios de <strong>MercadoPago para procesar los pagos</strong>.
		</div>
	</div>

	
	<div class="item_block" id="item_block_contact">
		<div class="item_block_title"><?php echo $lang_qa?></div>
		<br>
		
		<div id="item_question_makeForm_wrapper">
				<form id="item_question_makeForm" action="/controllers/server/question/questionController.php?<?php echo $comment?>" method="post">
					<textarea id="item_question_questionArea" name="comment_text" placeholder="<?php echo $lang_writeQ?>"></textarea>
					
					<div id="comments_types_box">
						<div class="comment_option_radio"><input type="radio" name="comment_type" value="1" checked="checked">Pregunta</div>
						<div class="comment_option_radio"><input type="radio" name="comment_type" value="0">Comentario positivo</div>
						<div class="comment_option_radio"><input type="radio" name="comment_type" value="2">Queja o reclamo<br></div>
					</div>
					
					<input class="button_green" style="float: right;" type="submit" value="<?php echo $lang_ask?>">
				</form>
			</div>
		
		<?php showAllQuestionsFromItem($item)?>
		<div id="item_question_showMoreQuestions" class="general_link_small"> <?php echo $lang_moreComments ?></div>
	</div>
	
	
	<div class="item_block" id="item_block_contact">
		<div class="item_block_title"><?php echo $lang_item_contact?></div>
		<div class="item_block_data" id="item_block_owner">
		
			<div id="item_owner_wrapper">
			
				<div class='item_owner_data_row' id='item_owner_data'>
					<a class="general_link_big" href="<?php echo getRentalLink($rental->rental_id, $rental->rental_name) ?>"><?php echo $rental->rental_name?></a>
				</div>
				
				<br>
				
				<div class='item_block_data'>
					<div class="small_icon"><img src="/lib/images/phone_icon.png"></div>
					<div ><?php echo $rental->user_phone?></div>
				</div>
		
				<div class='item_block_data'>
					<div class="small_icon"><img src="/lib/images/pin.png"></div>
					<div ><?php echo "$rental->user_address, $rental->city_name"?></div>
				</div>
		
				<div class='item_block_data'>
					<div class="small_icon"><img src="/lib/images/email_black.png"></div>
					<div><?php echo $rental->user_email?></div>
				</div>
				
			</div>
		</div>
	
		<div id="map_canvas" style="width: 100%; height: 170px"></div>
	
		<script type="text/javascript">	
			window.onload = function(){
				//TODO: use only item information once the db is full with data
				ShowMapGeocode("<?php echo $item->item_geolocation?>","<?php echo $itemAddress?>");
		   	};
		 </script>
	</div>
	
	<div class="item_block">
		<div class="item_block_title"><?php echo $lang_moreFromOwner?></div>
		<?php showItemsFromOwner($item->rental_id)?>
	</div>
</div>


<script type="text/javascript">
	var lastShowed = 6;
	var questions = document.getElementsByClassName('item_question_box');
	$("#item_question_showMoreQuestions").click(function () {
		if((lastShowed + 1) < questions.length){
			var i;
			for(i = lastShowed; i < lastShowed + 3; i++){
		  	   $('#item_question_box_' + i).fadeIn(500);
	    	   goToByScroll('item_question_box_' + i);
			}
		   lastShowed = i;
		}else{
			document.getElementById('item_question_showMoreQuestions').innerHTML = "";
		}
	});
</script>
</html>













