<?php
	$controler = new ProfileController('');
	$user = $controler->getLogedUserModel();
?>

<html>
<div id="profile_wrapper">

	<div id="rental_divide"></div>
	<div id="rental_divide_shadow"></div>

	<div class="profile_block" id="profile_block_verif">
		<div style="width: 400px; height: 200px;" id="map_canvas"></div>
	</div>


	<div class="profile_block" id="profile_block_contact">
		<form class="profile_form"  id="user_contactInfo" action="/controllers/server/profile/profileController.php" method="POST">
			<div class="profile_buttons_panel">
				<div class="profile_button_save" onclick="sendFormAjaxly('user_contactInfo', 'updateUser', ajaxResponseMessage)"><?php echo $lang_save?></div>
				<div class="profile_button_save" onclick="editBlock('profile_block_contact')"><?php echo $lang_edit?> &middot; </div>
			</div>
			
			<div class="profile_box_title"><?php echo $lang_item_contact?></div>

			<div class="profile_block_data">
				<label class="profile_label" for="user_phone"><?php echo $lang_phone?></label>
				<input class="profile_input" name="user_phone" value="<?php echo $user->user_phone ?>" readonly="readonly">
			</div>
			
			<div class="profile_block_data">
				<label class="profile_label" for="user_place">Lugar</label>
				<!-- PROVINCE -->
				<div class="button_bubble" style="margin-left: 20px">
					<input id="province_input" value="<?php echo $user->province_id?>" style="display: none;">
					<div class="selectmenu_selected" id="province_selectmenu_selected" onclick="fadeIn('province_select')"><?php
														if(strlen($user->province_name) > 2 ){
															echo $user->province_name;
														}else{
															echo $lang_item_province;
														} 
													?>
					</div>	
					
					<div class="bubble-left_wrapper" id="province_select" style="display: none;">
						<div class="bubble-down">
							<div id="province_select_bubble_content">
			  					<?php echo showProvinces('1')?>
							</div>
		  					
		  					<div class="bubble-down_arrow_border"></div>
		  					<div class="bubble-down_arrow"></div>
						</div>
					</div>
				</div>
				
				<!-- CITY -->
				<div class="button_bubble">
					<input id="city_input" name="city_id" value="<?php echo $user->city_id?>" style="display: none;">
					<div class="selectmenu_selected" id="city_selectmenu_selected" 
					onclick="fadeIn('city_select')"><?php
														if(strlen($user->city_name) > 2 ){
															echo $user->city_name;
														}else{
															echo $lang_item_city;
														} 
													?>
					</div>
					<div class="bubble-left_wrapper" id="city_select" style="display: none;">
						<div class="bubble-down" id="city_select_bubble">
						
							<div id="city_select_bubble_content">
			  					<?php echo $lang_help_selecteProvinceFirst?>
							</div>
		  					
		  					<div class="bubble-down_arrow_border"></div>
		  					<div class="bubble-down_arrow"></div>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="profile_block_data">
				<label class="profile_label" for="user_address"><?php echo $lang_address?></label>
				<input class="profile_input" name="user_address" value="<?php echo $user->user_address ?>" readonly="readonly"
				onkeyup="updateAddressOnFly(this.value, 'user')">
			</div>
			<input id="user_geolocation" name="user_geolocation" style="display: none;">
		</form>
	</div>

	
		<div class="profile_block" id="profile_block_basicinfo">
		
		<form id="user_basicInfo" class="profile_form" action="/controllers/server/profile/profileController.php" method="GET">
		
			<div class="profile_buttons_panel">
				<div class="profile_button_save" onclick="sendFormAjaxly('user_basicInfo', 'updateUser', ajaxResponseMessage)"><?php echo $lang_save?></div>
				<div class="profile_button_save" onclick="editBlock('profile_block_basicinfo')"><?php echo $lang_edit?> &middot; </div>
			</div>
			
			<div class="profile_box_title"><?php echo $lang_basicInfo?></div>
			
			<div class="profile_block_data">
				<label class="profile_label" for="user_name"><?php echo $lang_name?></label>
				<input class="profile_input" name="user_name" value="<?php echo $user->user_name ?>" readonly="readonly">
			</div>
			
			<div class="profile_block_data">
				<label class="profile_label" for="user_lastname"><?php echo $lang_lastname?></label>
				<input class="profile_input" name="user_lastname" value="<?php echo $user->user_lastname ?>" readonly="readonly">
			</div>
			
			
			<div class="profile_block_data">
				<label class="profile_label" for="user_password"><?php echo $lang_password?></label>
				<input class="profile_input" name="user_password" type="password" value="<?php echo $user->user_password ?>" readonly="readonly">
			</div>
		</form>
	</div>

	<br><br><br>
	<div id="rental_divide"></div>
	<div id="rental_divide_shadow"></div>
	
	
	<div id="updateItem_image_scroller">
			<div id="updateItem_imgs_uploader">
				<form method="post" enctype="multipart/form-data">
					
					<output id="imgsSelected" name="imgList[]"></output>
				
					<div id="uploadFilesButton" class="button_green" onclick="getFile()"><?php echo $lang_upImgs?></div>
					
					<div id="updateItem_thumbsLine"><?php showImgsEdit(getUserImages($user->user_id), 'user')?></div>
					
					<!-- Needed fields for the process -->
					<input type="file" id="updateItem_imgFileExplorer" name="files[]" multiple="multiple" class="div_hidden">
					<input id="updateItem_saveImgs" type="submit" name="submit" value="<?php echo $lang_saveImgs?>" class="div_hidden">				
					<input id="item_id" name="item_id" value="<?php echo $item->item_id?>" contenteditable="false" class="div_hidden">
					<input id="img_type" name="img_type" value="user" contenteditable="false" class="div_hidden">
				</form>
				
				<script type="text/javascript">
					document.getElementById('updateItem_imgFileExplorer').addEventListener('change', handleImgsSelection, false);
				</script>
			</div>
		</div>
	
</div>


<script type="text/javascript">	
		window.onload = function(){
			ShowMapGeocode("<?php echo $user->user_geolocation?>","<?php echo $user->user_name?>");
	   	};
</script>
</html>





