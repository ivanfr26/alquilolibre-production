<html>

<div id="item_wrapper">

	<?php echo showRentalCategories($rental->rental_id, $userManager->getConnection())?>

	<div class="item_block">

		<div class="item_block_data">
			<div class="icon_data" id="item_name"><?php echo $rental->rental_name ?></div>
		</div>
	
		<div class="item_block_data">
			<div class="item_block_title" id="item_block_description"><?php echo $lang_item_description?></div>
			<div class="icon_data" id="item_description"><?php echo $rental->rental_description ?></div>
		</div>
		
		<div class="item_block_data">
			<div class="small_icon"><img src="/lib/images/clock.png"></div>
			<div class="icon_data"><?php echo $rental->rental_hour ?></div>
		</div>
		
		<div class='item_block_data'>
			<div class="small_icon"><img src="/lib/images/phone_icon.png"></div>
			<div ><?php echo $rental->user_phone?></div>
		</div>
		
		<div class='item_block_data'>
			<div class="small_icon"><img src="/lib/images/pin.png"></div>
			<div ><?php echo "$rental->user_address, $rental->city_name"?></div>
		</div>
		
		<div class='item_block_data'>
			<div class="small_icon"><img src="/lib/images/email_black.png"></div>
			<div><?php echo $rental->user_email?></div>
		</div>
		
		<div class="item_block_data">
			<div class="small_icon"><img src="/lib/images/golink.png"></div>
			<a class="general_link_small" href="<?php  
			if(strstr($rental->rental_website, 'http://') == false){
				echo "http://";
			} 
			echo $rental->rental_website ?>"><?php echo $rental->rental_website ?></a>
		</div>
	
		<br>
		<br>

		<div id="item_image_scroller">
			<?php showImgs($rentalImagesArray)?>
		</div>
	</div>

	<div class="item_block" id="item_block_qa">
		<div class="item_block_title"><?php echo $lang_qa?></div>
			
			<div id="item_question_makeForm_wrapper">
				<form id="item_question_makeForm" 
				action="/controllers/server/commentsController.php?event=addToUser&userCommented_id=<?php echo $rental->user_id?>" method="post">
					<textarea id="item_question_questionArea" name="text" placeholder="<?php echo $lang_writeQ?>"></textarea>
					
					<div id="comments_types_box">
						<div class="comment_option_radio"><input type="radio" name="type" value="1" checked="checked">Pregunta</div>
						<div class="comment_option_radio"><input type="radio" name="type" value="0">Comentario positivo</div>
						<div class="comment_option_radio"><input type="radio" name="type" value="2">Queja o reclamo<br></div>
					</div>
					
					<input class="button_green" style="float: right;" type="submit" value="<?php echo $lang_ask?>">
				</form>
			</div>		
		
			<?php showCommentsPerUser($rental->user_id)?>
			<div id="item_question_showMoreQuestions" class="general_link_small"> <?php echo $lang_moreComments ?></div>
	</div>
	
	<div class="item_block">
  			<div id="map_canvas" style="width: 100%; height: 170px"></div>
	
		<script type="text/javascript">	
			window.onload = function(){
				//TODO: use only item information once the db is full with data
				ShowMapGeocode("<?php echo $rental->user_geolocation?>","<?php echo $rental->user_name?>");
		   	};
		 </script>
	</div>
	
	<div class="item_block">
		<div class="item_block_title">Art&iacute;culos en alquiler</div>
		<?php showItemsFromOwner($rental->rental_id)?>
	</div>

</div>


<script type="text/javascript">
	var lastShowed = 6;
	var questions = document.getElementsByClassName('item_question_box');
	$("#item_question_showMoreQuestions").click(function () {
		if((lastShowed + 1) < questions.length){
			var i;
			for(i = lastShowed; i < lastShowed + 3; i++){
		  	   $('#item_question_box_' + i).fadeIn(500);
	    	   goToByScroll('item_question_box_' + i);
			}
		   lastShowed = i;
		}else{
			document.getElementById('item_question_showMoreQuestions').innerHTML = "";
		}
	});
</script>
</html>













