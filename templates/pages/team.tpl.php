
<div id="team_wrap">
<div id="team">

	<br><br>	

	<div class='team_box'>
		<img class="team_imgs" src="/lib/images/julian_thumb.jpg">
		<div class="team_data">
			<div class='about_question_title'>Julian Schtutman</div>
			<div class="team_data">CO-FOUNDER</div>
			<div>julian@alquilolibre.com</div>
		</div>
	</div>
	
	<div class='team_box'>
		<img class="team_imgs" src="/lib/images/fede_team.jpg">
		<div class="team_data">
			<div class='about_question_title'>Fede Tula</div>
			<div class="team_data">CO-FOUNDER</div>
			<div>fede@alquilolibre.com</div>
		</div>
	</div>

	<div class='team_box'>
		<img class="team_imgs" src="/lib/images/ivan_thumb.jpg">
		<div class="team_data">
			<div class='about_question_title'>Iván Roth Rolnik</div>
			<div class="team_data">CO-FOUNDER</div>
			<div>ivan@alquilolibre.com</div>
		</div>
	</div>
	
	<br><br><br><br><br><br><br><br><br>
	
	<div class="team_data">
		<div class="footer_link" id="team_video"><a href="http://www.youtube.com/watch?v=sTEzo1Te3sU">Video de presentaci&oacute;n</a></div>
	</div>
	
	<br><br><br>
</div>
</div>
