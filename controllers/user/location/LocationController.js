var categoryLevel = 1;

function showMoreCats(catId, levelBox){
	var xmlhttp = getAjaxCaller();
    
	xmlhttp.onreadystatechange = (function() {
		if (xmlhttp.readyState == 4) {
			
			document.getElementById("category_id").value = catId;

			if(!(levelBox == categoryLevel)){
				removeHigherLevels(levelBox, categoryLevel);
			}
			
			clearBoxSelection(levelBox);
			
			if(xmlhttp.responseText.length > 82){
				addCatBox(xmlhttp.responseText, levelBox);
				categoryLevel++;
			}else{
				//TODO continue button
			}
			
			document.getElementById("category_selection_option_" + catId).setAttribute("class", "category_option_selected");
		}
	});

	xmlhttp.open("GET", "/controllers/server/category/categoryController.php?event=getCats&cat_id=" + catId + "&levelBox=" + levelBox,	true);
	xmlhttp.send();
}

function clearBoxSelection(levelBox){
	var children = document.getElementById("category_box_level_" + levelBox).childNodes;
	
	for ( var i in children) {
		children.item(i).setAttribute("class", "category_selection_option");
	}
}

function removeCat(divNum) {
	  var d = document.getElementById('item_categories_boxes');
	  var olddiv = document.getElementById('category_box_level_' + divNum);
	  if(olddiv != null){
		  d.removeChild(olddiv);
	  }
}

function addCatBox(text, levelBox){
	var boxes = document.getElementById('item_categories_boxes');
	boxes.innerHTML = boxes.innerHTML + text;
}

function removeHigherLevels(levelBox, categoryLevel){
	for ( var i = levelBox+1; i <= categoryLevel; i++) {
		removeCat(i);
	}
	categoryLevel = levelBox;
}

function selected(name, id, type){
	document.getElementById(type + "_input").value = id;
	document.getElementById(type + "_selectmenu_selected").innerHTML = name;
}

/**
 * Updates the location list inside a bubble
 * @param type
 */
function updateLocation(type, id){
	var xmlhttp = getAjaxCaller();
    
	xmlhttp.onreadystatechange = (function() {
		if (xmlhttp.readyState == 4) {
			updateDivContent(type + "_select_bubble_content", xmlhttp.responseText);
		}
	});

	var vars = new Array();
	vars[0] = "event=" + type;
	vars[1] = "id=" + id;

	callAjax(xmlhttp, "/controllers/server/location/locationController.php", vars);
}


/**
 * Calls an object in the backend
 * 
 * @param xmlhttp
 * @param address
 * @param vars
 */
function callAjax(xmlhttp, address, vars){
	var varsText = "";
	for ( var i in vars) {
		varsText += vars[i] + "&";
	}
	
	var fullAddress = address + "?" + varsText;
	
	xmlhttp.open("GET", fullAddress,	true);
	xmlhttp.send();
}

/**
 * Updates the content of a div
 * @param bubble
 * @param inner
 */
function updateDivContent(id, content) {
	var div = document.getElementById(id);
	div.innerHTML = content;
}


/**
 * The XMLHttpRequest object to do AJAX calls.
 * @returns
 */
function getAjaxCaller(){
	if (window.XMLHttpRequest) {// IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    } else {// IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}