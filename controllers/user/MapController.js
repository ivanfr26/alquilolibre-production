var geocoder;
var map;
var markers = new Array();
var address= new Array();
//show the map with a address, calculate geocode and store it for future uses
function ShowMapAddress(type, itemId, address) {
    geocoder = new google.maps.Geocoder();

    if (window.XMLHttpRequest) {// IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    codeAddress(itemId, address, xmlhttp);
}

//show the map using a geo code
function ShowMapGeocode(geocode, address) {
    var latLngArray = geocode.split(",");
    var latlng = new google.maps.LatLng(latLngArray[0], latLngArray[1]); 
    setMap(latlng,address);
}


function updateAddressOnFly(street, type){
	var address = street + "," 
	+ document.getElementById('city_selectmenu_selected').innerHTML + ", " 
	+ document.getElementById('province_selectmenu_selected').innerHTML + ","
	+ "Argentina";
	
	codeAddress(address, type);
}

function codeAddress(address, type) {
	geocoder = new google.maps.Geocoder();
	var lat = '';
	var lng = '';
	geocoder.geocode({
		'address' : address
	}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			
			lat = results[0].geometry.location.lat(); //getting the lat
			lng = results[0].geometry.location.lng(); //getting the lng
			
			map.setCenter(results[0].geometry.location);
			
			setInfoBox(addMarker(results[0].geometry.location), address);
			
			updateGeoInput(lat, lng, type);
			
		} else {
			//TODO: use a default location here
		}
	});
	var latlng = new google.maps.LatLng(lat, lng);
	setMap(latlng);

}

//callback function to update the calculated geocode in the form
function updateGeoInput(lat, lng, type){
	var input = document.getElementById(type + '_geolocation');
	input.value = lat + ", " + lng;
}

function addMarker(pos) {
	var marker = new google.maps.Marker({
		map : map,
		position : pos
	});
	
	return marker;
}
function setMarker(map, pos,address) {
	var marker = new google.maps.Marker({
		map : map,
		position : pos
	});
	
	setInfoBox(marker, address, map);
}

function setInfoBox(marker, message, map) {
	var infowindow = new google.maps.InfoWindow(
	{
		content : message,
		size : new google.maps.Size(1, 1)
	});

	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map, marker);
	});
}

function setMap(pos,address) {
	var myOptions = {
		zoom : 15,
		scrollwheel: false,
		center : pos,
		mapTypeControl : false,
		streetViewControl : false,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	setMarker(map, pos, address);
}

function sendCode(itemId, myCode, xmlhttp) {

	xmlhttp.onreadystatechange = (function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
		}
	});

	b = "b=" + itemId;
	c = "&c=" + myCode;

	xmlhttp.open("GET", "../server/mapsController.php?" + b + c,	true);
	xmlhttp.send();
}



function ShowMapResult(geocodes, address_p) {
	var myOptions = {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false,
			maxZoom: 20
		};
	
	var mapDiv = document.getElementById("map_canvas_result");
	var map = new google.maps.Map(mapDiv, myOptions);
	var fullBounds = new google.maps.LatLngBounds();
    
	for ( var i = 0; i < geocodes.length; i++) {
		var aux = geocodes[i].split(',');
		
		var lat = aux[0];
		var lng = aux[1];
		
		var point=new google.maps.LatLng(lat,lng);
		fullBounds.extend(point);
		
		setMarker(map, point, address_p[i]);
	}
	
	map.fitBounds(fullBounds);
}












