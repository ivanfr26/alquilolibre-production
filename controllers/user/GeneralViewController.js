var KEY_CODE_ENTER = 13;
var KEY_CODE_ESC = 27;

/**
 * Shows the menu if its hiddne, and viceverza.
 * @param menu_id
 */
function showHideMenu(menu_id) {
	var div = document.getElementById(menu_id);

	if(div == null){
		return;
	}
	
	if(div.style.visibility == 'visible'){
		hideMenu(menu_id);
	}else{
		showMenu(menu_id);
	}
}


function clickEnter(divId, event){
	if(event.keyCode == KEY_CODE_ENTER){
		document.getElementById(divId).click();
	}
}



/**
 * Shows a hidden div.
 * TODO give a sliding effect.
 * @param menu_id
 */
function showMenu(menu_id){
	var div = document.getElementById(menu_id);
	if(div == null){
		return;
	}
	div.style.visibility = 'visible';
}

function thumToMainImg(file){
	var mainImg = document.getElementById('thumbs_main_img');
	
	mainImg.src = file;
}

function showUserMenu(){
	fadeIn('header_user_menu');
}


var gid = 0;
function fadeIn(id) {
	$('#' + id).fadeIn(200, function() {
		document.addEventListener('click', close, false);
		gid = id;
	});
}

function close(){
	fadeOut(gid);
	document.removeEventListener('click', close, false);
}


function closeAjaxEsc(event){
	if(event.keyCode == KEY_CODE_ESC){
		closeAjaxBox();
	}
}


function fadeOut(id) {
	$('#' + id).fadeOut(200);
}


function fadeIn2(id) {
	$('#' + id).fadeIn(200);
}

function fadeOut2(id) {
	$('#' + id).fadeOut(200);
}

/**
 * Shows a hidden div.
 * TODO give a sliding effect.
 * @param menu_id
 */
function hideMenu(menu_id){
	var div = document.getElementById(menu_id);
	if(div == null){
		return;
	}
	div.style.visibility = 'hidden';
}


/**
 * Display, undisplay a div
 * @param menu_id
 */
function disUndisDiv(div_id) {
	var div = document.getElementById(div_id);
	
	if(div.style.display == 'block'){
		undisplayDiv(div_id);
	}else{
		displayDiv(div_id);
	}
}

/**
 * Shows a none displayed div 
 * TODO give a sliding effect.
 * @param menu_id
 */
function displayDiv(div_id){
	var div = document.getElementById(div_id);
	div.style.display = 'block';
}

/**
 * Undisplays a div 
 * TODO give a sliding effect.
 * @param menu_id
 */
function undisplayDiv(div_id){
	var div = document.getElementById(div_id);
	div.style.display = 'none';
}

function goToByScroll(id){
	try {
		$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
	} catch (e) {
	}
}

function getThumbId(){
	var src = "updateItem_imgs_thumb_";
	
	for ( var pos = 0; pos < 6; pos++) {
		var img = document.getElementById(src + pos);
		
		if(img.src.indexOf('noimg.png') != -1){
			return pos;
		}
	}
	
	return 0;
}

var IMGS_PER_USER = 6;

function imgsLimit(){
	
	for ( var k = 0; k < IMGS_PER_USER; k++) {
		var imgsrc = document.getElementById("updateItem_imgs_thumb_" + k).src;
		if(imgsrc.indexOf("/lib/images/noimg.png") != -1){
			return k;
		}
	}
	
	return IMGS_PER_USER;
}


function handleImgsSelection() {
	var start = imgsLimit();
	startUpImages(start);
}


function startUpImages(start){
	var form = new FormData();
	form.append("item_id", document.getElementById('item_id').value);
	
	var imgsSelected = document.getElementById('updateItem_imgFileExplorer');
	var size = imgsSelected.files.length;
	
	for ( var i = 0; i < size; i++) {
		setImgSrc("updateItem_imgs_thumb_" + (i + start), '/lib/images/load.gif');
		var imgFile = imgsSelected.files.item(i);
		form.append("img_" + (i + start), imgFile);
	}

	sendFormToBack(form, start);
}


function responseHandler(response){
	ajaxPop(response);
}

function ajaxResponseMessage(response){
	if(verifyResponse(response, 'DATA_OK')){
		ajaxPopOk("Datos guardados correctamente");
	}else{
		ajaxPopFail(response);
	}
}

function loginHandler(response){
	if(verifyResponse(response, "LOGIN_OK")){
		window.location.reload();
	}else{
		console.log(response);
		goUrl('/pages/login/?notice=Ingreso incorrecto, por favor intente nuevamente&nType=Fail');
	}
}

function sendFormAjaxly(formId, event, handler){
	var form = document.forms[formId];
	ajaxPopLoading("Procesando operaci&oacute;n, por favor aguarde...");
	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = (function() {
		
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			handler(xmlhttp.responseText);
		}
	});
	
	var eventStr = '?&event=' + event;
	var uri = form.action + eventStr + elements2String(form);
	
	xmlhttp.open("GET", uri, true);
	xmlhttp.send();
}



function elements2String(form){
	var str = '';
	
	for (var i in form.elements) {
		var name = form.elements[i].name;
		var value = form.elements[i].value;
		
		if(name != undefined && value != undefined){
			str += '&' + name + "=" + value;
		}
	}
	
	return str;
}


function sendAjaxForm(form, event, message){
	ajaxPopLoading("Procesando operaci&oacute;n, por favor aguarde...");
	
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = (function() {
		
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			if(verifyResponse(xmlhttp.responseText, "MAIL_OK")){
				ajaxPop("<div id='ajax_block_wrapper'>" +  message + "</div>");
			}else{
				ajaxPop("<div id='ajax_block_wrapper'>" + xmlhttp.responseText +"</div>");
			}
		}
	});
	
	var vars = '?&event=' + event;
	for ( var i in form.elements) {
		vars += '&' + form.elements[i].name + "=" + form.elements[i].value;
	}
	
	xmlhttp.open("GET", form.action + vars, true);
	xmlhttp.send();
}

function sendFormToBack(form, start){
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = (function() {
		
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			console.log(xmlhttp.responseText);
			
			var imgsArray = xmlhttp.responseText.split(',');
			for ( var o = 0; o < imgsArray.length - 1; o++) {
				setImgSrc("updateItem_imgs_thumb_" + (o + start), imgsArray[o]);
			}
		}
	});
	
	var type = document.getElementById('img_type').value;
	
	xmlhttp.open("post", "/controllers/server/uploaderController.php?event=upload&type=" + type, true);
	xmlhttp.send(form);
}

function setImgSrc(id, src){
	if(src.length > 4){
		document.getElementById(id).src = src;
	}
}

function getFile(){
    document.getElementById("updateItem_imgFileExplorer").click();
}

function deleteImage(uri, img_id, div, type){
	if (window.XMLHttpRequest) {// IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = (function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			setImgSrc(div, '/lib/images/noimg.png');
		}
	});
	
	setImgSrc(div, '/lib/images/load.gif');
	
	xmlhttp.open("GET", "/controllers/server/removerController.php?uri=" + uri + "&img_id=" + img_id + "&type="+type, true);
	xmlhttp.send();
}


function fixScroll(div) {
    $( window ).scroll( function( ){
        var scroller_object = $( "#" + div);
        
        var topL = 120;
        
        if( document.documentElement.scrollTop >= topL || window.pageYOffset >= topL ){
           	scroller_object.css( { position: "fixed", right: "10px", top:"0px"} );
        }else if( document.documentElement.scrollTop < 102 || window.pageYOffset < 102 ){
            scroller_object.css( { position: "relative", right: "10px", top:"0px", margin: "0px" } );
        }
        
    });
}































var indexImages = new Array();
var j = 0;
indexImages[j++] = "/lib/images/index/img_index_tent.jpeg";
indexImages[j++] = "/lib/images/index/img_index_kayak.jpg";
indexImages[j++] = "/lib/images/index/img_index_graps.jpg";
indexImages[j++] = "/lib/images/index/img_index_chain.jpg";
indexImages[j++] = "/lib/images/index/img_index_bike.jpg";
indexImages[j++] = "/lib/images/index/img_index_truck.jpg";
indexImages[j++] = "/lib/images/index/img_index_events.jpg";
indexImages[j++] = "/lib/images/index/img_index_chair.jpg";
indexImages[j++] = "/lib/images/index/img_index_tent.jpeg";
indexImages[j++] = "/lib/images/index/img_index_bride.jpg";

var currentImg = 0;

function showNextImg(){
	currentImg++;
	
	if(currentImg%2 != 0){
		document.getElementById('index_bk_image').style.backgroundImage = 'url("'+indexImages[currentImg]+'")';
		
		$('#index_bk_image2').fadeOut(1000);
		$('#index_bk_image').fadeIn(2000);
		
	}else{
		document.getElementById('index_bk_image2').style.backgroundImage = 'url("'+indexImages[currentImg]+'")';
		
		$('#index_bk_image').fadeOut(1000);
		$('#index_bk_image2').fadeIn(2000);
	}
	
	checkLimit();
}

function checkLimit(){
	if(currentImg == indexImages.length - 1){
		currentImg = 0;
	}
}





function setInner(divId, value){
	try {
		document.getElementById(divId).innerHTML = value;
	} catch (e) {
		console.log(e);
	}
}

function setInputValue(input, value){
	try {
		input.value = value;
	} catch (e) {
		console.log(e);
	}
}







var markers = new Array(); 
var address = new Array();











function initFbApi(){
	  // Additional JS functions here
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '379342498827882', // App ID
	      channelUrl : '//www.alquilolibre/fbChannel.html', // Channel File
	      status     : true, // check login status
	      cookie     : true, // enable cookies to allow the server to access the session
	      xfbml      : true  // parse XFBML
	    });

	    
	    FB.getLoginStatus(function(response) {
	    	  if (response.status === 'connected') {
	    	    // connected
	    	  } else if (response.status === 'not_authorized') {
	    	    // not_authorized
	    	  } else {
	    	    // not_logged_in
	    	  }
	   	});

	    // Additional init code here

	  };

	  // Load the SDK Asynchronously
	  (function(d){
	     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement('script'); js.id = id; js.async = true;
	     js.src = "//connect.facebook.net/en_US/all.js";
	     ref.parentNode.insertBefore(js, ref);
	   }(document));

}

function loginWithFacebook() {
    FB.login(function(response) {
        if (response.authResponse) {
            // connected
        	FB.api('/me', function(user) {
                if (user) {
                	logInFb(user.id, user.first_name, user.last_name, user.email);
                }else{
                	document.write("fb user error");
                }
              });
        } else {
            // cancelled
        	document.write("login cancelled");
        }
    });
}

function fbConnected(){
	FB.api('/me', function(user) {
        if (user) {
			var image = document.getElementById("header_user_img");
	        image.src = 'https://graph.facebook.com/' + user.id + '/picture';
        }
      });
}


function logInFb(user_facebookId, user_firstName, user_lastName, user_email){
	if (window.XMLHttpRequest) {// IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = (function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			if(xmlhttp.responseText.indexOf("LOGIN OK", 0) != -1){
				window.location.reload();
			}
		};
	});
	
	xmlhttp.open("GET", "/controllers/server/profile/loginController.php?event=fblogin&user_name=" + user_firstName
																				   + "&user_lastname=" + user_lastName
																				   + "&user_email=" + user_email
																				   + "&user_facebookId=" + user_facebookId,
																				   true);
	xmlhttp.send();
}

function verifyResponse(response, expected){
	if(response.indexOf(expected, 0) != -1){
		return true;
	}else{
		return false;
	}
}

function logoutFb(){
	FB.logout(function(response) {
		// user is now logged out
	});
}


function sendFeedback(){
	if (window.XMLHttpRequest) {// IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = (function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			if(xmlhttp.responseText.indexOf("MAIL_OK", 0) != -1){
				document.getElementById("feedback_done").innerHTML = 'Muchas Gracias!';
			}
		};
	});
	
	var sender = document.getElementById('feedback_email').value;
	var text = document.getElementById('feedback_text').value;
	
	xmlhttp.open("GET", "/controllers/database/MailManager.php?event=feedback&sender=" + sender
																		  + "&text=" + text,
																				   true);
	
	try {
		document.getElementById("feedback_send").style.display = 'none';
	} catch (e) {
		document.getElementById("index_feedback_send").style.display = 'none';
	}
	document.getElementById("feedback_done").innerHTML = '<img src="/lib/images/load_small.gif">';
	
	xmlhttp.send();
}

function callRegister(){
	callAjaxContent("/templates/pages/register.tpl.php");
}

function registerHandler(response){
	if(verifyResponse(response, "EMAIL_PRESENT")){
		goUrl('/pages/register/?notice=Email ya existe&nType=Fail');
		return;
	}
	
	if(verifyResponse(response, "REGISTER_FAIL")){
		goUrl('/pages/register/?notice=Por favor intenta de nuevo&nType=Fail');
		return;
	}
	
	if(verifyResponse(response, "MAIL_OK")){
		goUrl('/pages/login/?notice=Muchas gracias! Revisa tu correo para activar tu cuenta&nType=Ok');
		return;
	}
}

function callLogin(){
	callAjaxContent("/templates/pages/login.tpl.php");
}

function callAjaxContent(url){
	ajaxPopLoading("Procesando operaci&oacute;n, por favor aguarde...");
	
	xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = (function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			ajaxPop(xmlhttp.responseText);
		};
	});
	
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function createAjaxBox(){
	var div = document.createElement('ajaxbox');
	div.innerHTML = '<div id="ajaxbox"></div>';
	document.body.appendChild(div);
}

function ajaxPopLoading(message){
	ajaxPop("<img src='/lib/images/load_icon.gif'><div id='ajax_processing'>" + message + "</div>");
}

function ajaxPopOk(content){
	ajaxPop("<div class='response_ok'>" + content + "</div>");
}

function ajaxPopFail(content){
	ajaxPop("<div class='response_fail'>" + content + "</div>");
}

function ajaxPop(content){
	document.addEventListener('keyup', closeAjaxEsc, false);
	
	var box = document.getElementById('ajaxbox');
	box.innerHTML = "<div id='ajax_block_wrapper'><div class='box_close' onclick=\"closeAjaxBox()\">x</div>" + content + " </div>";
	$('#main_body').fadeTo("fast", 0.1);
}


function closeAjaxBox(){
	fadeOut2('ajax_block_wrapper');
	$('#main_body').fadeTo("fast", 1.0);
	
	document.removeEventListener('keyup', closeAjaxEsc, false);
}

function goUrl(url){
	window.location.href = url;
}

function mapBigger(){
	var url = getCurrentUrl('', '', '');
	window.location.href = url + "&mapview=true";
}

function mapSmaller(){
	var url = getCurrentUrl('mapview', '', '');
	window.location.href = url;
}


















/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2013 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.8.4
 *
 */
(function($, window, document, undefined) {
    var $window = $(window);

    $.fn.lazyload = function(options) {
        var elements = this;
        var $container;
        var settings = {
            threshold       : 0,
            failure_limit   : 0,
            event           : "scroll",
            effect          : "show",
            container       : window,
            data_attribute  : "original",
            skip_invisible  : true,
            appear          : null,
            load            : null
        };

        function update() {
            var counter = 0;
      
            elements.each(function() {
                var $this = $(this);
                if (settings.skip_invisible && !$this.is(":visible")) {
                    return;
                }
                if ($.abovethetop(this, settings) ||
                    $.leftofbegin(this, settings)) {
                        /* Nothing. */
                } else if (!$.belowthefold(this, settings) &&
                    !$.rightoffold(this, settings)) {
                        $this.trigger("appear");
                        /* if we found an image we'll load, reset the counter */
                        counter = 0;
                } else {
                    if (++counter > settings.failure_limit) {
                        return false;
                    }
                }
            });

        }

        if(options) {
            /* Maintain BC for a couple of versions. */
            if (undefined !== options.failurelimit) {
                options.failure_limit = options.failurelimit; 
                delete options.failurelimit;
            }
            if (undefined !== options.effectspeed) {
                options.effect_speed = options.effectspeed; 
                delete options.effectspeed;
            }

            $.extend(settings, options);
        }

        /* Cache container as jQuery as object. */
        $container = (settings.container === undefined ||
                      settings.container === window) ? $window : $(settings.container);

        /* Fire one scroll event per scroll. Not one scroll event per image. */
        if (0 === settings.event.indexOf("scroll")) {
            $container.bind(settings.event, function(event) {
                return update();
            });
        }

        this.each(function() {
            var self = this;
            var $self = $(self);

            self.loaded = false;

            /* When appear is triggered load original image. */
            $self.one("appear", function() {
                if (!this.loaded) {
                    if (settings.appear) {
                        var elements_left = elements.length;
                        settings.appear.call(self, elements_left, settings);
                    }
                    $("<img />")
                        .bind("load", function() {
                            $self
                                .hide()
                                .attr("src", $self.data(settings.data_attribute))
                                [settings.effect](settings.effect_speed);
                            self.loaded = true;

                            /* Remove image from array so it is not looped next time. */
                            var temp = $.grep(elements, function(element) {
                                return !element.loaded;
                            });
                            elements = $(temp);

                            if (settings.load) {
                                var elements_left = elements.length;
                                settings.load.call(self, elements_left, settings);
                            }
                        })
                        .attr("src", $self.data(settings.data_attribute));
                }
            });

            /* When wanted event is triggered load original image */
            /* by triggering appear.                              */
            if (0 !== settings.event.indexOf("scroll")) {
                $self.bind(settings.event, function(event) {
                    if (!self.loaded) {
                        $self.trigger("appear");
                    }
                });
            }
        });

        /* Check if something appears when window is resized. */
        $window.bind("resize", function(event) {
            update();
        });
              
        /* With IOS5 force loading images when navigating with back button. */
        /* Non optimal workaround. */
        if ((/iphone|ipod|ipad.*os 5/gi).test(navigator.appVersion)) {
            $window.bind("pageshow", function(event) {
                if (event.originalEvent.persisted) {
                    elements.each(function() {
                        $(this).trigger("appear");
                    });
                }
            });
        }

        /* Force initial check if images should appear. */
        $(window).load(function() {
            update();
        });
        
        return this;
    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

    $.belowthefold = function(element, settings) {
        var fold;
        
        if (settings.container === undefined || settings.container === window) {
            fold = $window.height() + $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top + $(settings.container).height();
        }

        return fold <= $(element).offset().top - settings.threshold;
    };
    
    $.rightoffold = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.width() + $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left + $(settings.container).width();
        }

        return fold <= $(element).offset().left - settings.threshold;
    };
        
    $.abovethetop = function(element, settings) {
        var fold;
        
        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top;
        }

        return fold >= $(element).offset().top + settings.threshold  + $(element).height();
    };
    
    $.leftofbegin = function(element, settings) {
        var fold;
        
        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left;
        }

        return fold >= $(element).offset().left + settings.threshold + $(element).width();
    };

    $.inviewport = function(element, settings) {
         return !$.rightoffold(element, settings) && !$.leftofbegin(element, settings) &&
                !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
     };

    /* Custom selectors for your convenience.   */
    /* Use as $("img:below-the-fold").something() or */
    /* $("img").filter(":below-the-fold").something() which is faster */

    $.extend($.expr[':'], {
        "below-the-fold" : function(a) { return $.belowthefold(a, {threshold : 0}); },
        "above-the-top"  : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-screen": function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-screen" : function(a) { return !$.rightoffold(a, {threshold : 0}); },
        "in-viewport"    : function(a) { return $.inviewport(a, {threshold : 0}); },
        /* Maintain BC for couple of versions. */
        "above-the-fold" : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-fold"  : function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-fold"   : function(a) { return !$.rightoffold(a, {threshold : 0}); }
    });

})(jQuery, window, document);


























var dob_regex = /^([0-9]){2}(\/){1}([0-9]){2}(\/)([0-9]){4}$/; // DD/MM/YYYY
var date_regex = /(\d{4}-\d{2}-\d{2} \d{2}:\d{2})/; // 2008-09-01 12:35
var email_regex = /^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/; // email address
var username_regex = /^[\w.-]+$/; // allowed characters: any word . -, ( \w ) represents any word character (letters, digits, and the underscore _ ), equivalent to [a-zA-Z0-9_]
var title_regex = /^[^A-Za-z0-9 ]+/;
var num_regex = /^\d+$/; // numeric digits only
var float_regex = /^-?[0-9]+(\.[0-9]+)?$/;
var search_regex = "/hello/";
var password_regex = /^[A-Za-z\d]{6,8}$/; // any upper/lowercase characters and digits, between 6 to 8 characters in total
var phone_regex = /^\(\d{3]\) \d{3}-\d{4}$/; // (xxx) xxx-xxxx
var question_regex = /\?$/; // ends with a question mark

function setOkLook(div) {
	div.style.borderColor = '#40d74b'; //green
}

function setFailLook(div) {
	div.style.borderColor = '#d74040'; // red
}

function checkNum(div){
	if(num_regex.test(div.value)){
		setOkLook(div);
	}else{
		setFailLook(div);
	}
}

function checkLenght(id, min, max) {
	var div = document.getElementById(id);
	var len = div.value.length;
	
	if (len >= min && len <= max) {
		setOkLook(div);
	}else{
		setFailLook(div);
	}
}

function checkMail(id) {
	var input = document.getElementById(id);
	
	if (email_regex.test(input.value)) {
		setOkLook(input);
	} else {
		setFailLook(input);
	}
}

function checkFloat(id) {
	var input = document.getElementById(id);
	if (input.value.match(float_regex)) {
		setOkLook(input);
	} else {
		setFailLook(input);
	}
}


function newItemValidate() {
//	var form = document.getElementById('new_item_form');
	var submit = document.getElementById('new_item_form_submit');
	submit.click();
	
}


function setInitPoint(reservation_id, item_id, item_name){
	var xmlhttp = new XMLHttpRequest();
	
	
	xmlhttp.onreadystatechange = (function() {
		
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var link = document.getElementById("reserv_mp_" + reservation_id);
			link.href = xmlhttp.responseText;
			
			//http://mp-tools.mlstatic.com/buttons/render.js
			eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('t $4(){$4.57()}1Z $3s!="4e"||($3s={});$4.7c="0.3.5.13";$4.V={3c:K.1P.3c.2n("4b:")>=0?"4b:":"5J:",23:K.7B=="7z",1j:K.1P.3c.2n("4b:")>=0?"4b://7A.5Q.3a/7H-1G/7N/7M/5H/5D/":"5J://53-7n.7m.3a/5H/5D/",5U:K.1P.3c+"//5E.7v.3a/7q/7r.5E.7p.7I",5j:".*("+$3s.5j+"|5Q.3a|8i.8g.3a).*",2M:$3s.2M!=j?$3s.2M:(1Z C.4A!="4e"),3W:8q};$4.u={};$4.57=t(){C.2t==j&&(C.2t=[]);k R=[].8n(3Y.4d(K.5R("1b-1w")),3Y.4d(K.5R("1b-3w")));8(R.14<=0&&C.5C!=U){C.5C=$4.1p.c(C,"5r",$4.57);H}1n(k f=0;f<R.14;f++){8(R[f].40===U){35}R[f].40=1h;R[f].2V=1h;C.2t.2b(R[f])}$4.4u();$4.4V();C.$5B!==U&&$4.6u({"8b":"5r"});C.$5B=U;C.7T=C.2t};$4.4u=t(R,5f){k 2y=1h;C.3H==j&&(C.3H=[]);8(R==j){R=C.2t}J{8(R.14==j){R=[R]}2y=U}1n(k i=0;i<R.14;i++){8(2y){R[i].2V=1h;R[i].Z=j}8(R[i].2V){35}k 11="";8(2y){11=5f!=j?5f:R[i].56}J{11=R[i].11;R[i].56=11}8(11==""){R[i].2V=U;35}8(!/^(-?\\w+)+-?$/.4Y(11)){R[i].2V=U;35}1n(k d 2P $4.5a){k 3t=$4.5a[d];k G=3t.7a(11);8($4.O[d]!=j&&$4.O[d][G]!=j){8(R[i].Z==j){R[i].Z={3r:{},28:{}};R[i].Z.3r[$4.O.1r]="2g";R[i].Z.28[$4.O.1r]="2g"}8(R[i].Z[3t.1F]==j){R[i].Z[3t.1F]={}}R[i].Z.3r[d]=G;R[i].Z[3t.1F][d]=G}}8(R[i].Z!=j){R[i].11="";$4.5y(R[i]);$4.6g(R[i]);$4.6h(R[i])}J{R[i].11=R[i].56}R[i].2V=U}};$4.5y=t(7){8(7.Z.28==j){H}k 11=$4.3E(7,"28");7.11+=7.11==""?11:" "+11};$4.6g=t(7){8(7.Z.q==j){H}k 11=$4.3E(7,"q");7.11+=7.11==""?11:" "+11};$4.6h=t(7){8(7.2H!=j){5k{6e 7.3I.89(7.2H)}5n(e){}5k{6e 7.2H}5n(e){}}8(7.Z.5d==j){H}8(7.2W==j){7.2W=K.1W("3C");7.2W.q.6d="1m";7.2W.q.1l="6s-39";7.3I.88(7.2W,7);7.2W.1D(7)}7.2H=K.1W("81");k 6n=$4.3E(7,"5d");7.2H.11=6n;8(7.6l!=j){7.3I.7V(7.2H,7.6l)}J{7.3I.1D(7.2H)}};$4.3E=t(7,Z){k 4X=[];1n(k i 2P 7.Z[Z]){4X.2b(7.Z[Z][i])}k 2Y="1b-"+4X.7X().4q("-");k 29={};k 3q="";1n(k i 2P 7.Z[Z]){k 1C=$4.O[i][7.Z[Z][i]].1C;1C=7.Z.3r[1C];8(1C!=j){2Y+="-D"+1C}8(i!="28"&&$4.O[i]["2g"]!=j){k 2X=$4.O[i]["2g"].1C;2X=7.Z.3r[2X];8(2X!=j){2Y+="-80"+2X}1n(k a 2P $4.O[i]["2g"].21){8(29[a]==j){29[a]=""}29[a]+=$4.O[i]["2g"].4Z(a,2X)}}1n(k a 2P $4.O[i][7.Z[Z][i]].21){8(29[a]==j){29[a]=""}29[a]+=$4.O[i][7.Z[Z][i]].4Z(a,1C)}}1n(k a 2P 29){3q=a=="3T"?2Y:2Y+":"+a;8(C.3H.2n(3q)>=0){35}$4.6C(Z,3q,29[a]);C.3H.2b(3q)}H 2Y};$4.4V=t(7){8(7==j){8(C.2t==j){H}1n(k b=0;b<C.2t.14;b++){$4.4V(C.2t[b])}H}$4.2s==j&&($4.2s={3h:[],X:[],2d:[]});8(7.40===U){H}k 3o=1h;7.G={};8(7.4U.2m()=="2d"){7.1M="5o";$4.2s.2d.2b(7);$4.1p.c(C,"2L",$4.4f)}J 8((7.4U.2m()=="8r"||7.4U.2m()=="8u")&&7.1O.2m()=="3d"){7.1M="2q";$4.2s.X.2b(7);8(7.X!=j&&7.X.1B.22().2n(7.X.1B.22()+"#")<0){8(/^(54:)/.4Y(7.X.1B.22())){7.G.3u=7.X.1B}J{7.G.N=7.X.1B}7.X.8t=t(){H 1h};3o=U}}J{7.1M="8s";$4.2s.3h.2b(7);8(7.19("2h")!=j&&7.19("2h").2n(K.1P.2h+"#")<0){8(/^(54:)/.4Y(7.19("2h").22())){7.G.3u=7.19("2h")}J{7.G.N=7.19("2h")}7.2h="54:8l(0)";3o=U}}7.G.F={B:7.19("B")||(7.X?7.X.19("B"):j),r:7.19("r")||(7.X?7.X.19("r"):j)};k 2a=7.19("53-2a")||(7.X?7.X.19("53-2a")||"1r":"1r");8(2a!=j&&2a.22()!=""){k 32=2a.2m().3k("-");7.G.2a=32[0];7.G.32=32[1]}J{7.G.1k=7.19("1k")||(7.X?7.X.19("1k"):j)}7.2v("1k","");7.X&&7.X.2v("1k","");k 3p=7.41("1G");8(3p.14<=0){8(3o){$4.1p.c(7,"3z",$4.5g)}}J{1n(k i=0;i<3p.14;i++){3p[i].G=7.G;8(3o){$4.1p.c(3p[i],"3z",$4.5g)}}}8(7.1M=="2q"){7.2j=7.19("2j")||7.X.19("2j")}J{7.2j=7.19("2j")}8(7.2j&&1Z C[7.2j]=="t"){7.2j=C[7.2j]}8(7.1M=="2q"){7.1A=7.19("1A")||7.19("3b")||7.X.19("1A")||7.X.19("3b")}J{7.1A=7.19("1A")||7.19("3b")}7.51=t(N){N&&(E.G.N=N);$4.15(E)};8(7.1M=="2q"){7.X.65=7;7.X.51=t(N){E.65.51(N)}}7.40=U};$4.5g=t(2O){8(2O==j){H 1h}k 7=2O.4y||2O.1k;8(7!=j&&7.G!=j){8(7.G.3u!=j&&7.G.3u.22()!=""){5x(7.G.3u)}J 8(7.G.N!=j||7.G.N.22()!=""){$4.15(7)}}};$4.15=t(7){k 1a=7?(7.G||7):j;8(1a==j||1a.N==j||1a.N.22()==""){H}7.G=1a;8(1Z $4.15[7.G.2a]=="t"){$4.15[7.G.2a](7)}J{$4.15.1r(7)}};$4.15.F={1r:{B:6f,r:66},7t:{B:6f,r:66}};$4.15.F.37=t(F){8(F==j||$4.15.F[F]==j){H $4.15.F.1r}H $4.15.F[F]};$4.15.1r=t(7){$4.15.4g(7)};$4.15.7h=t(7){8(7.1M=="2q"){$4.2w=C.3B("",7.G.1k||"2B");7.X.2v("1k",7.G.1k||"2B");7.X.3d()}J{$4.2w=C.3B(7.G.N,7.G.1k||"2B")}};$4.15.7i=t(7){8(7.1M=="2q"){7.X.2v("1k","7j");7.X.3d()}J{K.1P.2h=7.G.N}};$4.15.7s=t(7){k F=$4.15.F.37(7.G.32);k 1V=(69.B-F.B)/2;k 1S=(69.r-F.r)/2-50;$4.2w=C.3B("","7K","B="+F.B+",r="+F.r+",7J=1,1P=0,7x=0,7L=1,7P=0,7O=0,1q=0,1V="+1V+",1S="+1S+"");8(7.1M=="2q"){7.X.2v("1k",7.G.1k||"2B");7.X.3d()}J{$4.2w.1P.2h=7.G.N}$4.2w.7G()};$4.15.4g=t(7){k F=$4.15.F.37(7.G.32);$4.P.3B({7:7,B:F.B,r:F.r},7)};$4.4f=t(e){8(e==j){e=C.2O}8(e==j){H}k 47=Q 49($4.V.5j);k 2G=47.59(e.7C);8(2G==j||2G.14<=0){H}k 7=$4.5v(e.7F);k 2L=j;5k{5x("2L = "+e.G)}5n(e){H}8(1Z $4.P[2L.1B]=="t"){$4.P[2L.1B](2L.G,7,U)}};$4.5v=t(5l){8($4.n.4J&&$4.n.1c.1t.3N===5l){H $4.n.2A}1n(k i=0;i<$4.2s.2d.14;i++){8($4.2s.2d[i].3N===5l){H $4.2s.2d[i]}}};$4.P={};$4.P.9n=t(G,7,1L){$4.P.2x(7);8(G!=j){$4.P.5L(G,7,1L)}};$4.P.5L=t(G,7,1L){k 1A=1Z 7.1A=="t"?7.1A:(1Z C[7.1A]=="t"?C[7.1A]:j);8(1A){1A(G)}};$4.P.1g=t(2r,7,1L){8(!7){H}$4.u.A=7.1M=="5o"?7:$4.n;8(7.1M=="5o"){$4.u.A.z={}}J{$4.u.A.z==j&&($4.u.A.z={})}8(2r){k 17=j;8($4.15.F[2r]!=j){17=$4.15.F[2r]}J 8(2r.B!=j&&2r.r!=j){17=2r}J{k 3v=(2r+"").3k(/[9o,|;]/);8(3v.14==2){17={B:3v[0],r:3v[1],2y:(3v[2]==="U")}}}17.B=31((17.2y?17.B:(7.G.F.B||17.B))+"",10);17.r=31((17.2y?17.r:(7.G.F.r||17.r))+"",10);8(17==j||4a(17.B)||4a(17.r)){$4.u.A.z.W=j;H}8(1L&&$4.u.A.1O=="n"){$4.P.6p(7)}$4.u.A.z.4P={B:17.B,r:17.r};8((17.r+($4.u.A.z.1N*2))>C.F().r){17.r=5S.5u($4.u.A.z.6o,(C.F().r-($4.u.A.z.1N*2)))}8((17.B+($4.u.A.z.1N*2))>C.F().B){17.B=5S.5u($4.u.A.z.6j,(C.F().B-($4.u.A.z.1N*2)))}k 3S=$4.u.A.1O=="n"?($4.n.z.B!=j&&$4.n.z.r!=j?$4.n.z:j):{B:$4.u.A.3V||$4.u.A.8w,r:$4.u.A.4p||$4.u.A.9p};8(3S==j||($4.u.A.1O=="n"&&!$4.n.2z)){$4.u.A.z.W=j;$4.P.1g.3Q(17.B);$4.P.1g.3D(17.r);H}8($4.u.A.z.W!=j){H}$4.u.A.z.W={4i:3S,2J:3S,2e:17,30:Q 4o().4n(),9q:E.30+$4.V.3W};(7.G.F.B==j||$4.u.A.1O=="n")&&2S(E.1g.3Q);(7.G.F.r==j||$4.u.A.1O=="n")&&2S(E.1g.3D)}};$4.P.1g.3Q=t(w){8($4.u.A.z.W!=j){8($4.u.A.z.W.2J.B==$4.u.A.z.W.2e.B){8($4.u.A.z.W.62===U){$4.u.A.z.W=j}J{$4.u.A.z.W.61=U}H}k 3R=(w==j||w<=$4.u.A.z.W.30)?Q 4o().4n():w;k 3O=(3R-$4.u.A.z.W.30)/$4.V.3W;k 4T=$4.u.A.z.W.2e.B-$4.u.A.z.W.4i.B;k 4l=4T<0?-1:1;k 5z=4T*3O;w=$4.u.A.z.W.2J.B+5z;8((w*4l)>($4.u.A.z.W.2e.B*4l)){w=$4.u.A.z.W.2J.B=$4.u.A.z.W.2e.B}}J 8(w==j){H}$4.u.A.z.B=w;8($4.u.A.1O=="n"){$4.u.A.z.4O=$4.u.A.z.B+($4.u.A.z.2c*2);$4.u.A.1c.q.B=$4.u.A.z.4O+"18";$4.u.A.1c.1t.q.B=($4.u.A.z.B-(($4.1H()>=0&&$4.V.23)?$4.u.A.z.1d*2:0))+"18"}J{$4.u.A.q.B=$4.u.A.z.B+"18"}$4.P.1m($4.u.A);8($4.u.A.z.W!=j){2S($4.P.1g.3Q)}};$4.P.1g.3D=t(h){8($4.u.A.z.W!=j){8($4.u.A.z.W.2J.r==$4.u.A.z.W.2e.r){8($4.u.A.z.W.61===U){$4.u.A.z.W=j}J{$4.u.A.z.W.62=U}H}k 3R=(h==j||h<=$4.u.A.z.W.30)?Q 4o().4n():h;k 3O=(3R-$4.u.A.z.W.30)/$4.V.3W;k 4S=$4.u.A.z.W.2e.r-$4.u.A.z.W.4i.r;k 4I=4S<0?-1:1;k 63=4S*3O;h=$4.u.A.z.W.2J.r+63;8((h*4I)>($4.u.A.z.W.2e.r*4I)){h=$4.u.A.z.W.2J.r=$4.u.A.z.W.2e.r}}J 8(h==j){H}$4.u.A.z.r=h;8($4.u.A.1O=="n"){$4.u.A.z.4H=$4.u.A.z.r+($4.u.A.z.2c*2);$4.u.A.1c.q.r=$4.u.A.z.4H+"18";$4.u.A.1c.1t.q.r=($4.u.A.z.r-(($4.1H()>=0&&$4.V.23)?$4.u.A.z.1d*2:0))+"18"}J{$4.u.A.q.r=$4.u.A.z.r+"18"}$4.P.1m($4.u.A);8($4.u.A.z.W!=j){2S($4.P.1g.3D)}};$4.P.1g.5O=t(e){8($4.n.2z&&$4.n.z.4P.r>$4.n.z.r){$4.P.1g($4.n.z.4P)}};$4.P.1m=t(7,1L){8($4.n.2z||$4.n.2E){k 2U=$4.1H()==6;k 3A=C.1m({B:$4.n.z.4O,r:$4.n.z.4H},2U);k 1S=3A.y>=$4.n.z.1N?3A.y:$4.n.z.1N;k 1V=3A.x>=$4.n.z.1N?3A.x:$4.n.z.1N;$4.n.1c.q.1S=1S+"18";$4.n.1c.q.1V=1V+"18"}};$4.P.4R=t(7,1L){8($4.n.2z&&!$4.n.2E&&!$4.n.4F){8($4.n.1c.1t.3N.4A!=j){$4.n.1c.1t.3N.4A({"1B":"4R"},"*");$4.n.4F=U;$4.n.6m=4z($4.P.2x,9D)}}};$4.P.2x=t(7,1L){8($4.n.2z||$4.n.2E){8($4.n.36!=j){$4.n.36.1z()}8($4.n.1c!=j){$4.n.1c.1z();$4.n.1J.3n.1z();$4.n.1J.1X.1z();$4.n.1c.1t.34="";$4.n.2z=1h;$4.n.2E=1h}6q($4.n.6m);$4.n.4F=1h}J 8($4.2w!=j){$4.2w.2x();$4.2w=j}};$4.P.6p=t(7,1L){8(1L===U){$4.P.1g($4.n.z.6F,7,1L)}$4.n.1J.1X.1z();$4.n.1J.3n.1x();$4.n.1c.1t.1x()};$4.n={1O:"n",4J:1h,2z:1h,z:{1d:5,2c:0,1N:20,6o:6i,6j:6i,4B:16}};$4.n.1J={1X:(t(){k l=K.1W("3C");l.q.6d="1m";l.F={B:9y,r:76};k 34=\'1X.9g\';l.1G=K.1W("1G");l.1D(l.1G);l.1G.34=$4.V.1j+34;l.1G.F={B:44,r:44};l.1G.B=l.1G.F.B;l.1G.r=l.1G.F.r;l.1G.q.1N="6A 74 2o";l.1G.q.1l="39";l.2Z=K.1W("p");l.1D(l.2Z);l.2Z.q.6c="4m";l.2Z.q.4v="4w";l.2Z.q.I="8L";l.1o=K.1W("a");l.1D(l.1o);l.1o.q.6c="4m";l.1o.q.4v="6I";l.1o.q.I="#8N";l.1o.q.8M="8F";l.1o.q.55="5h";l.1o.q.4C="5T";l.1o.1x=t(){E.q.4C=""};l.1o.1z=t(){E.q.4C="5T"};l.1x=t(){E.46();E.q.1l=\'\';E.5M=4z("$4.n.1J.1X.1o.1x()",8z)};l.1z=t(){$4.n.2E=1h;E.q.1l=\'26\';E.1o.1z();6q(E.5M)};l.46=t(){8(!E.3y){E.2Z.1D(K.5A($4.3U.37("3L")));E.1o.1D(K.5A($4.3U.37("1o")));$4.1p.c(E.1o,"3z",$4.P.2x);E.3y=U}};H l})(),3n:(t(){k 1e=K.1W("3C");1e.1x=t(){E.46();E.q.1l=\'\'};1e.1z=t(){E.q.1l=\'26\'};1e.q.B=$4.n.z.4B+"18";1e.q.r=$4.n.z.4B+"18";1e.q.4v="2l";1e.q.4c="8P";1e.q.8Q="N("+$4.V.1j+"2x.1v)";1e.q.4Q="0 0";1e.q.1i="45";1e.q.93="1Q";1e.q.1S="1Q";1e.q.55="5h";1e.q.4h="92";1e.91="";1e.5s=t(e){8(e==j){e=2O}8(e==j){H}k 1u=e.4y||e.1k;1u.q.4Q="0 -94"};1e.5t=t(e){8(e==j){e=2O}8(e==j){H}k 1u=e.4y||e.1k;1u.q.4Q="0 0"};1e.46=t(){8(!E.3y){8(!$4.V.2M||($4.1H()>=0&&$4.V.23)){$4.1p.c(E,"3z",$4.P.2x)}J{$4.1p.c(E,"3z",$4.P.4R)}$4.1p.c(E,"8T",E.5s);$4.1p.c(E,"8R",E.5t);E.3y=U}};1e.3y=1h;H 1e})()};$4.n.4E=t(){$4.n.36==j&&($4.n.36=$4.43.5F());$4.n.1c==j&&($4.n.1c=$4.43.48());$4.n.4J=U;H $4.n};$4.43={2d:t(1R){k o=K.1W("2d");o.6r="1b-3w-5N";o.2v("1R",1R||"1b-3w-5N");o.8Y="0";o.1x=t(){E.q.1l=\'\'};o.1z=t(){E.q.1l=\'26\'};H o},48:t(){k o=K.1W("3C");o.6r="1b-3w-48";o.2v("1R","1b-3w-48");o.1x=t(){E.q.1l=\'39\'};o.1z=t(){E.q.1l=\'26\'};K.41("2Q")[0].1D(o);o.q.4c="#1f";o.q.1d=$4.n.z.1d+"18 2K #8S";o.q.90="1s 1s 1s 1s";o.q.97="1s 1s 1s 1s";o.q.8C="1s 1s 1s 1s";o.q.1l="26";8($4.1H()==6||$4.V.23){o.q.1i="45";$4.1p.c(C,"5G",$4.P.1m)}J{o.q.1i="7g"}$4.1p.c(C,"1g",$4.P.1g.5O);$4.1p.c(C,"1g",$4.P.1m);$4.1p.c(C,"2L",$4.4f);o.q.4h="8A";o.1D($4.n.1J.3n);8($4.V.2M&&!($4.1H()>=0&&$4.V.23)){$4.n.1J.3n.1z()}o.1t=$4.43.2d();o.1D(o.1t);o.1t.q.1i="45";o.1t.q.1V=$4.n.z.2c+"18";o.1t.q.8K=$4.n.z.2c+"18";8($4.V.2M&&!($4.1H()>=0&&$4.V.23)){o.1D($4.n.1J.1X)}H o},5F:t(){k o=K.1W("3C");o.1x=t(){E.q.1l=\'39\'};o.1z=t(){E.q.1l=\'26\'};K.41("2Q")[0].1D(o);8($4.1H()>=0){o.q.4c="#8I";o.q.8J="9a(9z=50) !9x";8($4.1H()==6||$4.V.23){o.q.B=C.F().B+"18";o.q.r=(C.F().r+12)+"18";o.q.1i="45";o.q.1V=C.1m(C.F(),U).x+"18";o.q.1S=C.1m(C.F(),U).y+"18";$4.1p.c(C,"5G",t(){o.q.1V=C.1m(C.F(),U).x+"18";o.q.1S=C.1m(C.F(),U).y+"18"});$4.1p.c(C,"1g",t(){o.q.B=C.F().B+"18";o.q.r=(C.F().r+12)+"18";o.q.1V=C.1m(C.F(),U).x+"18";o.q.1S=C.1m(C.F(),U).y+"18"})}}J{o.q.4c="9b(0,0,0,.5)"}o.q.1l="26";8($4.1H()!=6&&!($4.1H()>=0&&$4.V.23)){o.q.B="5I%";o.q.r="5I%";o.q.1V="0";o.q.1S="0";o.q.1i="7g"}o.q.4h="76";H o}};$4.P.3B=t(1a,7,1L){$4.n.2A=j;8(1a==j){H}1a.7==j&&(1a.7=7);8(1a.7==j&&1a.N==j){H}8(!7.G.F){7.G.F=$4.15.F.1r}k B=1a.B!=j&&!4a(31(1a.B,10))?31(1a.B,10):$4.15.F.1r.B;k r=1a.r!=j&&!4a(31(1a.r,10))?31(1a.r,10):$4.15.F.1r.r;k N=1a.N;8(1a.7!=j){8(1a.7.G!=j&&1a.7.G.N!=j){N=1a.7.G.N}}$4.n.2A=1a.7;N+=(N.2n("?")>0?"&":"?")+"8E=4g";8($4.n.2A.1A&&1Z C[$4.n.2A.1A]=="t"){N+=(N.2n("?")>0?"&":"?")+"8D=U"}$4.n.4E();8($4.V.2M&&!($4.1H()>=0&&$4.V.23)){$4.P.1X({B:B,r:r},7)}J{$4.P.1g({B:B,r:r},7);$4.n.36.1x();$4.n.1c.1x()}8($4.n.2A.1M=="2q"){$4.n.2A.X.2v("1k",$4.n.1c.1t.19("1R"));$4.n.2A.X.3d()}J{$4.n.1c.1t.34=N}$4.n.2z=U};$4.P.1X=t(6G,7){8($4.n.2E){H}$4.P.2x(7);$4.n.z.6F=6G;$4.n.4E();$4.n.1c.1t.1z();$4.n.1J.1X.1x();$4.P.1g({B:$4.n.1J.1X.F.B,r:$4.n.1J.1X.F.r,2y:U},7);$4.n.2E=U;$4.n.36.1x();$4.n.1c.1x();$4.P.1m()};C.2S=(t(){H C.2S||C.9v||C.9A||C.9C||C.9G||t(3b,1u){C.4z(3b,9E/60)}})();$4.1p={c:t(1u,33,1B){8(1u.6L){1u.6L(33,1B,1h)}J 8(1u.6Y){1u.6Y("72"+33,1B)}J{H 1h}H U},9s:t(1u,33,1B){8(1u.6W){1u.6W(33,1B,1h)}J 8(1u.73){1u.73("72"+33,1B)}J{H 1h}H U}};9i.2I.22=t(){H E.6H(/^\\s+|\\s+$/,\'\')};3Y.2I.2n=t(o){k 4j=-1;1n(k i=0;i<E.14;i++){8(E[i]==o){4j=i;6B}}H 4j};3Y.4d=t(3Z){8(1Z 3Z.14=="4e"){H j}k 4s=[];1n(k i=0;i<3Z.14;i++){4s.2b(3Z[i])}H 4s};C.F=t(){k w=0;k h=0;8(!C.6V){8(!(K.2T.3V==0)){w=K.2T.3V;h=K.2T.4p}J{w=K.2Q.3V;h=K.2Q.4p}}J{w=C.6V;h=C.9e}H{B:w,r:h}};C.1m=t(2N,2U){2U=2U===U;8(2N==j||2N.B==j||2N.r==j){2N={B:0,r:0}}k 4k=0;k 4G=0;k 3m=0;k 3g=0;8(2U){8(!C.6M){8(!(K.2T.4r==0)){3g=K.2T.4r;3m=K.2T.6O}J{3g=K.2Q.4r;3m=K.2Q.6O}}J{3m=C.9k;3g=C.6M}}4k=((E.F().B-2N.B)/2)+3m;4G=((E.F().r-2N.r)/2)+3g;H{x:4k,y:4G}};$4.1H=t(){8($4.5p==j){k 5m=-1;8(1T.9l==\'9m 8y 7Y\'){k 6N=1T.7D;k 6S=Q 49("7E ([0-9]{1,}[\\.0-9]{0,})");8(6S.59(6N)!=j){5m=7Q(49.$1)}}$4.5p=5m}H $4.5p};$4.5W=t(){8($4.1K==j){k 1K=j;8(1T){8(1T.70){1K=1T.70}J 8(1T.6X){1K=1T.6X}J 8(1T.6x){1K=1T.6x}J 8(1T.6y){1K=1T.6y}J{1K="68-8h"}k 3l=1K.3k("-");1K={2i:3l[0],8d:3l.14>1?3l[1]:3l[0].8m()}}$4.1K=1K}H $4.1K};t $Y(1C){E.1C=1C;E.21={}}$Y.2I.1O="$Y";$Y.2I.4Z=t(1q,3G){8(1q==j){1q="3T"}k 4W="";8(E.21[1q]!=j){1n(k f=0;f<E.21[1q].14;f++){k 1R=E.21[1q][f].1R;k 1I=E.21[1q][f].1I;8(E.1C!=j&&$4.O[E.1C]!=j){8($4.O[E.1C][3G]==j){3G="24"}k 3P=1I.3k(/[,;]/);8(3P.14>1){1n(k v=0;v<3P.14;v++){k 3F=3P[v].3k("=");8(3F.14!=2){35}8(3F[0].22().2m()==3G.2m()){1I=3F[1];6B}}}}4W+=1R+": "+1I+";"}}H 4W};$Y.2I.c=t(){k 3J=j;k 1q=j;k 1I=j;8(2f.14<2||2f.14>3){H}8(2f.14==2){3J=2f[0];1I=2f[1]}8(2f.14==3){3J=2f[0];1q=2f[1].6H(":","");1I=2f[2]}1q==j&&(1q="3T");E.21[1q]==j&&(E.21[1q]=[]);E.21[1q].2b(Q $3M(3J,1I));H E};t $3M(1R,1I){E.1R=1R;E.1I=1I}$3M.2I.1O="$3M";$4.6C=t(1F,11,5c){8(K.82!=j){$4.2k==j&&($4.2k={});8($4.2k[1F]==j){k 3i=K.1W(\'q\');3i.1O=\'2D/8a\';K.41("87")[0].1D(3i);$4.2k[1F]=3i.85||3i.8k}$4.2k[1F].7d?$4.2k[1F].7d("."+11,5c):$4.2k[1F].7W("."+11+" {"+5c+"} ",$4.2k[1F].7Z.14)}};t $2F(1F,2R){E.1F=1F;E.2R=2R}$2F.2I.7a=t(11){8(E.2R==j||E.2R.14<=0){H""}k 47=Q 49("-("+E.2R.4q("|")+")-","g");k 2G=47.59("-"+11.2m()+"-");H((2G!=j&&2G.14>0)?2G[1]:"")};$4.5a={I:Q $2F("28",["4N","5i","52","5e","58","5b"]),F:Q $2F("q",["l","m","s"]),1E:Q $2F("q",["6T","71","6U"]),3f:Q $2F("q",["6Z","6K","6w"]),1U:Q $2F("5d",["77","79","78","7f","6P","5K","5q","5X"])};$4.O={28:{},I:{},F:{},1E:{},3f:{},1U:{},1r:"28"};$4.O.28.2g=Q $Y().c("55","5h").c("1l","6s-39").c("1N","6E").c("1E-8e","3T").c("2D-6Q","26").c("2D-6Q",":1Y","26").c("1E-F","2o").c("3X-r","6J").c("2c","2l 2o");$4.O.I.2g=Q $Y("F").c("I",":3h","#1f").c("T-6z","6z-x").c("T-1i","0 0").c("T-1i",":1Y","S=0 -8B,M=0 -98,L=0 -7e,24=0 -7e").c("T-1i",":25","S=0 -9f,M=0 -8G,L=0 -5w,24=0 -8W");$4.O.I.4N=Q $Y().c("T","N("+$4.V.1j+"1b-1w-4N.1v)").c("T-I","#8Z").c("T-I",":1Y","#9F").c("T-I",":25","#9t").c("I","#1f").c("I",":38","#1f").c("I",":1Y","#1f").c("I",":25","#1f").c("1d","1y 2K #6k").c("2D-42","1y 1y #6k");$4.O.I.5i=Q $Y().c("T","N("+$4.V.1j+"1b-1w-5i.1v)").c("T-I","#7k").c("I","#1f").c("I",":38","#1f").c("I",":1Y","#1f").c("I",":25","#1f").c("1d","1y 2K #64").c("2D-42","1y 1y #64");$4.O.I.52=Q $Y().c("T","N("+$4.V.1j+"1b-1w-52.1v)").c("T-I","#8o").c("I","#1f").c("I",":1Y","#1f").c("I",":38","#1f").c("I",":25","#1f").c("1d","1y 2K #6b").c("2D-42","1y 1y #6b");$4.O.I.5e=Q $Y().c("T","N("+$4.V.1j+"1b-1w-5e.1v)").c("T-I","#8f").c("I","#1f").c("I",":38","#1f").c("I",":1Y","#1f").c("I",":25","#1f").c("1d","1y 2K #6D").c("2D-42","1y 1y #6D");$4.O.I.58=Q $Y().c("T","N("+$4.V.1j+"1b-1w-58.1v)").c("T-I","#7l").c("T-I",":1Y","#7u").c("T-I",":25","#8p").c("I","#3x").c("I",":3h","#3x").c("I",":38","#3x").c("I",":1Y","#3x").c("I",":25","#3x").c("1d","1y 2K #8c");$4.O.I.5b=Q $Y().c("T","N("+$4.V.1j+"1b-1w-5b.1v)").c("T-I","#86").c("I","#3j").c("I",":3h","#3j").c("I",":38","#3j").c("I",":1Y","#3j").c("I",":25","#3j").c("1d","1y 2K #7U");$4.O.F.l=Q $Y().c("1E-F","2o").c("3X-r","6J").c("2c","2l 2o");$4.O.F.m=Q $Y().c("1E-F","6t").c("3X-r","6A").c("2c","2l 6t");$4.O.F.s=Q $Y().c("1E-F","6R").c("3X-r","8v").c("2c","2l 6R");$4.O.1E.6T=Q $Y().c("1E-4K","4m");$4.O.1E.6U=Q $Y().c("1E-4K","9j 9c");$4.O.1E.71=Q $Y().c("1E-4K","9h");$4.O.3f.6Z=Q $Y().c("1d-27","2l").c("-4L-1d-27","2l").c("-4D-1d-27","2l");$4.O.3f.6K=Q $Y("F").c("1d-27","S=2C,M=1s,L=1Q,24=1Q").c("-4L-1d-27","S=2C,M=1s,L=1Q,24=1Q").c("-4D-1d-27","S=2C,M=1s,L=1Q,24=1Q");$4.O.3f.6w=Q $Y("F").c("1d-27","S=6I,M=4x,L=2o,24=1Q").c("-4L-1d-27","S=4w,M=4x,L=2o,24=1Q").c("-4D-1d-27","S=4w,M=4x,L=2o,24=1Q");$4.O.1U.2g=Q $Y().c("1N","-6E 74 0").c("1l","39");$4.O.1U.77=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p.1v)").c("T-1i","0 -3K").c("B","8V").c("r","2u");$4.O.1U.79=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p.1v)").c("T-1i","0 -2C").c("B","99").c("r","2u");$4.O.1U.78=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p-7b.1v)").c("T-1i","0 -3K").c("B","8O").c("r","2u");$4.O.1U.7f=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p-7b.1v)").c("T-1i","0 -2C").c("B","9u").c("r","2u");$4.O.1U.6P=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p-5P.1v)").c("T-1i","0 -3K").c("B","5w").c("r","2u");$4.O.1U.5K=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p-5P.1v)").c("T-1i","0 -2C").c("B","95").c("r","2u");$4.O.1U.5q=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p-67.1v)").c("T-1i","0 -3K").c("B","9B").c("r","2u");$4.O.1U.5X=Q $Y().c("T","N("+$4.V.1j+"1b-1w-2p-67.1v)").c("T-1i","0 -2C").c("B","7y").c("r","2u");$4.3U={1r:{3L:"5V 5Y 6a 2B",1o:"7R"},68:{3L:"5V 5Y 6a 2B"},7S:{3L:"83 o 7o 3a 2B"}};$4.3U.37=t(3e,2i){2i=2i==j?$4.5W().2i:2i;H E[2i]!=j&&E[2i][3e]!=j?E[2i][3e]:(E.1r[3e]!=j?E.1r[3e]:"")};$4.6u=t(G){8(1Z G!="8H"){H}G.8X="4";G.7w=E.7c;G.8j=6v(K.9r);G.9d=K.1P.75;G.9w=6v(K.1P.3c+"\\/\\/"+K.1P.75+K.1P.8x+K.1P.96);k 4M=[];1n(k 4t 2P G){4M.2b(4t+"="+G[4t])}Q 8U().34=$4.V.5U+"?"+4M.4q("&")};$4();$5Z=$4;$5Z.84=$4.4u;',62,601,'||||MPC|||trigger|if||||add|||||||null|var|||Modal|||style|height||function|__temp|||||__size|resizedElement|width|window||this|size|data|return|color|else|document|||url|styles|__actions|new|triggers||background|true|conf|transition|form|MPCSSOBJ|styleDefinition||className|||length|openCheckout||newSize|px|getAttribute|openData|MP|__dialog|border|cb|fff|resize|false|position|imageBasePath|target|display|center|for|cancel|__event|status|dflt|5px|__container|element|png|payButton|show|1px|hide|onreturn|action|dependency|appendChild|font|groupType|img|__getIEVersion|value|__assets|locale|isMessage|integrationType|margin|type|location|7px|name|top|navigator|logo|left|createElement|loading|hover|typeof||attributes|trim|quirksMode|DEF|active|none|radius|base|combinedStyle|mode|push|padding|iframe|end|arguments|common|href|lang|onerror|__styleSheets|0px|toLowerCase|indexOf|20px|logos|FORM|resizeData|__integrations|mp_checkout_triggers|23px|setAttribute|checkout|close|force|__opened|__caller|MercadoPago|3px|text|__isLoading|MPVG|match|extraElement|prototype|current|solid|message|messagingEnabled|whObj|event|in|body|elements|requestAnimationFrame|documentElement|useScrollOffset|cssProcessed|extraContainer|commonDependency|styleClassName|welcome|timeStart|parseInt|modeData|eventName|src|continue|__dimmer|get|visited|block|com|callback|protocol|submit|textId|shape|offsetY|link|mpStyleSheet|333333|split|tempLocale|offsetX|closeButton|fireEvent|imgContent|finalClassName|all|MPCDEV|group|js|whPair|Checkout|215181|initialized|click|pos|open|div|setHeight|__processCSS|pair|depValue|mp_stylesLoaded|parentNode|attribute|29px|starting|MPCSSATTR|contentWindow|elapsedPercentTime|values|setWidth|transitionTime|currentSize|normal|_txt|clientWidth|resizeDuration|line|Array|collection|processed|getElementsByTagName|shadow|__components||absolute|init|regex|dialog|RegExp|isNaN|https|backgroundColor|fromCollection|undefined|__msgReceiver|modal|zIndex|start|returnIndex|_x|widthSign|Arial|getTime|Date|clientHeight|join|scrollTop|ret|key|__doVisual|fontSize|13px|15px|srcElement|setTimeout|postMessage|controlSize|visibility|webkit|__build|__closeRequested|_y|dialogHeight|heightSign|__built|family|moz|pairs|blue|dialogWidth|requested|backgroundPosition|requestClose|heightDiff|widthDiff|tagName|__doBehavior|returnCSS|styleArray|test|getCSS||openMPCheckout|red|mp|javascript|cursor|srcClassName|__doRender|lightblue|exec|vGroups|grey|styleBody|extra|green|forcedClass|fire|pointer|orange|messageOrigin|try|msgSource|rv|catch|IFRAME|IEVersion|veall|load|mouseOver|mouseOut|max|__getTrigger|160px|eval|__processBase|widthIncrement|createTextNode|MPC_executed|mp_renderLoaded|assets|dejavu|dimmer|scroll|buttons|100|http|mxon|complete|cancelTimer|IFrame|checkSize|mex|mercadopago|getElementsByName|Math|hidden|dejavuPath|Iniciando|__getLocale|veon|pago|MPBR||widthFinished|heightFinished|heightIncrement|F67C41|mpButton|550|ven|es|screen|con|CC1B17|fontFamily|textAlign|delete|815|__processStyle|__processExtra|350|minWidth|293E75|nextSibling|__closeTimeout|extraClassName|minHeight|ready|clearTimeout|id|inline|17px|__track|escape|ov|systemLanguage|userLanguage|repeat|30px|break|__addCSS|0B898B|10px|opening|dialogSize|replace|12px|40px|rn|addEventListener|pageYOffset|ua|scrollLeft|mxall|decoration|14px|re|ar|tr|innerWidth|removeEventListener|browserLanguage|attachEvent|sq|language|ge|on|detachEvent|auto|hostname|150|arall|brall|aron|getData|br|version|addRule|80px|bron|fixed|blank|redirect|_self|F27126|B8D2EB|mlstatic|tools|pagamento|web|jm|ml|popup|lite|D9E7F4|mlapps|Version|menubar|71px|BackCompat|www|compatMode|origin|userAgent|MSIE|source|focus|org|NavPixel|scrollbars|MPCheckout|resizable|mptools|jsapi|toolbar|titlebar|parseFloat|Cancelar|pt|mp_buttons|ADADAD|insertBefore|insertRule|sort|Explorer|cssRules|CD|span|styleSheets|Inicializando|doVisual|sheet|C5C5C5|head|replaceChild|removeChild|css|Action|8DB7E9|country|weight|077574|mercadolibre|AR|accountrecovery|_referrer|styleSheet|void|toUpperCase|concat|C12020|E8F8FD|400|button|LINK|onsubmit|input|25px|offsetWidth|port|Internet|5000|200|470px|borderRadius|useCallback|openMode|underline|360px|object|000000|filter|bottom|333|textDecoration|00f|165px|transparent|backgroundImage|mouseout|CCCCCC|mouseover|Image|137px|120px|App|frameBorder|28536F|MozBorderRadius|innerHTML|250|right|16px|88px|pathname|webkitBorderRadius|300px|111px|alpha|rgba|MS|_host|innerHeight|520px|gif|Georgia|String|Trebuchet|pageXOffset|appName|Microsoft|finalize|Xx|offsetHeight|timeEnd|referrer|remove|326689|125px|webkitRequestAnimationFrame|_location|important|300|opacity|mozRequestAnimationFrame|141px|oRequestAnimationFrame|2000|1000|2F719D|msRequestAnimationFrame'.split('|'),0,{}));

			closeAjaxBox();
			link.click();
		}
	});
	
	var action = "/controllers/payments/MercadoPagoController.php";
	var eventStr = '?&event=getPayButton';
	
	var id = "&reservation_id=" + reservation_id;
	var item = "&item_id=" + item_id;
	var name = "&item_name=" + item_name;
	var price = "&reservation_price=" + document.getElementById("totalprice_" + reservation_id).value;
	
	var uri = action + eventStr + id + item + name + price;
	
	ajaxPopLoading("Procesando pago, puede demorar unos segundos, por favor aguarde...");
	
	xmlhttp.open("GET", uri, true);
	xmlhttp.send();
}



function goSearchWhat(){
	var what = document.getElementById('search_input_what').value;
	var current = getCurrentUrl('NONE', 'search_input_what', 'search_page');
	
	var go = appendVariable(current, 'search_input_what', what);
	window.location.href = go;
}


/**
 * Addds a category inside the URL
 * @param category
 */
function goSearchCategory(categoryId, categoryName){
	window.location.href = "/pages/find/?" + "search_filter_categoryId=" + categoryId + "&search_filter_categoryName=" + categoryName;
}

/**
 * Adds a filter
 * @param location
 * @param locationName
 * @param locationLevel
 */
function addFilter(filterIdValue, filterNameValue, type){
	var filterIdVariable = "search_filter_" + type + "Id";
	var filterNameVariable = "search_filter_" + type + "Name";
	
	var foward = getCurrentUrl(filterIdVariable, filterNameVariable, 'search_page');
	foward = appendVariable(foward, filterIdVariable, filterIdValue);
	foward = appendVariable(foward, filterNameVariable, filterNameValue);
	
	window.location.href = foward;
}

/**
 * Appends a variable to the end of the URL
 * @param variable
 * @param value
 * @returns {String}
 */
function appendVariable(url, variable, value){
	return url + "&" + variable + "=" + value;
}

/**
 * Gets the current URL minus one variable
 * 
 */
function getCurrentUrl(skipId, skipName, avoid){
	var curr = document.location.href;
	
	var mainAddress;
	
	if(curr.indexOf('?', 0) == -1){
		return "/pages/find/?";
	}else{
		mainAddress = curr.substring(0, curr.indexOf('?', 0));
	}
	
	var variables = curr.substring(mainAddress.length+1, curr.length);
	var arr = variables.split('&');

	var go = '';
	
	for ( var i in arr) {
		if(arr[i].indexOf(skipId) == -1 && arr[i].indexOf(skipName) == -1 && arr[i].indexOf(avoid) == -1 ){
			go = go + arr[i] + "&";
		}
	}
	var trimmed = mainAddress + "?" + go;
	
	return trimmed;
}

/**
 * Removes a filter
 * @param filterId
 * @param filterName
 */
function removeFilter(filterId, filterName){
	window.location.href = getCurrentUrl(filterId, filterName);
}


function sortBy(code){
	var foward = getCurrentUrl("search_sort", "search_sort");
	foward = appendVariable(foward, "search_sort", code);
	
	window.location.href = foward;
}











