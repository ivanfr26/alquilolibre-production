/**
 * Enables edition for all #input in the block
 * @param block_id
 */
function editBlock(block_id) {
	var block = document.getElementById(block_id);
	var inputs = block.getElementsByClassName('profile_input');
	
	for ( var i = 0; i < inputs.length; i++) {
		inputs[i].removeAttribute('readonly');
		inputs[i].style.background = 'white';
		inputs[i].style.border = '1px #e9edf3 solid';
	}
	
}

/**
 * Goes to the page to update item
 * @param itemId
 */
function editItem(itemId){
	window.location.href = '/pages/updateitem/?itemId=' + itemId;
}