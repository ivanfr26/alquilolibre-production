<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path .'controllers/server/tools.php';

new CommentsController($_REQUEST['event']);

class CommentsController{
	
	var $link;
	
	function __construct($event){
		controlUserLogguedIn();
		
		$this->setLink();
		
		if($event == 'addToUser'){
			$this->addCommentToUser();
			$this->respondOk("Gracias por comentar!");	
		}

		if($event == 'replyUser'){
			$this->replyUser();
			$this->respondOk("1 comentario respondido correctamente");
		}
	}
	
	function setLink(){
		$connction = new ConnectionManager();
		$this->link = $connction->getConnection();
	}

	function getLink(){
		return $this->link;
	}

	function replyUser(){
		$comment_id = $_REQUEST['comment_id'];
		$text = $_REQUEST['text'];
	
		$query = "call sp_add_userComment_reply('$comment_id', '$text')";
		$result = mysqli_query($this->getLink(), $query);
	
		controlDbResul($result, $query, $this->getLink());
	}
	
	function addCommentToUser(){
		$userCommented_id = $_REQUEST['userCommented_id'];
		$text = $_REQUEST['text'];
		$userCommenter_id = $_SESSION['user_id'];
		$type = $_REQUEST['type'];
		
		$query = "call sp_add_userComment_text('$userCommented_id', '$text', '$userCommenter_id', $type)";
		$result = mysqli_query($this->getLink(), $query);

		controlDbResul($result, $query, $this->getLink());
	}
	
	function respondOk($notice){
		gotUrl(getBackUrl(), $notice, "ok");
	}
	
	
	
}



?>