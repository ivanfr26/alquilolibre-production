<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/locationManager.php';
include_once $path . 'controllers/database/models/country.model.php';
include_once $path . 'controllers/database/models/province.model.php';


if(isset($_REQUEST['event'])){
	$event = $_REQUEST['event'];
	
	if($event == 'province'){
		showProvinces($_REQUEST['id']);
	}
	
	if($event == 'country'){
		showCountries();
	}
	
	if($event == 'city'){
		showCities($_REQUEST['id']);
	}
}


function showCities($province){
	$manager = new LocationManager();
	$result = $manager->searchCities($province);

	$model = new BaseModel();

	showOptions($model, "city", "Argentina", $result, false);
}

function showProvinces($country){
	$manager = new LocationManager();
	$result = $manager->searchProvince($country);

	$model = new Province();

	showOptions($model, "province", "Argentina", $result, "city");
}


function showCountries(){
	$manager = new LocationManager();
	$result = $manager->searchCountry();

	$model = new Country();
	
	showOptions($model, "country", "Argentina", $result, "province");
}

/**
 * Prints an option for a select
 * @param unknown $name
 * @param unknown $id
 */
function showOptions(BaseModel $model, $type, $default, $result, $update){
	
	$options = '<div class="selectmenu_column">';
	
	$count = 0;
	while($model->getModelFromResult($model, $result) != false)  {
		
		$name = $model->getProperty($type . "_name");
		$id = $model->getProperty($type . "_id");

		$updateCall = ""; 
		if($update != false){
			$updateCall = "updateLocation('$update', $id)";
		}
		
		$option = "<div class='selectmenu_option' onclick=\"selected('$name', '$id', '$type'); $updateCall;\">$name</div>";
		
		$options = addOption($count, $options, $option);
		$count++;
	}
	
	$options .= "</div>";
	echo $options;
}

function addOption($count, $options, $option){
// 	if($count%6 == 0 && $count > 0){
// 		$options .= '</div><div class="selectmenu_column">';
// 	}
	
	$options .= $option;
	
	return $options;
}

?>







