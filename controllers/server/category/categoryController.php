<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";

include_once $path . 'controllers/database/categoryManager.php';
include_once $path . 'controllers/database/models/category.model.php';

if(@$_REQUEST['event'] == 'getCats'){
	printCategoriesOneLevelDown($_REQUEST['cat_id'], $_REQUEST['levelBox']);
}



/**
 * Get the category tree from db
 * @param int $item_id
 */
function showCategories(Item $item, $showURL){
	$CATEGORY_SEPARATOR = ",";
	$tree = explode($CATEGORY_SEPARATOR, $item->category_path);
	
	if(count($tree) <= 1) /*just the father category*/
	{
		echo "Sin categoria asignada"; //FIXME language
	}
	else
	{
	
		$firstTime = 1;
		$view = "";
		
		
		$count = 0;
		$treeSize = sizeof($tree);
		foreach ($tree as $value) {
			$name = strstr($value, '.', true);
			$id = substr(strstr($value, ".", false), 1);

			$count++;
			$arrow = " &gt; ";
			if ($count == $treeSize){
				$arrow = "";
			}
			
			$raw = '';
			$onclick = '';
			if(!$firstTime) /*dont show the first category*/
			{
				if($showURL){
					$onclick = "onclick=\"goSearchCategory('$id', '$name')\" ";
				}else{
					$raw = "_raw";
				}
				
				$catRow = "<div class='item_categoty_level$raw' $onclick> $name $arrow </div>";
				
				$view .= $catRow;			
			}
		    $firstTime = 0;
			
		    
		    
		}
		
		echo $view;	
	
	}
}

function getCategoriesWithItems(){
	$manager = new CategoryManager();
	$result = $manager->getCategoriesWithItems();
	
	return $result;
}

function getCitiesWithItems(){
	$manager = new ConnectionManager();
	$result = mysqli_query($manager->getConnection(), "call sp_get_cities_with_items()");

	return $result;
}

function getChildrenCategories($father_id){
	$manager = new CategoryManager();
	$result = $manager->getChildrenCategories($father_id);
	
	return $result;
}

function printCategoriesOneLevelDown($father_id, $levelBox){
	$result = getChildrenCategories($father_id);
	$levelBox++;

	printCategoriesBox($result, $levelBox);
}

function printCategoriesBox($result, $levelBox){
	$print = '';
	$category = new Category();
	while($category->getModelFromResult($category, $result)){
		$cat_name = $category->category_name;
		$cat_id = $category->category_id;
			
		$print .= "<div class='category_selection_option' id='category_selection_option_$cat_id' onclick=\"showMoreCats($cat_id, $levelBox);\"
		>$cat_name</div>";
	}

	$print = "<div class='category_selection_box' id='category_box_level_$levelBox'\">"
	. $print .
	"</div>";
	
	echo $print;
}

function isBetween($value, $min, $max){
	return ($value > $min && $value < $max);
}

?>













