<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/server/tools.php';
include_once $path . 'controllers/database/itemManager.php';

$controller = new updateController();
$controller->event(@$_REQUEST['event']);

class updateController{

	private $itemManager;
	
	/**
	 * Constructor, we need to get a connection to DB
	 */
	function __construct(){
		$this->itemManager = new ItemManager();
	}
	
	
	function event($event) {
		if($event == 'updateItemData'){
			
			controlRentalLogguedIn();
			
			$this->updateItemData();
			return;
		}
		
		if($event == 'delete'){
			
			controlRentalLogguedIn();
			
			$this->delete();
			return;
		}
		
		if($event == 'activate'){
			
			controlRentalLogguedIn();
			
			$this->activate();
			return;
		}
	}

	/**
	 * Adds the new item to the DB
	 */
	function updateItemData() {
		$user_id = $_SESSION['user_id'];
	
		$item_id = $_REQUEST['item_id'];
		$item_name = $_REQUEST['item_name'];
		$item_description = $_REQUEST['item_description'];
	
		$item_address = $_REQUEST['item_address'];
	
		$item_geo = $_REQUEST['item_geolocation'];
	
		$item_conditions = $_REQUEST['item_conditions'];
	
		$item_price = $_REQUEST['item_price'];
		$item_priceType = $_REQUEST['item_priceType'];
	
		$city_id = $_REQUEST['city_id'];
		$category_id = $_REQUEST['category_id'];
	
		$isDummy = 0; //Not dummy
		$isActive = 1; //FIX ME siempre acativo

		$result = $this->itemManager->updateItemData($item_id,
											$item_name,
											$item_description,
											$city_id,
											$item_address,
											$item_geo,
											$isActive,
											$item_conditions,
											$item_price,
											$item_priceType,
											$category_id,
											$isDummy);
	
		$this->controlResult($result, "/pages/updateitem/?item_id=$item_id", mysqli_error($this->itemManager->getConnection()));
	}
	
	/**
	 * Adds the new item to the DB
	 */
	function delete() {
		$user_id = $_SESSION['user_id'];
		$item_id = $_REQUEST['item_id'];
	
		$result = $this->itemManager->delete($item_id, $user_id);
		$this->controlResult($result, "/pages/myitems/", mysqli_error($this->itemManager->getConnection()));
	}
	
	
	/**
	 * Adds the new item to the DB
	 */
	function activate() {
		$user_id = $_SESSION['user_id'];
		$item_id = $_REQUEST['item_id'];
		$active = $_REQUEST['active'];
	
		$result = $this->itemManager->activate($item_id, $active, $user_id);
		$this->controlResult($result, "/pages/myitems/", mysqli_error($this->itemManager->getConnection()));
	}
	
	
	/**
	 * If result is OK, reditrects
	 * @param unknown $result
	 */
	function controlResult($result, $address, $info) {
		include $_SERVER['DOCUMENT_ROOT'] . "/languages/es.php";
		if($result){
			header("Location: $address?&notice=$lang_dataSavedOK&nType=ok");
		}else{
			header("Location: $address?&notice=$lang_dataSavedFail.%20$info&nType=fail");
		}
	}
	
	function updateItem($itemId){
		//desglosar las variables del request y las paso
		$result = $this->itemManager->update($_POST, $_REQUEST['item_id']);
	
		if($result){
			header("Location: /pages/updateitem/?item_id=" . $_REQUEST['item_id']);
		}
	}
	
	/**
	 * Get the item from db
	 * @param int $item_id
	 */
	function getItem(){
		$item_id = $_REQUEST['item_id'];
		return $this->itemManager->read($item_id);
	}
	
}

?>






