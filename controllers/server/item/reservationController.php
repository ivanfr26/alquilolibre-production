<?php

if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/itemManager.php';
include_once $path . 'controllers/server/item/itemController.php';
include_once $path . 'controllers/server/tools.php';

new ReservationController(@$_REQUEST['event']);

class ReservationController{

	var $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	var $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	
	function __construct($event){
		if($event == 'add'){
			controlUserLogguedIn();
			$this->addReservation($_REQUEST['item_id'], $_REQUEST['item_name'], $_REQUEST['owner_id'], $_SESSION['user_id'], $_REQUEST['reservation_from'], $_REQUEST['reservation_to']);
		}

		if($event == 'updStat'){
			$this->updateStatus($_REQUEST['reservation_id'], $_REQUEST['reservation_status']);
		}
	}

	function getLink(){
		$conn = new ConnectionManager();
		return $conn->getConnection();
	}

	/**
	 * Addds a new reservation asociated to an item, an user and its owner
	 */
	function addReservation($item_id, $item_name, $owner_id, $user_id, $reservation_from, $reservation_to){
		if($reservation_from == "" || $reservation_to == ""){
			gotUrl("/product/?$item_id/Reserva-no-realizada", "Debes elegir las fechas de reserva", "Fail");
			return;
		}

		$query = "INSERT INTO `reservation`(`item_id`, `item_name`, `owner_id`, `user_id`, `reservation_from`, `reservation_to`)
		VALUES ('$item_id', '$item_name', '$owner_id', '$user_id', '$reservation_from 00:00:00', '$reservation_to 00:00:00')";

		$link = $this->getLink();
		$result = mysqli_query($link, $query);

		echo $query ."<br>" .  mysqli_error($link);

		gotUrl("/product/?$item_id/Reserva-realizada", "Reserva realizada, muchas gracias!", "OK");
	}

	function updateStatus($reservation_id, $reservation_status){
		$link = $this->getLink();
		$query = "UPDATE `reservation` SET `reservation_status`='$reservation_status' WHERE `reservation_id`='$reservation_id'";

		$result = mysqli_query($link, $query);
	
		if($result != false){
			$status = $this->getStatus($reservation_status);
			gotUrl("/pages/reservations/?", $status, "OK");
		}else{
			$error = mysqli_error($link);
			gotUrl("/pages/reservations/?", $error, "fail");
		}
		
	}


	/**
	 * Shows only the questions yet to be answered
	 *
	 * @param Item $item
	 */
	function showMyItemsReservationsHistory($user_id, $status) {
		$link = $this->getLink();

		$query = "SELECT * FROM reservation WHERE owner_id=$user_id AND reservation_status=$status";
		$result = mysqli_query($link, $query);

		$payed = RESERVATION_PAYED;
		
		while(($row = mysqli_fetch_assoc($result)) != false){
			$item_id = $row['item_id'];
			$item_name = $row['item_name'];
			$user_id = $row['user_id'];
			$reservation_id = $row['reservation_id'];
			$reservation_status = $row['reservation_status'];
			$reservation_from = $row['reservation_from'];
			$reservation_to = $row['reservation_to'];
				
			$dateFrom = DateTime::createFromFormat('Y-m-d H:i:s', $reservation_from);
			$dateto = DateTime::createFromFormat('Y-m-d H:i:s', $reservation_to);
			$days = date_diff($dateFrom, $dateto)->format("%d");
				
			$printDate = $this->getPrintDate($dateFrom, $dateto, $days);

			$user = getUserLink($user_id, "Cliente");
			$item = getItemLink($item_id, $item_name);
			
			$payButton = "";
			if($status == RESERVATION_ACEPTED){
				$payButton = "<form action='/controllers/server/item/reservationController.php?event=updStat&reservation_id=$reservation_id&reservation_status=$payed' method='POST'>
								<input class='button_green' style='float:left; margin-right:10px;' type='submit' value='Pagada'>
							</form>";
			}

			$text = "<div class='reservation_box'>
						<a href='$item' class='profile_item_row_name'>$item_name</a>
						<div class='reservations_date'>$printDate ($days d&iacute;as)</div>
						<br>
						
						$payButton
						
						<div class='reservations_seeclient'><a href='$user'>Ver cliente</a></div>
					</div>";
		

			echo $text;
		}

	}
	/**
	 * Shows only the questions yet to be answered
	 *
	 * @param Item $item
	 */
	function showMyReservations($user_id) {
		$link = $this->getLink();

		$query = "SELECT * FROM reservation WHERE user_id=$user_id ORDER BY reservation_id DESC";
		$result = mysqli_query($link, $query);

		while(($row = mysqli_fetch_assoc($result)) != false){
			$reservation_id = $row['reservation_id'];
			$item_id = $row['item_id'];
			$rental_id = $row['owner_id'];
			$reservation_status = $row['reservation_status'];
			$reservation_from = $row['reservation_from'];
			$reservation_to = $row['reservation_to'];

			$item = getItem($item_id);
			$item_name = $item->item_name;
			$item_price = $item->item_price;
			
			$dateFrom = DateTime::createFromFormat('Y-m-d H:i:s', $reservation_from);
			$dateto = DateTime::createFromFormat('Y-m-d H:i:s', $reservation_to);
			$days = date_diff($dateFrom, $dateto)->format("%d");

			$totalPrice = $item_price * $days;
			
			$imgs = getItemImages($item_id, $item->user_id);
			$file = $imgs[0]->image_fullpath . "_thumb";

			$statusLabel = $this->getStatus($reservation_status);

			$printDate = $this->getPrintDate($dateFrom, $dateto, $days);

			$rentalLink = getRentalLink($rental_id, "Rental");
			$itemLink = getItemLink($item_id, $item_name);

			$button_pay = "";
			if($reservation_status == RESERVATION_ACEPTED){
				$button_pay = "<div id='reserv_$reservation_id' onclick=\"setInitPoint('$reservation_id', '$item_id', '$item_name')\" class='button_green'>Pagar reserva</div> <a href='initPoint' id='reserv_mp_$reservation_id' name='MP-Checkout'></a>";
			}

			$text = "<div class='reservation_box'>
						<div class='reservation_img'>
							<img src='$file'>
							<p>$statusLabel</p>
						</div>
						<div class='reservations_seeclient'><a href='$rentalLink'>Ver due&ntilde;o</a></div>
						<a href='$itemLink' class='profile_item_row_name'>$item_name</a>
						<div class='reservations_date'>$printDate</div>
						$days d&iacute;as - Precio por d&iacute;a: <strong>$$item_price</strong>
						<br><br>
						<div class='reservation_price'>
							Modifique el precio luego de arreglar con el due&ntilde;o
							<br>
							<strong>Total a pagar</strong> $<input style='color:#c00; font-weight: bold;' id='totalprice_$reservation_id' value='$totalPrice'>
							<div class='reservation_button_pay'>$button_pay</div>
						</div>
					</div>";
			
			echo $text;
		}
	}

	/**
	 * Shows only the questions yet to be answered
	 *
	 * @param Item $item
	 */
	function showMyItemsReservations($user_id) {
		$link = $this->getLink();

		$query = "SELECT * FROM reservation WHERE owner_id=$user_id AND reservation_status=0";
		$result = mysqli_query($link, $query);

		while(($row = mysqli_fetch_assoc($result)) != false){
			$item_id = $row['item_id'];
			$item_name = $row['item_name'];
			$user_id = $row['user_id'];
			$reservation_id = $row['reservation_id'];
			$reservation_status = $row['reservation_status'];
			$reservation_from = $row['reservation_from'];
			$reservation_to = $row['reservation_to'];
			
			$dateFrom = DateTime::createFromFormat('Y-m-d H:i:s', $reservation_from);
			$dateto = DateTime::createFromFormat('Y-m-d H:i:s', $reservation_to);
			$days = date_diff($dateFrom, $dateto)->format("%d");
			
			$printDate = $this->getPrintDate($dateFrom, $dateto, $days);

			$imgs = getItemImages($item_id, $_SESSION['user_id']);
			$file = $imgs[0]->image_fullpath . "_thumb";
			
			$user = getUserLink($user_id, "Cliente");
			$item = getItemLink($item_id, $item_name);
				
			$text = "<div class='reservation_box'>
							<div class='reservation_img'>
								<img src='$file'>
							</div>
			
							<div class='reservations_seeclient'><a href='$user'>Ver cliente</a></div>
				
							<a href='$item' class='profile_item_row_name'>$item_name</a>
							<div class='reservations_date'>$printDate ($days d&iacute;as)</div>
				
							<form action='/controllers/server/item/reservationController.php?event=updStat&reservation_id=$reservation_id&reservation_status=1' method='POST'>
								<input class='button_green' style='float:left; margin-right:10px;' type='submit' value='Aceptar'>
							</form>
							
							<form action='/controllers/server/item/reservationController.php?event=updStat&reservation_id=$reservation_id&reservation_status=2' method='POST'>
								<input class='button_green' type='submit' value='Rechazar'>
							</form>
					 </div>";

			echo $text;
		}

	}

	function getStatus($status){
		switch ($status) {
			case RESERVATION_ACEPTED:
				return "Reserva aceptada";
				break;
	
			case RESERVATION_REJECTED:
				return "Reserva rechazada";
				break;
					
			case RESERVATION_PENDING:
				return "Reserva pendiente";
				break;
	
			case RESERVATION_PAYED:
				return "Reserva aceptada y pagada";
				break;
	
			case RESERVATION_PAYED_PENDING:
				return "Reserva con pago en preceso";
				break;
	
			default:
				return "Error";
				break;
		}
	}

	function getPrintDate($dateFrom, $dateto, $days){
		$from = $this->dias[$dateFrom->format('w')]." ".$dateFrom->format('d')." de ".$this->meses[$dateFrom->format('n')-1];
		$to = $this->dias[$dateto->format('w')]." ".$dateto->format('d')." de ".$this->meses[$dateto->format('n')-1];

		return "Reserva desde <strong>$from</strong> al <strong>$to</strong>";
	}
	
	function getReservation($reservation_id){
		$query = "SELECT * from reservation WHERE reservation_id=$reservation_id";
		$result = mysqli_query($this->getLink(), $query);
		
		return mysqli_fetch_assoc($result);
	}

}




?>








