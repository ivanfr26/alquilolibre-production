<?php

$controller = new SandboxController();
$controller->savImgToLocal();


class SandboxController{

	/**
	 * Loops through the images and stores the new ones
	 */
	function savImgToLocal() {
		set_time_limit(8000000);

		$files = sizeof($_FILES['files']['name']);

		$itemId = $_POST['itemId'];
		
		
		for ($i = 0; $i < $files; $i++) {
			$name = $_FILES['files']['name'][$i];
			$tmp_name = $_FILES['files']['tmp_name'][$i];

			if (!file_exists("upload/" . $name)){
				$path = "upload/$itemId";
				
				if($this->createDirectoryForUpload($path)){
					$this->storeFile($tmp_name, $name, $path);
				}
			}
		}
	}

	/**
	 * Creates a directory if it doesn't exists
	 * @param unknown $path
	 * @return boolean
	 */
	function createDirectoryForUpload($path) {
		if(!is_dir($path)){
			return mkdir($path, 0777);
		}else{
			return true;
		}
	}
	
	/**
	 * Stores a file in the desired directory
	 * @param unknown $tmp_name
	 * @param unknown $name
	 */
	function storeFile($tmp_name, $name, $path){
		move_uploaded_file($tmp_name, "$path/$name");
		
		echo "Stored in: " . "$path/$name <br>";
	}
}



?>