<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/itemManager.php';
include_once $path . 'controllers/server/tools.php';

controlRentalLogguedIn();
addNewItem();

/**
 * Adds the new item to the DB
 */
function addNewItem() {
	$manager = new ItemManager();

	$user_id = $_SESSION['rental_id'];

	$item_name = $_REQUEST['item_name'];
	$item_description = $_REQUEST['item_description'];

	$item_address = $_REQUEST['item_address'];

	$item_geo = $_REQUEST['item_geolocation'];
	$item_geo = str_replace("(", '', $item_geo);
	$item_geo = str_replace(")", '', $item_geo);

	$item_conditions = $_REQUEST['item_conditions'];

	$item_price = $_REQUEST['item_price'];
	$item_priceType = $_REQUEST['item_priceType'];

	$city_id = $_REQUEST['city_id'];
	$category_id = $_REQUEST['category_id'];

	$isDummy = 0; //Not dummy

	$result = $manager->addNewItem($item_name,
									$item_description,
									$user_id,
									$city_id,
									$item_address,
									$item_geo,
									$item_conditions,
									$item_price,
									$item_priceType,
									$category_id,
									$isDummy);
	
	controlResult($result, $manager);
}

/**
 * If result is OK, reditrects
 * @param unknown $result
 */
function controlResult($result, $manager) {
	if($result != false){
		$arr = mysqli_fetch_assoc($result);
		
		include $_SERVER['DOCUMENT_ROOT'] . "/languages/es.php";
		header("Location: /pages/updateitem/?item_id=" . $arr['LAST_INSERT_ID()'] . "&notice=$lang_newitemOk&nType=ok");
		
	}else{
		echo mysqli_error($manager->getConnection());
	}
}

?>




