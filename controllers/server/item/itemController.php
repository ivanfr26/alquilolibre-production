<?php
if(!isset($_SESSION)){
	session_start();
}


/**
 * Get the item from db
 * @param int $item_id
 */
function getItem($item_id){
	$manager = new ItemManager();
	
	return $manager->read($item_id);
}


/**
 * Prints the Imgs of an item
 * @param Item $item
 */
function showImgs($images){
	$view = '';
	
	$view .= "<div id='item_imgs_thumbBlock'>";
		foreach ($images as $i => $img) {
			$view .= "<img class='item_imgs_thumb' id='item_imgs_thumb_$i' src='$img->image_fullpath' onclick='thumToMainImg(this.src)'>";
		}
	$view .= "</div>";

	$view .= "<div id='item_imgs_mainBlock'>
		 		<img id='thumbs_main_img' src='" . @$images[0]->image_fullpath . "'>
			 </div>";
	
	echo $view; 
}


function showItemsFromOwner($rental_id){
	$manager = new ItemManager();
	$mysqlResults = $manager->getItemPerUser($rental_id);
	
	$item = new Item();
	while ($item->getModelFromResult($item, $mysqlResults) != FALSE){

		$link = getItemLink($item->item_id, $item->item_name);
		$images = getItemImages($item->item_id, $item->user_id);;
		$priceLabel = getPriceLabel($item->item_priceType, $item->item_price);
		
		$view = "<div class='item_mores'>
					<div class='item_mores_img'>
						<a href='$link'>
							<img src='" . @$images[0]->image_fullpath . "'>
						</a>
					</div>
					<div class='item_mores_name'><a class='general_link_small' href='$link'>".substr($item->item_name, 0, 50)."</a></div>
					<div class='item_mores_price'><price>$$item->item_price</price> $priceLabel</div>
				</div>";
		
		echo $view;
	}
	
	
}









