<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/questionManager.php';
include_once $path . 'controllers/server/tools.php';

if(isset($_REQUEST['event'])){
	controlUserLogguedIn();
	
	$event = $_REQUEST['event'];
	
	if($event == 'add_question'){
		controlUserLogguedIn();
		
		if(strlen($_REQUEST['comment_text']) > 3){
			addQuestionToDb();
		}
		respondOk("Gracias por comentar!");
	}
	
	if($event == 'reply'){
		reply();
		return;
	}
}


function reply(){
	$manager = new QuestionManager();
	$manager->addReply($_REQUEST['comment_id'], $_REQUEST['comment_reply']);
}

function addQuestionToDb(){
	$item_id = $_REQUEST['item_id'];
	$user_id = $_SESSION['user_id'];
	$manager = new QuestionManager();
	$result = $manager->addQuestion($item_id, $_REQUEST['comment_text'], $user_id, $_REQUEST['comment_type']);
	
	return $result;
}

function showCommentsPerUser($user_id){
	$manager = new QuestionManager();
	$result = $manager->getCommentsPerUser($user_id);
	
	printUserComments($result);
}

function showAllQuestionsFromItem(Item $item){
	$manager = new QuestionManager();
	$result = $manager->getQuestions($item->item_id);
	
	printComments($result);
}


function printUserComments($result){
	include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';

	$i = 0;
	$hidden = '';

	$comment = new UserComment();
	while($comment->getModelFromResult($comment, $result) != false){
		echo "<div id='item_question_box_$i' class='item_comment_container' $hidden>

		<div class='item_comment_box'>
		<img class='question_bubble' src='/lib/images/" . $comment->userComment_type. "_commentImg.png'>

		<div class='item_comments_text'>$comment->userComment_text</div>

		<div class='item_feedback_rentee_data'>
		<div class='item_feedback_rentee_date'>" . $comment->user_name . ", </div>
		<div class='item_feedback_rentee_date'>" . deduceDate($comment->userComment_dateTime) . "</div>
		</div>
		</div>
			
		";
		
				if(strlen($comment->userComment_reply) > 1){
				echo "<div class='item_comment_response_box'>
				<img class='question_bubble' src='/lib/images/question_a.png'>
					
				<div class='item_comments_text'>$comment->userComment_reply</div>
				<div class='item_feedback_rentee_data'>" . deduceDate($comment->userComment_reply_dateTime) . "</div>
				</div>";
		}
			

		echo "</div>";

				if($i++ == 5){
		$hidden = 'style="display: none";';
	}
}
}

function respondOk($notice){
	gotUrl(getBackUrl(), $notice, "ok");
}


function printComments($result){
	include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
	
	$i = 0;
	$hidden = '';
	
	$comment = new Comment();
	while($comment->getModelFromResult($comment, $result) != false){
		echo "<div id='item_question_box_$i' class='item_comment_container' $hidden>
	
				<div class='item_comment_box'>
					<img class='question_bubble' src='/lib/images/" . $comment->itemComment_type. "_commentImg.png'>
						
					<div class='item_comments_text'>$comment->itemComment_text</div>
						
					<div class='item_feedback_rentee_data'>
						<div class='item_feedback_rentee_date'>" . $comment->user_name . ", </div>
						<div class='item_feedback_rentee_date'>" . deduceDate($comment->itemComment_dateTime) . "</div>
					</div>
				</div>
			
				";
			
				if(strlen($comment->itemComment_reply) > 1){
				echo "<div class='item_comment_response_box'>
				<img class='question_bubble' src='/lib/images/question_a.png'>
			
				<div class='item_comments_text'>$comment->itemComment_reply</div>
				<div class='item_feedback_rentee_data'>" . deduceDate($comment->itemComment_reply_dateTime) . "</div>
						</div>";
				}
			
	
				echo "</div>";
	
				if($i++ == 5){
				$hidden = 'style="display: none";';
		}
	}
}

function deduceDate($date){
	$current = date("Y-m-d H:i:s");
	$interval = date_diff(date_create($current), date_create($date));

	if($interval->m !== 0) {
		return $interval->format('%m meses atras');
	}else{
		return $interval->format('%d d&iacute;as atras');
	}
}

?>