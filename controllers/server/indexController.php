<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path.'/controllers/server/category/categoryController.php';
include_once $path.'/controllers/database/connectionManager.php';
include_once $path.'/controllers/database/models/rental.model.php';
include_once $path.'/controllers/database/models/item.model.php';

new IndexController(@$_REQUEST['event']);

class IndexController {

	function __construct($event){
	}

	function getLink(){
		$conn = new ConnectionManager();
		return $conn->getConnection();
	}
	
	function showCategoriesWithItems(){
		$categories = getChildrenCategories(38);
		$this->printCategories($categories);
	}
	
	function showCitiesWithItems(){
		$this->printCities(getCitiesWithItems());
	}
	
	function getBestProducts(){
		$link = $this->getLink();
		$query = "SELECT item_name, item_price, item_priceType, item_description, item_id, rental_id FROM item WHERE item_deleted=0 AND item_active=1 ORDER BY item_karma DESC, item_id DESC LIMIT 8";
		
		return mysqli_query($link, $query);
	}
	
	function getLatestRentals(){
		$link = $this->getLink();
		$query = "SELECT rental_name,rental_description, rental_id, user_id FROM rental WHERE rental_type=2 ORDER BY rental_id DESC LIMIT 6";
		
		return mysqli_query($link, $query);
	}

	
	function showBestProducts(){
		$view = '';
		$result = $this->getBestProducts();
	
		$item = new Item();
		while(($item->getModelFromResult($item, $result)) != false){
			$name = $item->item_name;
			
			$user_id = getUserIdFromRental($item->rental_id, $this->getLink());
			$imgs = getItemImages($item->item_id, $user_id);
			
			$src = @$imgs[0]->image_fullpath;
			
			$link = getItemLink($item->item_id, $name);

			$price = ($item->item_price == 0) ? '' : '$' . $item->item_price;
			
			$box = '<div class="index_item_box">
						<a href="'.$link.'">
							<img class="index_img_item" src="'.$src.'">
						</a>
			
						<a href="'.$link.'">
							<div class="index_img_item_label">'.$name.'</div>
						</a>
						<div class="index_price"><price>' . $price . '</price> ' . getPriceLabel($item->item_priceType, $item->item_price).'</div>
					</div>';
				
			$view .= $box;
		}
	
		echo $view;
	}
	
	function showLatestRentals(){
		$view = '';
		$result = $this->getLatestRentals();

		$rental = new Rental();
		while(($rental->getModelFromResult($rental, $result)) != false){
			$imgs = getUserImages($rental->user_id);
			$src = @$imgs[0]->image_fullpath;
			$shortDesc = substr($rental->rental_description, 0, 140) . "...";
			
// 			$catgory = showRentalCategories($rental->rental_id, $this->getLink());
// 			<div class="index_img_box_category">'.$catgory.'</div>
			
			$link = getRentalLink($rental->rental_id, $rental->rental_name);
			
			$box = '<div class="index_rental_box">
						<a href="'.$link.'">
							<img class="index_img" src="'.$src.'">
						</a>
					
						<a href="'.$link.'">
							<div class="index_img_label">
								'.$rental->rental_name.'
								<div class="index_label_desc">'.$shortDesc.'</div>
							</div>
						</a>
					</div>';
			
			$view .= $box;
		}
		
		echo $view;
	}
	
	
	/**
	 *
	 * @param unknown $categories
	 */
	function printCities($cities){
		while (($row = mysqli_fetch_array($cities))) {
			$name = $row['city_name'];
			$id = $row['city_id'];
	
			echo "<div class='index_filter'><a href='/pages/find/?&search_filter_cityId=$id&search_filter_cityName=$name'>$name</a></div>";
		}
	}
	
	/**
	 * 
	 * @param unknown $categories
	 */
	function printCategories($categories){
		$category = new Category();
		while ($category->getModelFromResult($category, $categories)) {
			$name = $category->category_name;
			$id = $category->category_id;
				
			echo "<div class='index_filter'><a href='/pages/find/?search_filter_categoryId=$id&search_filter_categoryName=$name'>$name</a></div>";
		}
	}
	
	
}



?>