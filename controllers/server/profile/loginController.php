<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/userManager.php';
include_once $path . 'controllers/database/MailManager.php';
include_once $path . 'controllers/server/tools.php';
include_once $path . "languages/es.php";

$controller = new LoginController();
$controller->event(@$_REQUEST['event']);

class LoginController{
	
	var $userManager;
	
	/**
	 * Constructor, we need to get a connection to DB
	 */
	function __construct() {
		$this->userManager = new UserManager();
	}
	

	function event($event) {
		
		if($event == 'login'){
			$this->controlUserLogued();
			$this->loginUser($this->userManager);
		}

// 		if($event == 'fblogin'){
// 			$this->controlUserLogued();
// 			$this->facebookLoginUser($this->userManager);
// 		}
		
		if($event == 'register'){
			$ok = $this->verifyParams($_REQUEST['user_email'], $_REQUEST['user_password']);
			
			if($ok){
				$this->controlUserLogued();
				$result = $this->registerNewUser($this->userManager, '0');
				$this->afterRegister($result, $this->userManager);
				return;
				
			}else{
				echo "REGISTER_FAIL";
				return;
			}
			
		}
		
		if($event == 'token'){
			$this->updateToken();
			return;
		}
		
		if($event == 'logout'){
			session_destroy();
			header('Location: ' . "/");
		}
		
	}

	function afterRegister($result, UserManager $manager){
		if($result == false){
			echo "REGISTER_FAIL: " . mysqli_error($manager->getConnection());
		}else{
			echo $this->notifyNewUser($result);			
		}
	}
	
	/**
	 * Logs an user in, verifies params
	 */
	function loginUser(UserManager $manager){
		$user_email = $_REQUEST['user_email'];
		$user_password = $_REQUEST['user_password'];
		
		$user = $manager->getUserDataForLogin($user_email, $user_password);
		
		if($user == false || $user->user_token != 'done'){
			return gotUrl("/pages/login/?", "Datos incorrectos, por favor intenta de nuevo", "fail");
		}else{
			$this->setSessionValues($user);
			return gotUrl("/pages/profile/?", "Bienvenido!", "ok");
		}
	}
	
	/**
	 * Logs in with Fb data, if not present, cretes a new user.
	 * @param UserManager $manager
	 */
	function facebookLoginUser(UserManager $manager){
		$user = $manager->getUserByField('user_facebookId', $_REQUEST['user_facebookId']);
		
		if($user != false){
			$this->setFacebookSessionValues($user);
			$this->respondRegisterOk();
			return;
			
		}else{
			$_REQUEST['user_email'] = $_REQUEST['user_facebookId'];
			
			$user_id = $this->registerNewUser($manager, $_REQUEST['user_facebookId']);
			$this->fbLogingAfterRegister($manager, $user_id);
			return;
		}
		
	}

	
	function fbLogingAfterRegister(UserManager $manager, $user_id){
		$user = $manager->getUserByField('user_id', $user_id);
		
		if($user != false){
			$this->setFacebookSessionValues($user);
			$this->respondRegisterOk();
			return;
		}else{
			echo "<br>Error while loggin.<br>";
			die(); 
		}
	}
	
	/**
	 * Registers a new user into the DB, verifies params
	 */
	function registerNewUser(UserManager $manager, $facebookId){
		$user_name = @$_REQUEST['user_name'];
		$user_lastname = @$_REQUEST['user_lastname'];
		$user_email = @$_REQUEST['user_email'];
		$user_password = @$_REQUEST['user_password'];
		
		$isPresent = $manager->getUserByField('user_email', $user_email);
		if($isPresent != false){
			echo "EMAIL_PRESENT";
			return false;
		}
		
		$result = $manager->registerUser($facebookId, $user_name, $user_lastname, $user_email, $user_password);
		
		if($result != false){
			$arr = mysqli_fetch_assoc($result);
			return $arr['LAST_INSERT_ID()']; 			
		}
		
		return $result;
	}
	
	
	function notifyNewUser($user_id){
		$user = $this->userManager->getUser($user_id);
		$mail = new MailManager();
			
		$to = $user->user_email;
		$subject = "AlquiloLibre.com - Activa tu cuenta";
		
		$body = "Bienvenido a AlquiloLibre! 
		Haz click en el siguiente link para activar tu cuenta:
		
		www.alquilolibre.com/?user_id=$user_id&token=$user->user_token";
			
		$mail->sendEmail('alquilolibre@alquilolibre.com', $to, $subject, $body);
	}

	
	function updateToken(){
		$user_id = $_REQUEST['user_id'];
		$token = $_REQUEST['user_token'];
		
		$user = $this->userManager->getUser($user_id);
		
		if($user->user_token == $token){
			$array = Array();
			$array['user_token'] = 'done';
			$this->userManager->updateUser($array, $user_id);

			redirect("Tu cuenta esta activa, ya puedes ingresar con tus datos", "ok");
		}else{
			redirect("Error al validar tu mail. Intenta de nuevo, si el error persiste, mandanos un mensaje y lo resolveremos en 5 minutos.", "fail");
		}
	}
	
	/**
	 * Verifies the length of the params
	 * 
	 * @param String $user_email
	 * @param String $user_password
	 * 
	 * @return boolean
	 */
	function verifyParams($user_email, $user_password){
		$emailLen = (strlen($user_email) > 6);
		$passLen = (strlen($user_password) > 1);
		
		return $emailLen && $passLen;
	}	
	
	
	
	/**
	 * Sets all the user data into the $_SESSION object
	 */
	function setFacebookSessionValues(User $user){
		$_SESSION['user_id'] = $user->user_id;
		$_SESSION['user_facebookId'] = $user->user_facebookId;
		$_SESSION['user_name'] = $user->user_name;
	}
	
	
	/**
	 * Sets all the user data into the $_SESSION object
	 */
	function setSessionValues(LoginUser $user){
		$_SESSION['user_id'] = $user->user_id;
		$_SESSION['user_facebookId'] = $user->user_facebookId;
		$_SESSION['user_name'] = $user->user_name;
		
		$_SESSION['user_isAdmin'] = $user->user_isAdmin;
		
		$_SESSION['rental_id'] = $user->rental_id;
		$_SESSION['rental_type'] = $user->rental_type;
		
		$_SESSION['rental_confirmed'] = $user->rental_confirmed;
	}
	
	
	/**
	 * Destroys all the $_SESSION data 
	 */
	function endSession(){
		session_destroy();
	}
	
	/**
	 * Verifies if an user is loged in or note
	 * 
	 * @param int $user_id
	 * @return boolean
	 */
	function isUserLoged(){
		if(isset($_SESSION['user_id'])){
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Closes current session if user is logued
	 */
	function controlUserLogued(){
		if($this->isUserLoged()){
			$this->endSession();
		}
	}
	
	/**
	 * Response for the incorrect login attempt
	 */
	function respondLoginFail(){
		echo "LOGIN_FAIL";
	}
	
	/**
	 * Response for the correct registration
	 */
	function respondRegisterOk(){
		echo "LOGIN OK";
	}
	
}


?>