<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/userManager.php';
include_once $path . 'controllers/database/MailManager.php';
include_once $path . 'controllers/server/profile/profileController.php';
include_once $path . 'controllers/server/tools.php';
include_once $path . "languages/es.php";

$controller = new LoginController();
$controller->event(@$_REQUEST['event']);

class LoginController{
	
	var $userManager;
	var $profileControler;
	
	/**
	 * Constructor, we need to get a connection to DB
	 */
	function __construct() {
		$this->userManager = new UserManager();
		$this->profileControler = new ProfileController('');
	}
	

	function event($event) {
		
		if($event == 'ask'){
			$this->askForUpgrade();
		}
	}
	
	
	function askForUpgrade(){
		$user = $this->profileControler->getLogedUserModel();
		$this->userManager->registerRental($user->user_id, $_REQUEST['rental_name'], '', '', '', 1, 1);
		
		$aux2 = Array();
		$aux2['user_phone'] = $_REQUEST['user_phone'];
		$aux2['city_id'] = $_REQUEST['city_id'];
		$this->userManager->updateUser($aux2, $user->user_id);
		
		$this->sendMailNotification($user->user_email);
		
		session_destroy();
		
		gotUrl('/pages/login/?', 'Muchas gracias! Ingresa nuevamente a tu cuenta, y podras publicar tus productos o servicios', 'Ok');
	}
	
	function sendMailNotification($email){
		$mail = new MailManager();
			
		$to = $email;
		$subject = "AlquiloLibre.com - Upgrade a Cuenta Negocios";
		$body = "Muchas gracias por tu interes en AlquiloLibre. Ya puedes publicar tus productos o servicios.";
		$mail->sendEmail('alquilolibre@alquilolibre.com', $to, $subject, $body);
	}
	
	
}


?>












