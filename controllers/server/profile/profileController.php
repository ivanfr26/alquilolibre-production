<?php
if(!isset($_SESSION)){session_start();}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once  $path . 'controllers/database/userManager.php';

new ProfileController(@$_REQUEST['event']);

class ProfileController{
	var $manager;
	
	function __construct($event){
		$this->manager = new UserManager();
		
		if($event == 'updateUser'){
			unset($_REQUEST['event']);
			$this->updateUserData($_REQUEST, $_SESSION['user_id']);
			return;
		}
		
		if($event == 'updateRental'){
			unset($_REQUEST['event']);
			$this->updateRentalData($_REQUEST, $_SESSION['rental_id']);
			return;
		}
		
		if($event == 'updateRentalCats'){
			$this->updateRentalCats();
			return;
		}
		
	}

	
	function updateRentalCats(){
		$rental_id = $_SESSION['rental_id'];
		
		$query = "DELETE FROM rentalCategory WHERE rental_id=$rental_id";
		mysqli_query($this->manager->getConnection(), $query);
		
		//Then insert the RentalCategories 1 to 2
		for ($i = 1; $i < 3; $i++) {
			$cat = $_REQUEST['rental_cat_' . $i];
			$query = "call sp_add_rentalCategory('$rental_id', '$cat')";
			mysqli_query($this->manager->getConnection(), $query);
		}
		
		echo "DATA_OK";
	}

	function updateRentalData($data, $rental_id){
		$result = $this->manager->updateRental($data, $rental_id);
		$this->sendResponse($result);
	
		echo "UPDATE_DONE";
	}
	
	function updateUserData($data, $user_id){
		$result = $this->manager->updateUser($data, $user_id);
		$this->sendResponse($result);

		echo "UPDATE_DONE";
	}
	
	function sendResponse($result){
		if($result == false){
			echo mysqli_error($this->manager->getConnection());
		}else{
			echo "DATA_OK";
		}
	}
	
	/**
	 * Returns the User object of the logged in user
	 * @return User
	 */
	function getLogedRentalModel() {
		$manager = new UserManager();
		$rental = $manager->getRental($_SESSION['rental_id']);
	
		return $rental;
	}
	
	/**
	 * Returns the User object of the logged in user
	 * @return User
	 */
	function getLogedUserModel() {
		$manager = new UserManager();
		$user = $manager->getUser($_SESSION['user_id']);
	
		return $user;
	}
	
	function showItems(){
		$manager = new UserManager();
	
		$result = $manager->getItemsPerRental();
		$manager->showItems($result);
	}
	
	function getFacebookStatus(User $user){
		$facebookId = $user->user_facebookId;
		
		if($facebookId != 0 && $facebookId != ''){
			echo '<div id="profile_fb_status" class="profile_verif_status_Ok">Conectado</div>';	
		}else{
			echo '<div id="profile_fb_status" class="profile_verif_status_Fail">No conectado</div>';
		}
	}
	
	

	/**
	 * Shows only the questions yet to be answered
	 *
	 * @param Item $item
	 */
	function showQuestionsToAnswer($user_id) {
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
	
		$query = "call sp_get_user_comments('$user_id')";
		$result = mysqli_query($this->manager->getConnection(), $query);

		$count = 0;
		$commentsToAnswer = '';
		while(($comment = mysqli_fetch_assoc($result)) != false){
			$commentType = $comment['userComment_type'];
			$comment_text = $comment['userComment_text'];
			$comment_reply = $comment['userComment_reply'];
				
			$replyLabel;
			if(strlen($comment_reply) > 0){
				$replyLabel = "Modificar";
			}else{
				$replyLabel = $lang_reply;
			}
			
			$count++;
			$comment_id = $comment['userComment_id'];

			$commentsToAnswer .= "<div class='profile_comment_box'>
									<img class='profile_comment_bubble' src='/lib/images/" . $commentType. "_commentImg.png'>
										<div class='profile_comment_text'>$comment_text</div>

										<form id='item_question_makeForm' action='/controllers/server/commentsController.php?event=replyUser&comment_id=$comment_id' method='post'>
											<textarea class='profile_reply' name='text'>$comment_reply</textarea>
											<input class='profie_reply_send' type='submit' value='$replyLabel'>
										</form>
										</div>";
		}

		echo "<div class='profile_box_title'>Mensajes recibidos <weak>($count)</weak></div>";
		echo $commentsToAnswer;
	}
	
	
	
	
	
	
	
}




?>






