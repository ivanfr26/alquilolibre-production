<?php

function getPriceLabel($item_priceType, $item_price){
	include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';

	if($item_price == 0){
		return 'Consultar precio';
	}
	
	$priceLabel;

	switch ($item_priceType) {
		case 1:
			$priceLabel = $lang_item_day;
			break;

		case 2:
			$priceLabel = $lang_item_week;
			break;

		case 3:
			$priceLabel = $lang_item_month;
			break;

		default:
			$priceLabel = $lang_item_hour;
			break;
	}

	return $priceLabel;
}


/**
 * Returns the value from a variable in the URL address (GET method)
 * @param unknown $name
 * @param unknown $default
 * @return unknown
 */
function getUrlVariableValue($name, $default){
	$req = @$_REQUEST[$name];

	if(strlen($req) > 0){
		return $req;
	}

	return $default;
}

/**
 *
 * @param unknown $avoid
 * @return string
 */
function getCurrentUrl($avoid){
	$current = "";

	foreach ($_REQUEST as $key => $value) {

		if(strstr($key, $avoid) == false){
			$current .= "$key=$value&";
		}
	}

	return $current;
}

/**
 * Returns string "<input type='hidden' name='$key' value='$value'>"; with all the URL variables.
 * Usefull for all 'html forms'.
 * @param string $avoid variables to avoid
 * @return string
 */
function getCurrentUrlVars($avoid){
	$currentFilters = "";

	foreach ($_REQUEST as $key => $value) {

		if(@strstr($key, $avoid) == false){
			$filter = "<input type='hidden' name='$key' value='$value'>";
			$currentFilters .= $filter;
		}
	}

	return $currentFilters;
}


function printFilter($filterId, $filterValue){
	$id = @$_REQUEST[$filterId];

	$view = '';

	if(strlen($id) > 0){
		$value = $_REQUEST[$filterValue];

		$view = "<div class='filter_section_filterBy'>
		<div class=\"filter_section_filterBy_name\">$value
		<img class=\"filter_section_filterBy_cross\" onclick=\"removeFilter('$filterId', '$filterValue')\" src='/lib/images/cross.png'>
		</div>
		</div>";
	}else{
		return false;
	}

	return $view;
}

/**
 * Returns if a user is logged In
 * @return boolean
 */
function isRentalLogued(){
	if(!isset($_SESSION)){
		session_start();
	}

	if(@$_SESSION['rental_id'] != 0 && @$_SESSION['rental_confirmed'] == 1){
		return true;
	}

	return false;
}

/**
 * Returns if a user is logged In
 * @return boolean
 */
function isUserLogued(){
	if(!isset($_SESSION)){
		session_start();
	}

	if(isset($_SESSION['user_id'])){
		return true;
	}

	return false;
}

/**
 * If the user is not logged is, redirects to login page
 * @param unknown $redirect
 */
function controlRentalLogguedIn(){
	if(isUserLogued() == false){
		redirect("Debes crear una cuenta para continuar", 'Ok');
		return;
	}

	if(isRentalLogued() == false){
		ffUserLogguedIn("/pages/upgrade/");
		return;
	}
}


/**
 * If the user is not logged is, redirects to login page
 * @param unknown $redirect
 */
function controlUserLogguedIn(){
	if(isUserLogued() == false){
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		redirect($lang_logInToContinue, 'Ok');
	}
}


function gotUrl($url, $notice, $nType){
	header("location: $url&notice=$notice&nType=$nType");
	die();
}

function getBackUrl(){
	return $_SERVER['HTTP_REFERER'] . "?";
}

function redirect($notice, $nType){
	header("location: /pages/login/?notice=$notice&nType=$nType");
	die();
}

/**
 * If the user is ogged is, redirects to page
 * @param unknown $redirect
 */
function ffUserLogguedIn($redirect){
	if(isUserLogued() == true){
		header("location: $redirect");
		die();
	}
}


function printNotice(){
	if(isset($_REQUEST['notice'])){
		$message = $_REQUEST['notice'];

		$notice = "<div id='all_notice' class='notice_".$_REQUEST['nType']."' onclick=\"fadeOut(this.id)\">
		<div class='all_notice_cross'>x</div>
		$message
		</div>";

		echo $notice;
	}
}

function showRentalCategories($rental_id, $link){
	$view = '';
	$result = getRentalCategories($rental_id, $link);

	while(($row = mysqli_fetch_assoc($result)) != false){
		$id = $row['category_id'];
		
		if($id != CATEGORY_ID_GENERAL_TOP){
			$name = $row['category_name'];
			$view .= "<div class='item_categoty_level' onclick=\"goSearchCategory('$id', '$name')\">$name</div> <pipe> | </pipe> ";
		}
	}

	return $view;
}


function cleanStringForDb($string){
	$i = 0;
	$inp = Array();
	$out = Array();

	$inp[$i] = "'";
	$out[$i] = "&#39;";
	$i++;

	$inp[$i] = '"';
	$out[$i] = "&#34;";
	$i++;

	$string = str_replace($inp, $out, $string);
	$string = str_replace("\n", '<br />', $string);
	$string = preg_replace('/\s+/', ' ', trim($string));

	return $string;
}

function cleanStringJs($string) {
	$i = 0;
	$inp = Array();
	$out = Array();

	$inp[$i] = "&#39;";
	$out[$i] = "'";
	$i++;

	$inp[$i] = "&#34;";
	$out[$i] = '\"';
	$i++;

	$string = str_replace($inp, $out, $string);
	$string = str_replace('<br />', '\n', $string);
	
	return $string;
}

/**
 * Makes a call to the DB, and returns an Array of ItemImgs()
 *
 * @param int $item_id
 * @param int $owner_id
 *
 * @return multitype:
 */
function getUserImages($user_id){
	include_once $_SERVER['DOCUMENT_ROOT'] . '/controllers/database/models/image.model.php';
	include_once $_SERVER['DOCUMENT_ROOT'] . '/controllers/database/connectionManager.php';

	$link = new ConnectionManager();

	$query = "call sp_get_user_images('$user_id')";
	$result = mysqli_query($link->getConnection(), $query);

	$images = Array();

	$img = new Image();
	while($img->getModelFromResult($img, $result) != false){
		$img->image_fullpath = S3_IMAGES_PATH . "/upload/$user_id/profile/imgs/$img->image_name";

		array_push($images, $img);
		$img = new Image();
	}

	@mysqli_free_result($result);

	return $images;
}

/**
 * Makes a call to the DB, and returns an Array of ItemImgs()
 *
 * @param int $item_id
 * @param int $owner_id
 *
 * @return multitype:
 */
function getItemImages($item_id, $owner_id){
	include_once $_SERVER['DOCUMENT_ROOT'] . '/controllers/database/models/itemImg.model.php';
	include_once $_SERVER['DOCUMENT_ROOT'] . '/controllers/database/connectionManager.php';

	$link = new ConnectionManager();

	$query = "call sp_get_item_images('$item_id')";
	$result = mysqli_query($link->getConnection(), $query);

	$images = Array();

	$img = new ItemImg();
	while($img->getModelFromResult($img, $result) != false){
		$img->image_fullpath = S3_IMAGES_PATH . "/upload/$owner_id/items/$item_id/$img->image_name";

		array_push($images, $img);
		$img = new ItemImg();
	}

	@mysqli_free_result($result);

	return $images;
}

/**
 * If an error ocurred, prints and dies.
 * @param unknown $result
 * @param unknown $link
 */
function controlDbResul($result, $query, $link){
	if($result == false){
		echo "<br><b>$query</b><br> Error: " . mysqli_error($link);
		die();
	}
}


function getRentalCategories($rental_id, $link){
	$query = "call sp_get_rental_categories('$rental_id')";
	return mysqli_query($link, $query);

	controlDbResul($result, $query, $link);
}

/**
 * Prints the Imgs of an item
 * @param Item $item
 */
function showImgsEdit($images, $type){
	$view = '';

	$i = 0;
	foreach ($images as $img) {
		$deleteButton = "<div class='windows_button'>
		<div class='updateItem_imgs_delMe' id='updateItem_imgs_delMe_$i'
		onclick=\"deleteImage('$img->image_fullpath', '$img->image_id', 'updateItem_imgs_thumb_$i', '$type')\">
		x
		</div>
		</div>";

		$view .= "<div class='updateItem_imgs_thumbBlock'>
		$deleteButton
		<img class='updateItem_imgs_thumb' id='updateItem_imgs_thumb_$i' src='$img->image_fullpath'>
		</div>";
			
		$i++;
	}

	//Complete with empty images until the limit
	for ($extra = $i; $extra < MAX_IMAGES_PER_USER; $extra++) {
		$view .= "<div class='updateItem_imgs_thumbBlock'>
		<img class='updateItem_imgs_thumb' id='updateItem_imgs_thumb_$extra' src='/lib/images/noimg.png'>
		</div>";
	}

	echo $view;
}


/**
 * Creates a random string
 * @param unknown $length
 * @return string
 */
function rand_string( $length ) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	$str = '';

	$size = strlen( $chars );
	for( $i = 0; $i < $length; $i++ ) {
		$str .= $chars[ rand( 0, $size - 1 ) ];
	}

	return $str;
}

function getUserIdFromRental($rental_id, $link){
	$query = "SELECT user_id FROM rental WHERE rental_id=$rental_id";
	$result = mysqli_query($link, $query);
	
	$aux = mysqli_fetch_assoc($result);
	
	mysqli_free_result($result);
	mysqli_close($link);
	
	return $aux['user_id'];
}

/**
 * Prints all categories inside a select
 * @param unknown $selected
 */
function printCats($selected, $link){
	$query = "select * from `category` ORDER BY  `category`.`category_l` ASC";
	$result = mysqli_query($link, $query);

	printOptions($result, 'category_name', 'category_id', 'Otros', $selected);
}


/**
 * Prints the results inside a select
 * @param unknown $result
 * @param unknown $name
 * @param unknown $id
 * @param unknown $ignore
 * @param unknown $selected
 */
function printOptions($result, $name, $id, $ignore, $selected){
	while(($row = mysqli_fetch_assoc($result)) != false){

		$isSelect = "";
		if($row[$name] == $selected){
			$isSelect = 'selected="selected"';
		}else{
			$isSelect = '';
		}

		if($row[$name] != $ignore){
			echo "<option $isSelect value='".$row[$id]."'>".$row[$name]."</option>";
		}
	}
}


function getItemName($raw){
	$name = substr($raw, stripos($raw, "/") + 1);
	
	return str_replace('-', " ", $name);
}


function getItemLink($id, $name){
	$linkName = str_replace(' ', '-', $name);
	$link = "/product/?$id/$linkName";
	
	return $link;
}

function getRentalLink($id, $name){
	$linkName = str_replace(' ', '-', $name);
	$link = "/store/?$id/$linkName";

	return $link;
}

function getUserLink($id, $name){
	$linkName = str_replace(' ', '-', $name);
	$link = "/pages/user/?$id/$linkName";

	return $link;
}

/**
 * Counts the comments not responded done to the User
 * @param unknown $user_id
 * @param unknown $link
 */
function getReservationsCount($user_id, $link){
	$query = "SELECT reservation_status FROM reservation WHERE owner_id=$user_id OR user_id=user_id";
	$result = mysqli_query($link, $query);

	$count = "";
	while(($row = mysqli_fetch_assoc($result)) != false){
		$status = $row['reservation_status'];

		if($status == 0){
			$count++;
		}
	}

	return $count;
}

/**
 * Counts the comments not responded done to the User
 * @param unknown $user_id
 * @param unknown $link
 */
function getUnrespondedCommentsPerItems($user_id, $link){
	$query = "select itemComment_reply from itemComment where user_id=$user_id";
	$result = mysqli_query($link, $query);

	$count = "";
	while(($comment = mysqli_fetch_assoc($result)) != false){
		$comment_reply = $comment['itemComment_reply'];

		if(strlen($comment_reply) == 0){
			$count++;
		}
	}

	return $count;
}


/**
 * Counts the comments not responded done to the User
 * @param unknown $user_id
 * @param unknown $link
 */
function getUnrespondedCommentsPerUser($user_id, $link){
	$query = "call sp_get_user_comments('$user_id')";
	$result = mysqli_query($link, $query);
	
	$count = "";
	while(($comment = mysqli_fetch_assoc($result)) != false){
		$comment_reply = $comment['userComment_reply'];
	
		if(strlen($comment_reply) == 0){
			$count++;
		}
	}
	
	return $count;
}



?>


















