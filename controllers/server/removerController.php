<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';

$up = new RemoverController();
$up->delFile();

class RemoverController{

	var $UPLOAD_FOLDER;
	
	function __construct(){
		if(!isset($_SESSION['user_id'])){
			include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
			header("location: /pages/login/?discl=$lang_logInToContinue&ff=/pages/updateitem/?" . getCurrentUrl(''));
			return;
		} 

	}
	
	/**
	 * Loops through the images and stores the new ones
	 */
	function delFile() {
		set_time_limit(8000000);

		if(!$this->isLoggedUserOwner()){
			return;
		}
		
		$this->deleteFileFromS3();
	}

	function deleteFileFromS3(){
		$img_id = $_REQUEST['img_id'];
		$type = $_REQUEST['type'];
		
// 		$this->deleteFileFromS3(); FIXME revisar porque no se borran de S3 aws.

		if($type == 'item'){
			$this->updateItemData($img_id);
			return;
		}

		if($type == 'user'){
			$this->updateUserData($img_id);
			return;
		}
	}

	/**
	 * Deleste the img from S3
	 */
	function delFromAws(){
 		$uri = $_REQUEST['uri'];
		if (!class_exists('S3'))require_once('S3.php');
		$s3 = new S3("AKIAI7YYXN4WSCGBVA6A", "C6m6MJ0xeS4P1gcpnSv7o5iMODR2t9nmvTzN8IQk");

		if ($s3->deleteObject("alquilolibre_data_upload", $uri)) {
			echo "Deleted file: $uri";
		}
	}
	
	/**
	 * deletes str from DB
	 * @param unknown $item_id
	 * @param unknown $imgsOK
	 */
	function updateItemData($img_id){
		$query = "call sp_del_itemImage('$img_id')";
	
		$connection = new ConnectionManager();
		mysqli_query($connection->getConnection(), $query);
	}
	
	
	/**
	 * deletes str from DB
	 * @param unknown $item_id
	 * @param unknown $imgsOK
	 */
	function updateUserData($img_id){
		$query = "call sp_del_userImage('$img_id')";
	
		$connection = new ConnectionManager();
		mysqli_query($connection->getConnection(), $query);
	}
	
	/**
	 * Checks if the logged user is the owner of the file
	 * @return boolean
	 */
	function isLoggedUserOwner() {
		$uri = $_REQUEST['uri'];
		$user = $_SESSION['user_id'];
		
		if(strstr($uri, "upload/$user/") == false){
			return false;
		}
		
		return true;
	}
}


?>