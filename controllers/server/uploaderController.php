<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/server/SimpleImage.php';
include_once $path .'controllers/database/connectionManager.php';
include_once $path .'controllers/server/tools.php';
include_once $path .'global.php';

new UploaderController(@$_REQUEST['event']);

class UploaderController{

	var $IMG_MAX_SIZE;
	var $UPLOAD_FOLDER;
	
	function __construct($event){
		$this->IMG_MAX_SIZE = 8 * 1024 * 1024;  //in bytes
		
		if($event == 'upload'){
			$user_id = $_SESSION['user_id'];
			
			$this->UPLOAD_FOLDER = $_SERVER['DOCUMENT_ROOT'] . "/upload/$user_id/items/";
			$this->uploadFiles($user_id, @$_REQUEST['item_id']);
		}
		
		if($event == 'uploadBackOffice'){
			$user_id = $_REQUEST['user_id'];
			
			$this->UPLOAD_FOLDER = $_SERVER['DOCUMENT_ROOT'] . "/upload/$user_id/items/";
			$this->uploadFiles($user_id, '');
		}
	}
	
	/**
	 * Loops through the images and stores the new ones
	 */
	function uploadFiles($user_id, $item_id) {
		$type = $_REQUEST['type'];
		
		$images; 
		$path;
		
		if($type == 'item'){
			$path = "upload/$user_id/items/$item_id";
			$images = getItemImages($item_id, $user_id);
		}else if($type == 'user'){
			$path = "upload/$user_id/profile/imgs";
			$images = getUserImages($user_id);
		}
		
		if(sizeof($images) >= MAX_IMAGES_PER_USER){
			return;
		}
		
		set_time_limit(8000000);

		if (!class_exists('S3'))require_once('S3.php');
		$s3 = new S3('AKIAI7YYXN4WSCGBVA6A', 'C6m6MJ0xeS4P1gcpnSv7o5iMODR2t9nmvTzN8IQk');
		$s3->putBucket("alquilolibre_data_upload", S3::ACL_PUBLIC_READ);
		
		
		
		$imgsOK = Array();
		
		$count = 0;
		foreach ($_FILES as $fileObj) {
			$name = $fileObj['name'];
			$tmp_name = $fileObj['tmp_name'];

			$size = $fileObj['size'];
			
			$this->ceateThumbImg($tmp_name);
			$this->resizeImg($tmp_name);
			
			if($size < $this->IMG_MAX_SIZE){
				$result = $this->storeFile($tmp_name, $name, $path, $s3);
				$result = $this->storeFile($tmp_name . "_thumb", $name . "_thumb", $path, $s3);
				
				if($result){
					array_push($imgsOK, $name);
				}
			}
			
			$count++;
			if($count == 6){
				break;
			}
		}
		
		if($type == 'item'){
			$this->updateItemData($item_id, $imgsOK);
		}else if($type == 'user'){
			$this->updateUserData($user_id, $imgsOK);
		}
	}

	/**
	 * Resizes Images to hardcoded size
	 *
	 * @param unknown $file
	 */
	function ceateThumbImg($file){
		$MAX_IMG_WIDTH = 150;
	
		$image = new SimpleImage();
		$image->load($file);
		$image->resizeToWidth($MAX_IMG_WIDTH);
		$image->save($file . "_thumb");
	}
	
	/**
	 * Resizes Images to hardcoded size
	 * 
	 * @param unknown $file
	 */
	function resizeImg($file){
		$MAX_IMG_WIDTH = 400;
		
		$image = new SimpleImage();
		$image->load($file);
		$image->resizeToWidth($MAX_IMG_WIDTH);
		$image->save($file);
	}
	
	
	/**
	 * Updates the DB TODO
	 * @param int $item_id
	 * @param string $imgsOK
	 */
	function updateUserData($user_id, $imgsOK){
		$connection = new ConnectionManager();
	
		foreach ($imgsOK as $img_name) {
			$query = "call sp_add_userImage('$user_id',  '$img_name');";
			mysqli_query($connection->getConnection(), $query);
		}
	}
	
	/**
	 * Updates the DB TODO
	 * @param int $item_id
	 * @param string $imgsOK
	 */
	function updateItemData($item_id, $imgsOK){
		$connection = new ConnectionManager();
		
		foreach ($imgsOK as $img_name) {
			$query = "call sp_add_itemImage('$item_id', '$img_name')";
			mysqli_query($connection->getConnection(), $query);
		}
	}
	
	/**
	 * Stores a file in the desired directory
	 * And echoes the OK results
	 * 
	 * @param unknown $tmp_name
	 * @param unknown $name
	 */
	function storeFile($fileTempName, $fileName, $path, $s3){
		//move the file
		if ($s3->putObjectFile($fileTempName, "alquilolibre_data_upload", "$path/$fileName", S3::ACL_PUBLIC_READ)) {
			if(strstr($fileName, "thumb") != false){
				echo "http://alquilolibre_data_upload.s3.amazonaws.com/$path/$fileName,";
			}
			return true;
		}else{
			return false;
		}
	}
}



?>