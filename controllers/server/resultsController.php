<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/resultsManager.php';
include_once $path . 'controllers/database/models/filters/filter.model.php';
include_once $path . 'controllers/database/models/result.model.php';


/**
 * Calls the manager with a new search quary 
 */
function showResultsAndFilters(){
	$manager = new ResultsManager();
	$manager->manageNewSearchQuery();
	
	return $manager;
}

/**
 * Calls the manager with a new search quary
 */
function showResultsAndFiltersRentals(){
	$manager = new ResultsManager();
	$manager->manageNewSearchQueryRentals();

	return $manager;
}

function getNoResultsStr() {
	include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
	$text = "<div id='results_no_results_message'>$lang_noResults</div>";
	
	return $text;
}



?>