<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/" . 'controllers/database/models/base.model.php';

class LoginUser extends BaseModel{
	var $user_name;
	var $user_id;
	var $user_email;
	var $user_isAdmin;
	var $rental_id;
	var $rental_type;
	var $user_facebookId;
	var $rental_confirmed;
	var $user_token;
}

?>