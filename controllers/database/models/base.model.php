<?php
	
class BaseModel{
	
	
	/**
	 * Gets a property inside the model
	 * @param unknown $key
	 * @param unknown $value
	 */
	public function getProperty($key) {
		return $this->$key;
	}
	
	/**
	 * Sets a property inside the model
	 * @param unknown $key
	 * @param unknown $value
	 */
	public function setProperty($key, $value) {
		if(strlen($value) == 0){
			$value = '-';
		}
		
		$this->$key = $value;
	}
	
	/**
	 * Injects the MySQL result into an $model instance
	 *
	 * @param
	 *        	result MySQL result
	 * @return mixed the BaseModel or false if no more results
	 */
	public function getModelFromResult(BaseModel $model, $result) {
		if($result == FALSE){
			return FALSE;
		}
		
		$row = mysqli_fetch_assoc($result);
	
		if($row == FALSE){
			return FALSE;
		}
		
		$size = sizeof($row);
		for ($i = 0; $i < $size; $i++) {
			$key = key($row);
			$model->setProperty($key, $row[$key]);
			next($row);
		}
	
		return $model;
	}

	/**
	 * Injects the MySQL result into an array of Basemodel objects
	 *
	 * @param result MySQL results
	 * @return array of BaseModel objects
	 */
	public function getArrayModelFromResult($result) {
		$j=0;
		$dev = array();
		while(($row = $result->fetch_assoc() )!=false){
			$model = new BaseModel();
			$size = sizeof($row);
			for ($i = 0; $i < $size; $i++) {
				$key = key($row);
				$model->setProperty($key, $row[$key]);
				next($row);
			}
			$dev[$j] = $model;
			$j++;
		}
	
		return $dev;
	}
	
	/**
	 * Constructs an string with ready
	 * to inject in the MySQL queries (UPDATE or INSERT).
	 *
	 * @param BaseModel into an assoc array
	 * @return String `key`='value', `key`='value'...
	 */
	function getMySqlKeyValueStrFromArray($array) {
		$query = "";
	
		foreach ($array as $key => $value) {
			if($key == 'user_password'){
				$value = sha1($value);
			}
			
			$query .= "`$key`='$value', ";
		}
	
		//Removes the las ,
		$query .= "END";
		$query = str_replace(", END", "", $query);
	
		return $query;
	}
}
?>