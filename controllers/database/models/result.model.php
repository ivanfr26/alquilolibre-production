<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/" . 'controllers/database/models/base.model.php';

class Result extends BaseModel{
	
	var $item_id;
	var $item_name;
	var $item_description;
	
	var $item_geolocation;
	var $item_address;
	
	var $item_price;
	var $item_priceType;
	
	var $user_id;
	var $rental_id;
	
	var $city_name;
	var $city_id; 
	var $province_name;
	var $province_id;
	
	var $rentalHistory_count;
	
	var $category_path;
	
	var $item_dummy;
}

?>