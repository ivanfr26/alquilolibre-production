<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/" . 'controllers/database/models/base.model.php';

class Item extends BaseModel{
	var $item_id;
	var $item_name;
	var $item_description;
	var $rental_id;
	var $user_id;
	var $item_conditions;
	var $item_active;
	var $item_creationDate;
	var $item_price;
	var $item_priceType;
	
	var $category_path;
	
	var $city_name;
	var $province_name;
	var $city_id;
	var $province_id;
	
	var $item_address;
	var $item_geolocation;
	
	var $comments_good;
	var $comments_bad;
	var $comments_neutral;
	var $comments_all;
}

?>