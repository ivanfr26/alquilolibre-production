<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/" . 'controllers/database/models/base.model.php';

class User extends BaseModel{
	var $user_id;
	var $user_facebookId;
	var $user_registerDate;
	var $user_name;
	var $user_lastname;
	var $user_email;
	var $user_password;
	var $user_birthday;
	var $city_id;
	var $city_name;
	var $province_id;
	var $province_name;
	var $country_id;
	var $country_name;
	var $user_address;
	var $user_geolocation;
	var $user_deleted;
	var $user_phone;
	var $user_isAdmin;
	var $user_token;
	
	var $comments_good;
	var $comments_neutral;
	var $comments_bad;
	var $comments_all;
}

?>