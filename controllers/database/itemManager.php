<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path . 'controllers/database/models/item.model.php';

class ItemManager{
	private $connection;
	
	/**
	 * Constructor, we need to get a connection to DB
	 */
	function __construct(){
		$connectionManager = new ConnectionManager();
		$this->connection = $connectionManager->getConnection();
	}	
	
	/**
	 * Return the connection to database
	 */
	function getConnection(){
		return $this->connection;
	}
	
	/**
	 * Calls the SP to add a new item
	 * @param unknown $item_name
	 * @param unknown $item_description
	 * @param unknown $user_id
	 * @param unknown $city_id
	 * @param unknown $item_address
	 * @param unknown $item_geo
	 * @param unknown $item_conditions
	 * @param unknown $item_price
	 * @param unknown $item_priceType
	 * @param unknown $category_id
	 * @param unknown $isDummy
	 * @return unknown
	 */
	function addNewItem($item_name, $item_description, $rental_id, $city_id, $item_address, $item_geo, $item_conditions, $item_price, $item_priceType, $category_id, $isDummy){
		$item_name = cleanStringForDb( $item_name);
		$item_description = cleanStringForDb( $item_description);
		$item_conditions = cleanStringForDb( $item_conditions);
		
		$query = "call sp_add_item('$item_name', '$item_description', '$rental_id', '$city_id', '$item_address', '$item_geo', '$item_conditions', '$item_price', '$item_priceType', '$category_id', '$isDummy');";
		$result = mysqli_query($this->getConnection(), $query);

		if(!$result){
			echo "<br>query: " . $query . "<br>";
			echo mysqli_error($this->getConnection()) . "<br>";
		}
		
		return $result;
	}
	
	
	/**
	 * Calls the SP to update an item's data
	 * 
	 * @param unknown $item_name
	 * @param unknown $item_description
	 * @param unknown $user_id
	 * @param unknown $city_id
	 * @param unknown $item_address
	 * @param unknown $item_geo
	 * @param unknown $item_conditions
	 * @param unknown $item_price
	 * @param unknown $item_priceType
	 * @param unknown $category_id
	 * @param unknown $isDummy
	 * @return unknown
	 */
	function updateItemData($item_id,$item_name, $item_description, $city_id, $item_address, $item_geo,$isActive, $item_conditions, $item_price, $item_priceType, $category_id, $isDummy){
		$item_name = cleanStringForDb($item_name);
		$item_description = cleanStringForDb( $item_description);
		$item_conditions = cleanStringForDb( $item_conditions);
		
		$query = "call sp_upd_item('$item_id', '$item_name', '$item_description', '$city_id', '$item_address', '$item_geo','$isActive', '$item_conditions', '$item_price', '$item_priceType', '$category_id', '$isDummy');";
		$result = mysqli_query($this->getConnection(), $query);
		
		if(!$result){
			echo "<b>$query</b><br>" . mysqli_error($this->getConnection());
			die();
		}
		
		return $result;
	}
	
	/**
	 * Validate that the item to modify belongs to the logged user
	 * 
	 * @param int $item_id
	 * @return boolean or redirects to login page
	 */
	function validateUserOperation($item_id){
		$item = $this->read($item_id);
		
		if($item->user_id == $_SESSION['user_id']){
			return true;
		}else{
			header("location: /pages/login/?discl=Desbes%20ingresar%20a%20tu%20cuenta,%20Gracias!&");
			die();
		}
	}
	
	/**
	 * Select a item from DB
	 * @param int $id
	 */
	function read($id){
		$query = "call sp_get_item_data('$id')";
		$result = mysqli_query($this->getConnection(), $query);
		
		if (! $result) {
			echo mysqli_error($this->getConnection());
		}
		
		$item = new Item();
		$item->getModelFromResult($item, $result);
		
		mysqli_next_result($this->getConnection());
		mysqli_free_result($result);
		
		return $item;
	}

	/**
	 * TODO make sp
	 * @param Item $item
	 */
	function delete($item_id, $user_id){
		$this->validateUserOperation($item_id);
		
		$query = "call sp_del_item('$item_id')";
		$result = mysqli_query($this->getConnection(), $query);
		
		return $result;
	}
	
	/**
	 * TODO make sp
	 * @param Item $item
	 */
	function activate($item_id, $active, $user_id){
		$this->validateUserOperation($item_id);
	
		$query = "call sp_set_item_active('$item_id', '$active')";
		
		$result = mysqli_query($this->getConnection(), $query);
	
		return $result;
	}
	
	/**
	 * Genric fucntion to UPDATE to INSERT items into the DB
	 * @param unknown $type
	 * @param unknown $array
	 * @param unknown $item_id
	 * @return mixed
	 */
	private function genericItemModify($type, $array, $item_id=-1) {
		$item = new Item();
		$values = $item->getMySqlKeyValueStrFromArray($array);
	
		$query = "$type `item` SET $values";
		if($item_id!=-1)
		{
			$query .= " WHERE `item_id`='" . $item_id . "'" ;
		}
		
		$result = mysqli_query($this->getConnection(), $query);
	
		return $result;
	}
	
	function getItemPerUser($rental_id){
		$query = "call sp_get_rental_items_for_rent('$rental_id', '1')";
		$result = mysqli_query($this->getConnection(), $query);
		
		return $result;
	}

}
?>










