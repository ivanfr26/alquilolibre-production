<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path . 'controllers/database/models/category.model.php';

//This is the managers for country, province and cities

class LocationManager{
	private $connection;
	
	/**
	 * Constructor, we need to get a connection to DB
	 */
	function __construct(){
		$connectionManager = new ConnectionManager();
		$this->connection = $connectionManager->getConnection();
	}

	/**
	 * Return the connection to database
	 */
	function getConnection(){
		return $this->connection;
	}
	
	function searchCountry(){
		$query = "SELECT * FROM `country` ORDER BY  `country`.`country_name` ASC";
		$result = mysqli_query($this->getConnection(), $query);
		
		if (! $result) {
			echo mysqli_error($this->getConnection());
		}
		
		return $result;
	}
	function searchProvince($country){
		$query = "SELECT * FROM `province` where country_id=$country ORDER BY  `province`.`province_name` ASC";
		$result = mysqli_query($this->getConnection(), $query);
	
		if (! $result) {
			echo mysqli_error($this->getConnection());
		}
	
		return $result;
	}
	function searchCities($province){
		$query = "SELECT * FROM `city` where province_id=$province ORDER BY  `city`.`city_name` ASC";
		$result = mysqli_query($this->getConnection(), $query);
	
		if (! $result) {
			echo mysqli_error($this->getConnection());
		}
	
		return $result;
	}
}
?>