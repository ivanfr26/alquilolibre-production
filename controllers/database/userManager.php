<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path .'controllers/database/models/user.model.php';
include_once $path .'controllers/database/models/rental.model.php';
include_once $path .'controllers/database/models/loginUser.model.php';
include_once $path .'controllers/database/models/item.model.php';
include_once $path .'controllers/database/questionManager.php';

class UserManager {
	private $connection;
	
	function getConnection(){
		$connectionManager = new ConnectionManager();
		return $this->connection = $connectionManager->getConnection();
	}
	
	
	/**
	 * Returns an UserModel when the email and password are OK, and match only one user.
	 * Or false otherwise.
	 * 
	 * @param String $user_email
	 * @param String $user_password
	 * @return boolean|User
	 */
	function getUserDataForLogin($user_email, $user_password) {
		$passEncrypted = sha1($user_password);
		
		$link = $this->getConnection();
		$query = "CALL sp_get_user_login('$user_email','$passEncrypted')";
		$result = mysqli_query($link, $query);
	
		//Needed now for the already present users. TODO: sh1() para todos.
		if($result == false || mysqli_num_rows($result) == 0){
			$link = $this->getConnection();
			echo "$query --> no encrypt: ";
			$query = "CALL sp_get_user_login('$user_email','$user_password')";
			$result = mysqli_query($this->getConnection(), $query);
		}
		
		if($result == false){
			echo "error: " . mysqli_error($link);
			return false;
		}
		
		if(mysqli_num_rows($result) != 1){
			echo "results: " . mysqli_num_rows($result) . "/ query: $query / ";
			return false;
		}
		
		$user = new LoginUser();
		$user->getModelFromResult($user, $result);
		
		return $user;
	}

	/**
	 * Registers a new user into the database.
	 *
	 * TODO verify if user is not present
	 *
	 * @param unknown $user_name
	 * @param unknown $user_lastname
	 * @param unknown $user_email
	 * @param unknown $user_password
	 * @return unknown
	 */
	function registerRental($user_id, $rental_name, $rental_description, $rental_hour, $rental_website, $rental_type, $rental_confirmed){
		$query = "INSERT INTO `rental`(`user_id`, `rental_name`, `rental_description`, `rental_hour`, `rental_website`, `rental_type`, `rental_confirmed`)
		VALUES ('$user_id', '$rental_name', '$rental_description', '$rental_hour', '$rental_website', '$rental_type', '$rental_confirmed')";
		$result = mysqli_query($this->getConnection(), $query);
	
		controlDbResul($result, $query, $this->getConnection());
	
		return $result;
	}
	
	/**
	 * Registers a new user into the database.
	 * 
	 * TODO verify if user is not present
	 * 
	 * @param unknown $user_name
	 * @param unknown $user_lastname
	 * @param unknown $user_email
	 * @param unknown $user_password
	 * @return unknown
	 */
	function registerUser($user_facebookId, $user_name, $user_lastname, $user_email, $user_password){
		$passEncrypted = sha1($user_password);
		
		$token = $this->rand_string(180);
		$query = "call sp_add_user('$user_name', '$user_lastname', '$user_email', '$passEncrypted', '681', '$user_facebookId', '$token')";//Bariloche default
		$result = mysqli_query($this->getConnection(), $query);

		controlDbResul($result, $query, $this->getConnection());
		
		return $result;
	}
	
	/**
	 * Creates a random string
	 * @param unknown $length
	 * @return string
	 */
	function rand_string( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
		$str = '';
		
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}
	
	/**
	 * SELECTS only one user by the specified field.
	 *
	 * @param User $user
	 */
	function getUserByField($field_name, $field_value) {
		$query = "SELECT * FROM  `user` WHERE  `$field_name`='$field_value'";
		$result = mysqli_query($this->getConnection(), $query);
	
		if($result == false || mysqli_num_rows($result) != 1){
			echo mysqli_error($this->getConnection());
			return false;
		}
	
		$user = new User();
		$user->getModelFromResult($user, $result);
	
		return $user;
	}
	
	/**
	 * SELECTS only one user by the user_id.
	 *
	 * @param User $user
	 */
	function getRental($rental_id) {
		$link = $this->getConnection();
		$query = "CALL sp_get_rental_data('$rental_id')";
		$result = mysqli_query($link, $query);
	
		controlDbResul($result, $query, $link);
		
		if($result == false || mysqli_num_rows($result) != 1){
			return false;
		}
	
		$user = new Rental();
		$user->getModelFromResult($user, $result);
	
		return $user;
	}
	
	/**
	 * SELECTS only one user by the user_id.
	 *
	 * @param User $user        	
	 */
	function getUser($user_id) {
		$query = "CALL sp_get_user_data('$user_id')";
		$result = mysqli_query($this->getConnection(), $query);
		
		if($result == false || mysqli_num_rows($result) != 1){
			echo mysqli_error($this->getConnection());
			return false;
		}
		
		$user = new User();
		$user->getModelFromResult($user, $result);
		
		return $user;
	}

	/**
	 * Genric fucntion to UPDATE to INSERT users into the DB
	 * @param unknown $type
	 * @param unknown $array
	 * @param unknown $user_id
	 * @return mixed
	 */
	private function genericUserModify($type, $array, $user_id, $table) {
		$user = new User();
		$values = $user->getMySqlKeyValueStrFromArray($array);
		
		$query = "$type `$table` SET $values WHERE `".$table."_id`='" . $user_id . "'" ;
		$result = mysqli_query($this->getConnection(), $query);
		
		if($result ==  FALSE){
			echo "<b>$query</b><br>";
			echo "ERROR: " . mysqli_error($this->getConnection()) . "<br>";
		}
		
		return $result;
	}
	
	
	/**
	 * Updates the Rental in the DB.
	 * @param $array User model into an assoc array.
	 * @param $user_id the user_id
	 */
	function updateRental($array, $rental_id) {
		$result = $this->genericUserModify("UPDATE", $array, $rental_id, 'rental');
	
		return $result;
	}
	
	/**
	 * Updates the User in the DB.
	 * @param $array User model into an assoc array.
	 * @param $user_id the user_id
	 */
	function updateUser($array, $user_id) {
		$result = $this->genericUserModify("UPDATE", $array, $user_id, 'user');
		
		return $result;
	}


	/**
	 * Returns all the Items from one logged user
	 * @return unknown
	 */
	function getItemsPerRental(){
		$id = @$_SESSION['rental_id'];
		$query = "CALL sp_get_rental_items_for_rent('$id', '0')";
		$result = mysqli_query($this->getConnection(), $query);
	
		return $result;
	}
	
	/**
	 * Returns all the Items from one logged user
	 * @return unknown
	 */
	function getItemsPerUser(){
		$id = $_SESSION['user_id'];
		$query = "CALL sp_get_user_items_for_rent('$id')";
		$result = mysqli_query($this->getConnection(), $query);
		
		return $result;
	}
	
	/**
	 * Prints the items to the browser
	 * @param unknown $result
	 */
	function showItems($result){
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		
		$viewPublish = '';
		$viewNoPublish = '';
		
		$item = new Item();
		while(($item = $item->getModelFromResult($item, $result)) != false){
			include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
			
			$id = $item->item_id;
			$name = $item->item_name;
			$price = $item->item_price;

			$images = getItemImages($item->item_id, $item->user_id);
			
			$file = '/lib/images/noimg.png';
			if(isset($images[0])){
				$file = $images[0]->image_fullpath;
			}
			
			$isActive = "Publicar";
			$activate = "1";
			if($item->item_active == 1){
				$isActive = "No publicar";
				$activate = "0";
			}
			
			$link = getItemLink($id, $name);
			
			$itemRow = "<div class='profile_item_row'>
							
						 	<img class='profile_item_thum' src='$file'>
						 	
						 	<div class='profile_item_row_block1'>
								<a href='$link' class='profile_item_row_name'>$name</a>
								<div class='profile_item_row_under'>Publicaci&oacute;n gratuita | #$id</div>
								
								<form action='/controllers/server/item/updateController.php?event=activate&item_id=$id&active=$activate' method='POST'>
									<input class='button_green' style='margin-top: 10px;' type='submit' value='$isActive'>
								</form>
							
							</div>
							
							<form action='/controllers/server/item/updateController.php?event=delete&item_id=$id' method='POST'>
								<input class='myitems_delete_call' type='submit' value='$lang_delete'>
							</form>

							<div class='button_green' style='float: right; margin-right: 20px;'>
								<a href=\"/pages/updateitem/?item_id=$id\">$lang_edit</a>
							</div>
							
							<div class='profile_item_row_price'><price>$$price</price> ".getPriceLabel($item->item_priceType, $item->item_price)."</div>
							
							<div class='profile_commentsToAnswer'>" . $this->appendQuestionsToAnswer($item) . "</div>
						</div>";
			
			
			if($item->item_active == 1){
				$viewPublish .= $itemRow;
			}else{
				$viewNoPublish .= $itemRow;
			}
			
		}
		
		echo "<div class='myitems_block'><div class='myitems_active_title'>$lang_myitems_Acitve</div>$viewPublish</div>";
		
		echo "<div class='myitems_block'><div class='myitems_active_title'>$lang_myitems_NoAcitve</div>
				<div id='myitems_no_desc'>$lang_no_desc</div>
				$viewNoPublish
			</div>";
	}
	
	
	/**
	 * Shows only the questions yet to be answered
	 * 
	 * @param Item $item
	 */
	function appendQuestionsToAnswer(Item $item) {
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		
		$manager = new QuestionManager();
		$comments = $manager->getQuestions($item->item_id);

		$commentsToAnswer = '';
		while(($comment = mysqli_fetch_assoc($comments)) != false){
			
			$commentType = $comment['itemComment_type'];
			$comment_text = $comment['itemComment_text'];
			$comment_reply = $comment['itemComment_reply'];
			
			if(strlen($comment_reply) == 0){
				$comment_id = $comment['itemComment_id'];
				
				$commentsToAnswer .= "<div class='profile_comment_box'>
										<img class='profile_comment_bubble' src='/lib/images/" . $commentType. "_commentImg.png'>
										<div class='profile_comment_text'>$comment_text</div>
										
										<form id='item_question_makeForm' action='/controllers/server/question/questionController.php?event=reply&comment_id=$comment_id' method='post'>
											<textarea class='profile_reply' name='comment_reply'></textarea>
											<input class='profie_reply_send' type='submit' value='$lang_reply'>
										</form>
										
									  </div>";
			}
		}
		
		return $commentsToAnswer;
		
	}
	
}

?>













