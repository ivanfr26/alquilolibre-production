<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path . 'controllers/database/models/category.model.php';

class CategoryManager{
	private $connection;
	
	/**
	 * Constructor, we need to get a connection to DB
	 */
	function __construct(){
		$connectionManager = new ConnectionManager();
		$this->connection = $connectionManager->getConnection();
	}

	/**
	 * Return the connection to database
	 */
	function getConnection(){
		return $this->connection;
	}

	
	function getCategoriesWithItems(){
		$query = "call sp_get_categories_with_items()";
		$result = mysqli_query($this->getConnection(), $query);
		
		return $result;
	}
	
	function getChildrenCategories($father_id){
		$query = "call sp_get_child_categories('$father_id')";
		$result = mysqli_query($this->getConnection(), $query);
		
		return $result;
	}
}
?>