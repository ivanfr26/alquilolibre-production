<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path . 'controllers/database/models/comment.model.php';
include_once $path . 'controllers/database/models/userComment.model.php';

class QuestionManager{
	var $question;


	/**
	 * Constructor, we need to get a connection to DB
	 */
	function __construct(){
		$connectionManager = new ConnectionManager();
		$this->connection = $connectionManager->getConnection();
	}

	/**
	 * Return the connection to database
	 */
	function getConnection(){
		return $this->connection;
	}

	/**
	 * Returns the mysqli object with the results
	 * @param unknown $item_id
	 */
	function getCommentsPerUser($user_id){
		$query = "call sp_get_user_comments('$user_id')";
		$result = mysqli_query($this->getConnection(), $query);
		
		controlDbResul($result, $query, $this->getConnection());
		
		return $result;
	}
	
	/**
	 * Returns the mysqli object with the results
	 * @param unknown $item_id
	 */
	function getQuestions($item_id){
		$query = "call sp_get_item_comments('$item_id')";
		$result = mysqli_query($this->getConnection(), $query);

		return $result;
	}
	
	/**
	 * Adds a question to the DB
	 */
	function addQuestion($item_id,$text,$user_id,$comment_type){
		$query = "CALL sp_add_itemComment_text('$item_id','$text','$user_id','$comment_type')";
		$result = mysqli_query($this->getConnection(), $query);
		
		return $result;
	}
	
	/**
	 * Adds a question to the DB
	 */
	function addReply($item_id, $reply){
		$query = "CALL sp_add_itemComment_reply('$item_id','$reply')";
	
		$result = mysqli_query($this->getConnection(), $query);
	
		if(!$result){
			echo "$query <br>";
			echo mysqli_error($this->getConnection()) . "<br>";
		}
	
		header("location: /pages/myitems/");
	}
}




?>





