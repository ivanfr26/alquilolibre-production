<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/models/filters/filter.model.php';
include_once $path . 'global.php';


class FiltersManager{
	
	private $cities;
	private $provinces;
	private $categories;
	
	function __construct(){
		$this->categories = Array();
		$this->cities = Array();
		$this->provinces = Array();
	}

	function extractFiltersFromResult(Result $result){
		$this->addCity($result);
		$this->addProvince($result);
		$this->addCategory($result);
	}
	
	
	function addCategoryDirectly($cat_id, $cat_name){
		$type = 'category';
		$this->categories = $this->addGenricFilter($this->getCategories(), $cat_name, $cat_id, $type);
	}
	
	function addCategory(Result $result) {
		$categories = explode(",", $result->category_path);

		$toAdd;
		if(isset($_REQUEST['search_filter_categoryId'])){
			$toAdd = @$categories[2];
		}else{
			$toAdd = @$categories[1];
		}
		
		$id = substr(strstr($toAdd, ".", false), 1);
		$name = strstr($toAdd, ".", true);
		
		if($id != CATEGORY_ID_GENERAL_TOP){
			$type = 'category';
			$this->categories = $this->addGenricFilter($this->getCategories(), $name, $id, $type);
		}
	}
	
	function addProvinceDirectly($name, $id) {
		$type = 'province';
		$this->provinces = $this->addGenricFilter($this->getProvinces(), $name, $id, $type);
	}
	
	function addProvince(Result $result) {
		$name = ucwords(strtolower($result->province_name));
		$id = $result->province_id;
		$type = 'province';
		
		$this->provinces = $this->addGenricFilter($this->getProvinces(), $name, $id, $type);
	}
	
	
	function addCityDirectly($name, $id){
		$type = 'city';
		$this->cities = $this->addGenricFilter($this->getCities(), $name, $id, $type);
	}
	
	function addCity(Result $result){
		$name = ucwords(strtolower($result->city_name));
		$id = $result->city_id;
		$type = 'city';
		
		$this->cities = $this->addGenricFilter($this->getCities(), $name, $id, $type);
	}
	
	
	function addGenricFilter($list, $name, $id, $type){
		$aux = new FilterBase();
			
		if(isset($list[$name])){
			
			$aux = $list[$name];
			$aux->count++;
			
		}else{
			$aux->name = $name;
			$aux->id = $id;
			$aux->type = $type;
			 
			$aux->count++;
		}
		
		$list[$name] = $aux;
		
		return $list;
	}
	
	function getCities(){
		return $this->cities;
	}
	
	function getProvinces(){
		return $this->provinces;
	}
	
	function getCategories(){
		return $this->categories;
	}
	
}

?>