<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/models/result.model.php';
include_once $path . 'controllers/database/models/rental.model.php';

class ResultsViewer{
	
	var $geolocations;
	var $mapPoints;
	
	function __construct(){
		$this->geolocations = Array();
		$this->mapPoints = Array();
	}

	function getResultsRentalList($mysqlResults){
		$resultsList = "";
	
		$result = new Rental();
		while ($result->getModelFromResult($result, $mysqlResults) != FALSE){
			$resultsList .= $this->getResultBoxRental($result);
			$this->addRentalToMap($result);
		}
	
		mysqli_free_result($mysqlResults);
	
		return $resultsList;
	}
	
	
	function getResultsItemsList($mysqlResults){
		$resultsList = "";
		
		$result = new Result();
		while ($result->getModelFromResult($result, $mysqlResults) != FALSE){
			$resultsList .= $this->getResultBox($result);
			$this->addItemToMap($result);
		}
		
		@mysqli_free_result($mysqlResults);
		
		return $resultsList;
	}
	
	
	function getResultBox(Result $result){
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		
		$shortDesc = str_replace("<br />", " ", substr($result->item_description, 0, 140))  . "...";
		$link = getItemLink($result->item_id, $result->item_name);
		$images = getItemImages($result->item_id, $result->user_id);
		
		$file = '/lib/images/img_icon.png';
		if(isset($images[0])){
			$file = $images[0]->image_fullpath;
		}
		
		$priceNumber = '';
		if($result->item_price != 0){
			$priceNumber = '$' . $result->item_price;
		}
		
		$resultBox = "
		<div class='result_block'>
			<div class='result_block_price'>
				<div class='result_block_price_price'>$priceNumber</div>
				<div class='result_block_price_label'>".getPriceLabel($result->item_priceType, $result->item_price)."</div>
			</div>
				
			<a class='a_container' href='$link'>
				<img class='result_block_img' src='/lib/images/load_icon.gif' data-original='$file'>
			</a>
				
			<div class='result_block_main_data'>
				<a class='result_block_title' href='$link'>
					$result->item_name
				</a>
			</div>
			
			<div class='result_block_references'>
				<div class='result_block_references_count'>$result->comments_all</div>
				<div class='result_block_references_label'>$lang_Feedback</div>
			</div>
			
			<div class='result_block_shortDesc'>$shortDesc</div>
		</div>
		";
		
		return $resultBox;
	}
	

	function getResultBoxRental(Rental $rental){
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';

		$shortDesc = substr($rental->rental_description, 0, 140) . "...";
		
		$file = '/lib/images/user_icon.png';
		$images = getUserImages($rental->user_id);
		if(isset($images[0])){
			$file = $images[0]->image_fullpath;
		}
	
		$link =  getRentalLink($rental->rental_id, $rental->rental_name);
		
		$resultBox = "
		<div class='result_block'>
		
			<a class='a_container' href='$link'>
				<img class='result_block_img' src='/lib/images/load_icon.gif' data-original='$file'>
			</a>
			
			<div class='result_block_main_data'>
				<a class='result_block_title' href='$link'>
					$rental->rental_name
				</a>
			</div>
			
			<div class='result_block_shortDesc'>$shortDesc</div>
		</div>
		";
	
		return $resultBox;
	}
	

	
	function addRentalToMap(Rental $rental){
		$link =  getRentalLink($rental->rental_id, $rental->rental_name);
		
		$title = "<a class='map_title' href='$link'>$rental->rental_name</a>";
		$street = "<div>$rental->user_address</div>";
		
		$text = $title . $street;
		$this->addMapGenericPoint($text, $rental->user_geolocation);
	}
	
	
	/**
	 * Adds a generic point to the map
	 * @param unknown $text
	 * @param unknown $geolocation
	 */
	function addMapGenericPoint($text, $geolocation){
		$r = array_push($this->geolocations, $geolocation);
		array_push($this->mapPoints, $text);
	}
	

	
	function addItemToMap(Result $result){
		$link = getItemLink($result->item_id, $result->item_name);
		
		$price = '';
		if($result->item_price != 0){
			$price = '$' . $result->item_price;
		}
		
		$priceMap = "<div class='map_price_num'>$price</div><div class='map_price_text'>" . getPriceLabel($result->item_priceType, $result->item_price) . "</div>";
		$mapBox = "<a class='map_title' href='$link'>$result->item_name</a><div class='map_price'>$priceMap</div>";
			
		$this->addMapGenericPoint($mapBox, $result->item_geolocation);
	}
	
	
	
	/**
	 * Gets the markers to be shown
	 * @param unknown $result
	 * @param unknown $geolocations
	 * @param unknown $geoAddress
	 */
	function getResultsOnLoadActions() {
		$mapPointsJavascript = '';
		foreach ($this->geolocations as $index => $geolocation) {
			$mapPointsJavascript .= ' markers['.$index.']="' . $geolocation . '";';
			$mapPointsJavascript .= ' address['.$index.']="' . cleanStringJs($this->mapPoints[$index]) . '"; ';
		}
		
		$mapFunction = "<script type='text/javascript'>
							window.onload = function(){
								$mapPointsJavascript
								
								ShowMapResult(markers,address);
								var i =  document.getElementById('resutls_list_container').clientHeight;
								document.getElementById('results_filters_container').style.height = i;
								
								$('img.result_block_img').lazyload();
								
								//Create a new jQuery.Event object without the new operator.
  								var e = jQuery.Event('scroll');

							    // trigger an artificial click event
							    jQuery('body').trigger( e );
							};
						</script>";
		
		return $mapFunction;
	}

	/**
	 * Gets the pager
	 *  
	 * @param unknown $totalResults
	 * @param unknown $currentPage
	 */
	function getPager($totalResults, $currentPage){
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		
		$totalPages = ceil($totalResults / (RESULTS_ITEMS_SIZE));

		$urlSearch = "/pages/find/?" . getCurrentUrl("search_page") . '&search_page=';
		
		$pages = '';
		for ($pageNumber = $currentPage; $pageNumber < $totalPages; $pageNumber++) {
			$pages .= "<div id='page_$pageNumber' class='result_pager_box'><a href='$urlSearch$pageNumber'>".($pageNumber + 1)."</a></div>";
			
			if($pageNumber == RESULTS_PAGES_LIMIT){
				break;
			}
		}
		
		$previous = '';
		if($currentPage > 0){
			$previous = "<div id='pager_previous' class='result_pager_box'><a href='$urlSearch" . ($currentPage - 1) . "'> < $lang_previous</a></div>";
		}
		
		$next = '';
		if($currentPage < $totalPages-1){
			$next = "<div id='page_next' class='result_pager_box'><a href='$urlSearch" . ($currentPage + 1) . "'> $lang_next > </a></div>";
		}
		
		$pager = "<div id='result_pager'>
					$previous $pages $next
				  </div>";
		
		return $pager;
	}
}

?>















