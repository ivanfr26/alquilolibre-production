<?php
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include $path .'controllers/database/connectionManager.php';
include $path .'controllers/database/filtersManager.php';
include $path .'controllers/database/resultsViewer.php';
include_once $path . 'controllers/database/models/result.model.php';

class ResultsManager{
	private $connection;
	private $filters;

	/**
	 * Constructor, we need to get a connection to DB
	*/
	function __construct(){
		$connectionManager = new ConnectionManager();
		$this->connection = $connectionManager->getConnection();
		
		$this->filters = new FiltersManager();
	}

	function getFilters(){
		return $this->filters;
	}
	
	/**
	 * Return the connection to database
	 */
	function getConnection(){
		$connectionManager = new ConnectionManager();
		return $connectionManager->getConnection();
	}

	
	function getSort() {
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		
		$view = '';
		
		$view .= "<div id='sort_block_options'>";
			
		$view .= "<div id='results_sort_title'>$lang_sortBy</div>";
		
		$view .= "<div class='results_sort_by' ".$this->isSelected('')." onclick=\"sortBy('')\">$lang_magic</div>";
		$view .= "<div class='results_sort_sep'>|</div>";
		
		$view .= "<div class='results_sort_by' ".$this->isSelected(2)." onclick=\"sortBy('2')\">$lang_mostExpensives</div>";
		$view .= "<div class='results_sort_sep'>|</div>";
		
		$view .= "<div class='results_sort_by' ".$this->isSelected(3)." onclick=\"sortBy('3')\">$lang_lessExpensives</div>";
		
		$view .= "</div>";
		
		return $view;
	}
	
	function isSelected($code){
		if(@$_REQUEST['search_sort'] == $code){
			return "id='results_sort_by_selected'";
		}
	}

	/**
	 * Receives the search params of a new search query, and reurns the resutls and the list
	 */
	function manageNewSearchQueryRentals(){
		$viewer = new ResultsViewer();
	
		$count = $this->extractCatsFromRental($this->getRentals(true));
	
		//opens results_list_container
		$results = "<div id='resutls_list_container'>";
	
		if(@$_REQUEST['mapview'] == 'true'){
			$results .= '<div id="bigmap_canvas_wrap">
								<div id="map_canvas_result"></div>
						 		<div id="map_bigger" onclick="mapSmaller()">Reducir mapa</div>
					     </div>';
		}
	
		if($count == 0){
			$results .= getNoResultsStr();
		}else{
			$results .= $viewer->getResultsRentalList($this->getRentals(false));
			$results .= "<div id='results_sep_rental'></div>";
		}
	
		//Closes results_list_container
		$results .= "</div>";
	
		$results = str_ireplace(@$_REQUEST['search_input_what'], '<strong>' . @$_REQUEST['search_input_what'] . '</strong>', $results);

		$currentPage = isset($_REQUEST['search_page']) ? $_REQUEST['search_page'] : 0;
		$results .= $viewer->getPager($count, $currentPage);

		$results .= $viewer->getResultsOnLoadActions();

		$this->showAllResults($count, $results);
	}
	
	
	
	/**
	 * Receives the search params of a new search query, and reurns the resutls and the list
	 */
	function manageNewSearchQuery(){
		$viewer = new ResultsViewer(); 
		
		$count = $this->extractAllFilters($this->getItems(true));

		//opens results_list_container
		$results = "<div id='resutls_list_container'>";
		
		if(@$_REQUEST['mapview'] == 'true'){
			$results .= '<div id="bigmap_canvas_wrap">
								<div id="map_canvas_result"></div>
						 		<div id="map_bigger" onclick="mapSmaller()">Reducir mapa</div>
					     </div>';
		}
		
		if($count == 0){
			$results .= getNoResultsStr();
		}else{
			$results .= $viewer->getResultsItemsList($this->getItems(false));
			$results .= "<div id='results_sep_rental'></div>";
		}
		
		//Closes results_list_container
		$results .= "</div>";

		$results = str_ireplace(@$_REQUEST['search_input_what'], '<strong>' . @$_REQUEST['search_input_what'] . '</strong>', $results);

		$currentPage = isset($_REQUEST['search_page']) ? $_REQUEST['search_page'] : 0;
		$results .= $viewer->getPager($count, $currentPage);
		
		$results .= $viewer->getResultsOnLoadActions();
		
		$this->showAllResults($count, $results);
	}
	
	/**
	 * Prints the Results Page to the browser
	 * @param unknown $results
	 */
	function showAllResults($count, $results){
		echo "<div id='results_count'>$count resultados".$this->getSort()."</div>";
		echo $this->showFilters();
		echo $results;
	}
	
	
	function showFilters(){
		$filters = $this->getFilters();
	
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'languages/es.php';
		include $_SERVER['DOCUMENT_ROOT'] . "/" . 'templates/blocks/filters.tpl.php';
	}
	
	/**
	 * Search items SP
	 * 
	 * @param bool $onlyCount if true, only counts
	 * @return mysqli object
	 */
	function getItems($onlyCounts){
		$what 		= @$_REQUEST['search_input_what'];
		
		$cityId 	= @$_REQUEST['search_filter_cityId'];
		$provinceId = @$_REQUEST['search_filter_provinceId'];
		$categoryId = @$_REQUEST['search_filter_categoryId'];
		$minPrice 	= @$_REQUEST['search_filter_prices_min'];
		$maxPrice 	= @$_REQUEST['search_filter_prices_max'];
		
		$sort 		= @$_REQUEST['search_sort'];//0: random, 1: date desc, 2: price asc, 3: price desc
		
		$page 		= @$_REQUEST['search_page'];
		
		$query;
		if($onlyCounts){
			$query = $this->getCountItemsQuery($what, $cityId, $provinceId, $minPrice, $maxPrice, $categoryId, $sort);
		}else{
			$query = $this->getSearchItemsQuery($what, $cityId, $provinceId, $minPrice, $maxPrice, $categoryId, $sort, $page);
		}
		
		$link = $this->getConnection();
		
		$result = mysqli_query($link, $query);
		$this->debug($result, $query, $link);
			
		$page = @$_REQUEST['search_page'];

		return $result;
	}

	/**
	 * Search items SP
	 *
	 * @param bool $onlyCount if true, only counts
	 * @return mysqli object
	 */
	function getRentals($onlyCounts){
		$what 		= @$_REQUEST['search_input_what'];
	
		$cityId 	= @$_REQUEST['search_filter_cityId'];
		$provinceId = @$_REQUEST['search_filter_provinceId'];
		$categoryId = @$_REQUEST['search_filter_categoryId'];
	
		$sort 		= @$_REQUEST['search_sort'];//0: random, 1: date desc, 2: price asc, 3: price desc

		$page = @$_REQUEST['search_page'];
		
		$link = $this->getConnection();

		$query;
		if($onlyCounts){
			$query = "CALL `sp_get_search_rentals_count`('$what','$cityId' , '$provinceId' , '$categoryId');";
		}else{
			$query = "CALL `sp_get_search_rentals`('$what','$cityId' , '$provinceId' , '$categoryId', '$page');";
		}
		
		$result = mysqli_query($link, $query);
		$this->debug($result, $query, $link);
		
		return $result;
	}
	
	/**
	 * Calls the SP to search items
	 *
	 * @param unknown $what
	 * @param unknown $cityId
	 * @param unknown $provinceId
	 * @param unknown $minPrice
	 * @param unknown $maxPrice
	 * @param unknown $categoryId
	 * @param unknown $sort
	 * @param unknown $page
	 * @return unknown
	 */
	function getCountItemsQuery($what, $cityId, $provinceId, $minPrice, $maxPrice, $categoryId, $sort){
		$query = "CALL sp_get_search_items_count('$what', '$cityId', '$provinceId', '$minPrice', '$maxPrice', '$categoryId', '$sort')";
		return $query;
	}
	
	
	/**
	 * Calls the SP to search items
	 * 
	 * @param unknown $what
	 * @param unknown $cityId
	 * @param unknown $provinceId
	 * @param unknown $minPrice
	 * @param unknown $maxPrice
	 * @param unknown $categoryId
	 * @param unknown $sort
	 * @param unknown $page
	 * @return unknown
	 */
	function getSearchItemsQuery($what, $cityId, $provinceId, $minPrice, $maxPrice, $categoryId, $sort, $page){
		$query = "CALL sp_get_search_items('$what', '$cityId', '$provinceId', '$minPrice', '$maxPrice', '$categoryId', '$sort', '$page')";
		return $query;
	}
	
	/**
	 * Prints the error and query in case of failure
	 * @param unknown $result
	 * @param unknown $query
	 */
	function debug($result, $query, $link){
		if(!$result){
			echo "query: <b>$query</b> <br>";
			echo "error: " . mysqli_error($this->getConnection()) . "<br>";
		}		
	}

	/**
	 * Extracts the filters from the results count
	 * and returns the amoutn of results
	 * @param mysqli $resultsCount
	 */
	function extractAllFilters($mysqlResults){
		$count = 0;
		$result = new Result();
		while ($result->getModelFromResult($result, $mysqlResults) != FALSE){
			$this->getFilters()->extractFiltersFromResult($result);
			$count++;
		}
		
		@mysqli_free_result($mysqlResults);
		
		return $count;
	}
	
	
	function extractCatsFromRental($results){
		$count = 0;
		while (($row = mysqli_fetch_assoc($results)) != FALSE){
			$cats_perRental = getRentalCategories($row['rental_id'], $this->getConnection()); 
			
			while (($row2 = mysqli_fetch_assoc($cats_perRental)) != FALSE){
				$cat_id = $row2['category_id'];
				$cat_name = $row2['category_name'];
				
				$left = $row2['category_l'];
				$rigth = $row2['category_r'];
				
				if(isset($_REQUEST['search_filter_categoryId'])){
					//only shows children
					if(($rigth - $left) == 1){
						$this->getFilters()->addCategoryDirectly($cat_id, $cat_name);
					}
				}else{
					//only shows 1st level
					if(($rigth - $left) > 1){
						$this->getFilters()->addCategoryDirectly($cat_id, $cat_name);
					}
				}
			}
			
			$this->getFilters()->addProvinceDirectly($row['province_name'], $row['province_id']);
			$this->getFilters()->addCityDirectly($row['city_name'], $row['city_id']);
			
			$count++;
		}
		return $count;
	}
}

?>















