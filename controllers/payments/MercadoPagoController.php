<?php

if(!isset($_SESSION)){session_start();}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once  $path . 'global.php';
include_once  $path . 'lib/mercadopago/mercadopago.php';

new MercadoPagoController(@$_REQUEST['event']);

class MercadoPagoController{
	var $mp;

	function __construct($event) {
		$this->mp = new MP(CLIENT_ID, CLIENT_SECRET);
		
		if($event == 'getPayButton'){
			$reservation_id = $_REQUEST['reservation_id'];
			$item_id = $_REQUEST['item_id'];
			$item_name = $_REQUEST['item_name'];
			$price = $_REQUEST['reservation_price'] * 1;
			
			$preferenceResult = $this->getPreferencesResponse($reservation_id, $item_id, $item_name, $price);
			$initPoint = $preferenceResult["response"]["init_point"];
			
			echo $initPoint;
		}
	}

	function getInitPoint($reservation_id, $item_id, $item_name, $price){
		$preferenceResult = $this->getPreferencesResponse($reservation_id, $item_id, $item_name, $price);
		return $preferenceResult["response"]["init_point"];
	}
	
	function getPreferencesResponse($reservation_id, $item_id, $item_name, $price){
		
		$preference = array (
				"items" => array (
						array (
								"id"=> "$item_id",
						        "title"=> "$item_name",
						        "quantity"=> 1,
						        "unit_price"=> $price,
						        "currency_id"=> "ARS"
						)
				),
				
				"id" => "reserv_$reservation_id",
				
				"back_urls" => array (
					"success" => EXECUTION_ENV . "/controllers/server/item/reservationController.php?event=updStat&reservation_id=$reservation_id&reservation_status=3",
					"pending" => EXECUTION_ENV . "/controllers/server/item/reservationController.php?event=updStat&reservation_id=$reservation_id&reservation_status=3",
					"failure" => EXECUTION_ENV . "/pages/reservations/?notice=Error al procesar su pago&nType=fail"
				),
		);
		
		$preferenceResult = $this->mp->create_preference($preference);
		
		return $preferenceResult;
	}
}



?>

















