-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (x86_64)
--
-- Host: rentimus-db-public-main.cvxgjppxfasd.us-east-1.rds.amazonaws.com    Database: rentimus_db_01
-- ------------------------------------------------------
-- Server version	5.5.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `category_l` int(11) DEFAULT NULL,
  `category_r` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (38,'Alquileres',0,155),(92,'Inmuebles',1,20),(93,'Construcción e industria',21,40),(94,'Indumentaria',41,52),(95,'Electrónica y computación',53,64),(96,'Deportes y ocio',65,82),(97,'Servicios',83,100),(98,'Automóviles',101,112),(99,'Productos escolares y libros',113,122),(100,'Hogar y jardín',123,136),(101,'Eventos',137,154),(102,'Casas',2,3),(103,'Departamentos',4,5),(104,'Campos y chacras',6,7),(105,'Cocheras',8,9),(106,'Depósitos',10,11),(107,'Locales comerciales',12,13),(108,'Habitaciones',14,15),(109,'Alojamiento turístico',16,17),(110,'Otros',18,19),(111,'Obradores',22,23),(112,'Máquinas industriales',24,25),(113,'Herramientas',26,27),(114,'Niveladoras',28,29),(115,'Topadoras',30,31),(116,'Maquinaria agrícola',32,33),(117,'Elementos de seguridad',34,35),(118,'Baños químicos',36,37),(119,'Otros',38,39),(120,'Hombre',42,43),(121,'Mujer',44,45),(122,'Calzado',46,47),(123,'Niños y bebes',48,49),(124,'Otros',50,51),(125,'Audio y video',54,55),(126,'Fotografía y cine',56,57),(127,'Video juegos',58,59),(128,'Computación',60,61),(129,'Otros',62,63),(130,'Bicicletas',66,67),(131,'Montaña y camping',68,69),(132,'Escalada',70,71),(133,'Ski y snowboard',72,73),(134,'Caza y pesca',74,75),(135,'Instrumentos musicales',76,77),(136,'Profesionales',84,85),(137,'Profesores',86,87),(138,'Fletes',88,89),(139,'Remises',90,91),(140,'Catering',92,93),(141,'Animaciones',94,95),(142,'Belleza',96,97),(143,'Autos',102,103),(144,'Camionetas',104,105),(145,'Motos',106,107),(146,'Casas rodantes',108,109),(147,'Otros',110,111),(148,'Libros',114,115),(149,'Productos escolares',116,117),(150,'Productos de oficina',118,119),(151,'Otros',120,121),(152,'Calefacción y refrigeración',124,125),(153,'Electrodomésticos',126,127),(154,'Parque jardín y piletas',128,129),(155,'Muebles',130,131),(156,'Productos para bebes',132,133),(157,'Otros',134,135),(158,'Castillos inflables',138,139),(159,'Disfraces',140,141),(160,'Luz y sonido',142,143),(161,'Vajilla',144,145),(162,'Utilería',146,147),(163,'Choperas',148,149),(164,'Decoración',150,151),(165,'Otros',152,153),(166,'Deportes acuáticos',78,79),(167,'Otros',80,81),(168,'Otros',98,99);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY (`city_id`),
  KEY `city_province_idx` (`province_id`),
  KEY `city_province_idx1` (`province_id`),
  CONSTRAINT `city_province` FOREIGN KEY (`province_id`) REFERENCES `province` (`province_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=830 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (256,'Azul',1),(257,'Puan',1),(258,'La Matanza',1),(259,'Tigre',1),(260,'25 de Mayo',1),(261,'Trenque Lauquen',1),(262,'9 de Julio',1),(263,'Lanus',1),(264,'La Plata',1),(265,'Monte',1),(266,'Pehuajo',1),(267,'San Isidro',1),(268,'Pergamino',1),(269,'Alberti',1),(270,'Chascomus',1),(271,'Esteban Echeverria',1),(272,'Mercedes',1),(273,'Bahia Blanca',1),(274,'Merlo',1),(275,'Junin',1),(276,'Guamini',1),(277,'Lujan',1),(278,'Leandro N.Alem',1),(279,'Matanza',1),(280,'General Paz',1),(281,'San Vicente',1),(282,'Cañuelas',1),(283,'Almirante Brown',1),(284,'Cnl.de Marina  L.Rosales',1),(285,'Baradero',1),(286,'Saavedra',1),(287,'Brandsen',1),(288,'General Sarmiento',1),(289,'Tapalque',1),(290,'Saladillo',1),(291,'Magdalena',1),(292,'Gonzales Chaves',1),(293,'General Pinto',1),(294,'Navarro',1),(295,'Daireaux',1),(296,'Lobos',1),(297,'Coronel Dorrego',1),(298,'Adolfo Alsina',1),(299,'Colon',1),(300,'General Arenales',1),(301,'Lincoln',1),(302,'Villarino',1),(303,'Vicente Lopez',1),(304,'Bartolome Mitre',1),(305,'Exaltacion de la Cruz',1),(306,'Salto',1),(307,'Bragado',1),(308,'Zarate',1),(309,'Avellaneda',1),(310,'Ayacucho',1),(311,'San Andres de Giles',1),(312,'Tandil',1),(313,'Rivadavia',1),(314,'Patagones',1),(315,'Grl.Viamonte',1),(316,'Cnl.de Marina Leonardo Rosales',1),(317,'Balcarce',1),(318,'Tres Arroyos',1),(319,'General Villegas',1),(320,'Lomas de Zamora',1),(321,'Berisso',1),(322,'Juarez',1),(323,'Grl.Pueyrredon',1),(324,'Coronel Suarez',1),(325,'Escobar',1),(326,'Carlos Casares',1),(327,'Chivilcoy',1),(328,'Berazategui',1),(329,'Quilmes',1),(330,'Torquinst',1),(331,'3 de Febrero',1),(332,'Olavarria',1),(333,'Pellegrini',1),(334,'General Belgano',1),(335,'Florencio Varela',1),(336,'Salliquelo',1),(337,'Cnl.de Marina L.Rosales',1),(338,'Mar Chiquita',1),(339,'General Pueyrredon',1),(340,'Campana',1),(341,'Grl.Sarmiento',1),(342,'Rojas',1),(343,'Sarmiento',1),(344,'Carmen de Areco',1),(345,'Pila',1),(346,'Tres de Febrero',1),(347,'Moron',1),(348,'Castelli',1),(349,'Chacabuco',1),(350,'General Viamonte',1),(351,'Rauch',1),(352,'General Belgrano',1),(353,'Tornquist',1),(354,'Necochea',1),(355,'Marcos Paz',1),(356,'Carlos Tejedor',1),(357,'San Pedro',1),(358,'General Alvarado',1),(359,'San Nicolas',1),(360,'Hipolito Yrigoyen',1),(361,'Las Flores',1),(362,'Coronel Pringles',1),(363,'General San Martin',1),(364,'Benito Juarez',1),(365,'San Cayetano',1),(366,'Adolfo Gonzales Chaves',1),(367,'Dolores',1),(368,'San Antonio',1),(369,'Loberia',1),(370,'Ramallo',1),(371,'General Alvear',1),(372,'Ensenada',1),(373,'Tordillo',1),(374,'General Guido',1),(375,'General las Heras',1),(376,'General Juan Madariaga',1),(377,'General la Madrid',1),(378,'General Lavalle',1),(379,'General Rodriguez',1),(380,'General Vllegas',1),(381,'Adolfo Gonzalez Chaves',1),(382,'Coronel de Marina L.Rosales',1),(383,'Bolivar',1),(384,'San Fernando',1),(385,'Capitan Sarmiento',1),(386,'Roque Perez',1),(387,'Moreno',1),(388,'Laprida',1),(389,'Maipu',1),(390,'Echeverria',1),(391,'Pilar',1),(392,'Coronel de Marina L. Rosales',1),(393,'Suipacha',1),(394,'San Antonio de Areco',1),(395,'Barazategui',1),(396,'Alsina',1),(397,'Leandro N. Elem',1),(398,'Grl.San Martin',1),(399,'Coronel',1),(400,'Capayan',2),(401,'Andalgala',2),(402,'El Alto',2),(403,'Santa Rosa',2),(404,'Ancasti',2),(405,'Paclin',2),(406,'Santa Maria',2),(407,'Tinogasta',2),(408,'La Paz',2),(409,'Valle Viejo',2),(410,'Antofagasta de la Sierra',2),(411,'Belen',2),(412,'Capital',2),(413,'Fray Mamerto Esquiu',2),(414,'Poman',2),(415,'Ambato',2),(416,'Independencia',3),(417,'San Fernando',3),(418,'Primero de Mayo',3),(419,'Fray Justo Santa Maria de Oro',3),(420,'Sargento Cabral',3),(421,'General Guemes',3),(422,'Tapenaga',3),(423,'Chacabuco',3),(424,'Libertador Grl.San Martin',3),(425,'25 de Mayo',3),(426,'12 de Octubre',3),(427,'Comandante Fernandez',3),(428,'Quitilipi',3),(429,'Mayor Luis J.Fontana',3),(430,'Libertad',3),(431,'Bermejo',3),(432,'Almirante Brown',3),(433,'Meyor Luis J.Fontana',3),(434,'General Belgrano',3),(435,'Libertador Grl. San Martin',3),(436,'General Donovan',3),(437,'San Lorenzo',3),(438,'Lebertador Grl.San Martin',3),(439,'Maipu',3),(440,'O\'higgins',3),(441,'9 de Julio',3),(442,'Libertador San Martin',3),(443,'Rio Senguerr',4),(444,'Martires',4),(445,'Escalante',4),(446,'Gaiman',4),(447,'Sarmiento',4),(448,'Cushamen',4),(449,'Florentino Ameghino',4),(450,'Paso de Indios',4),(451,'Telsen',4),(453,'Gastre',4),(454,'Futaleufu',4),(455,'Tehuelches',4),(456,'Rawson',4),(457,'Biedma',4),(458,'Rio Cuarto',5),(459,'Totoral',5),(460,'Colon',5),(461,'Minas',5),(462,'Punilla',5),(463,'Juarez Celman',5),(464,'Marcos Juarez',5),(465,'San Justo',5),(466,'Tercero Arriba',5),(467,'Rio Seco',5),(468,'Capital',5),(469,'Santa Maria',5),(470,'San Alberto',5),(471,'Union',5),(472,'Pocho',5),(473,'Calamuchita',5),(474,'Ischilin',5),(475,'General San Martin',5),(476,'Rio Primero',5),(477,'Cruz del Eje',5),(478,'Grl.Roca',5),(479,'General Roca',5),(480,'Sobremonte',5),(481,'Rio Segundo',5),(482,'Tulumba',5),(483,'San Javier',5),(484,'Presidente Roque Saez Pe?????A',5),(485,'Presidente Roque Saenz Pe?????A',5),(486,'Cruz de Eje',5),(487,'Coronel Pringles',5),(488,'Rio Tercero',5),(489,'Cvalamuchita',5),(490,'San Roque',6),(491,'Monte Caseros',6),(492,'General Alvear',6),(493,'Curuzu Cuatia',6),(494,'San Martin',6),(495,'Mercedes',6),(496,'Saladas',6),(497,'Ituzaingo',6),(498,'Beron de Astrada',6),(499,'Bella Vista',6),(500,'San Luis del Palmar',6),(501,'Capital',6),(502,'Lavalle',6),(503,'Paso de los Libres',6),(504,'Goya',6),(505,'Empedrado',6),(506,'Sauce',6),(507,'General Paz',6),(508,'Santo Tome',6),(509,'San Miguel',6),(510,'Concepcion',6),(511,'Esquina',6),(512,'San Cosme',6),(513,'Itati',6),(514,'Mburucuya',6),(515,'Uruguay',7),(516,'Nogoya',7),(517,'Tala',7),(518,'Gualeguay',7),(519,'Diamante',7),(520,'Parana',7),(521,'Gualeguaychu',7),(522,'Colon',7),(523,'Victoria',7),(524,'Villaguay',7),(525,'Feliciano',7),(526,'Concordia',7),(527,'La Paz',7),(528,'Federacion',7),(529,'Federal',7),(530,'Castellanos',7),(531,'Pilagas',8),(532,'Patiño',8),(533,'Pilcomayo',8),(534,'Bermejo',8),(535,'Pirane',8),(536,'Formosa',8),(537,'Matacos',8),(538,'Ramon Lista',8),(539,'Pillagas',8),(540,'Laishi',8),(541,'Ledesma',9),(542,'Cochinoca',9),(543,'El Carmen',9),(544,'Tumbaya',9),(545,'Capital',9),(546,'Yavi',9),(547,'Humahuaca',9),(548,'Rinconada',9),(549,'Valle Grande',9),(550,'Susques',9),(551,'Santa Catalina',9),(552,'San Antonio',9),(553,'Santa Barbara',9),(554,'San Pedro',9),(555,'Tilcara',9),(556,'Ladesma',9),(557,'Hucal',10),(558,'Realico',10),(559,'Mara Co',10),(560,'Quemuquemu',10),(561,'Chical Co',10),(562,'Guatrache',10),(563,'Capital',10),(564,'Caleucaleu',10),(565,'Trenel',10),(566,'Utracan',10),(567,'Atreuco',10),(568,'Toay',10),(569,'Chapadleufu',10),(570,'Conelo',10),(571,'Puelen',10),(572,'Rancul',10),(573,'Quemu Quemu',10),(574,'Loventue',10),(575,'Conhelo',10),(576,'Catrilo',10),(577,'Chapaleufu',10),(578,'Ultracan',10),(579,'Chalileo',10),(580,'Lihuel Calel',10),(581,'Maraco',10),(582,'Caleu Caleu',10),(583,'Curaco',10),(584,'Limay Mahuida',10),(585,'Castro Barros',11),(586,'General San Martin',11),(587,'General Lavalle',11),(588,'Arauco',11),(589,'Gral.Angel V.Peñaloza',11),(590,'San Blas de los Sauces',11),(591,'Independencia',11),(592,'General Ocampo',11),(593,'Chilecito',11),(594,'Famatina',11),(595,'Grl.Juan Facundo Quiroga',11),(596,'Grl.Angel V. Peñaloza',11),(597,'General Belgrano',11),(598,'General Sarmiento',11),(599,'Capital',11),(600,'Gdor. Gordillo',11),(601,'General Angel Vicente Peñaloza',11),(602,'Gobernador Gordillo',11),(603,'Rosario Vera Peñaloza',11),(604,'Grl.San Martin',11),(605,'General la Madrid',11),(606,'General Juan Facundo Quiroga',11),(607,'Generalocampo',11),(608,'Sanagasta',11),(609,'San Rafael',12),(610,'Lujan',12),(611,'Malargue',12),(612,'Guaymallen',12),(613,'Lavalle',12),(614,'Junin',12),(615,'Tupungato',12),(616,'Rivadavia',12),(617,'Maipu',12),(618,'Godoy Cruz',12),(619,'General Alvear',12),(620,'La Paz',12),(621,'Tunuyan',12),(622,'San Carlos',12),(623,'Las Heras',12),(624,'Lujan de Cuyo',12),(625,'San Martin',12),(626,'Santa Rosa',12),(627,'Capital',12),(628,'25 de Mayo',13),(629,'Iguazu',13),(630,'El Dorado',13),(631,'Leandro N. Alem',13),(632,'Apostoles',13),(633,'Cainguas',13),(634,'Leandro N.Alem',13),(635,'Concepcion',13),(636,'San Pedro',13),(637,'Candelaria',13),(638,'Grl.Manuel Belgrano',13),(639,'Obera',13),(640,'Libertador Grl.San Martin',13),(641,'San Ignacio',13),(642,'Montecarlo',13),(643,'General Manuel Belgrano',13),(644,'Lebertador Grl.San Martin',13),(645,'El Guarani',13),(646,'Eldorado',13),(647,'Capital',13),(648,'Guarani',13),(649,'San Javier',13),(650,'Libertador Grl. San Martin',13),(651,'Collon Cura',14),(652,'Alumine',14),(653,'Minas',14),(654,'Añelo',14),(655,'Confluencia',14),(656,'Chos Malal',14),(657,'Picunches',14),(658,'Norquin',14),(659,'Pehuenches',14),(660,'Loncopue',14),(661,'Huiliches',14),(662,'Zapala',14),(663,'Catan Lil',14),(664,'Ñorquin',14),(665,'Los Lagos',14),(666,'Picun Leufu',14),(667,'Lacar',14),(668,'Ñoequin',14),(669,'Valcheta',15),(670,'25 de Mayo',15),(671,'El Cuy',15),(672,'Ñorquinco',15),(673,'General Roca',15),(674,'Avellaneda',15),(675,'Conesa',15),(676,'Pichi Mahuida',15),(677,'San Antonio',15),(678,'Pilcaniyeu',15),(679,'9 de Julio',15),(680,'Adolfo Alsina',15),(681,'Bariloche',15),(682,'Pihi Mahuida',15),(683,'La Viña',16),(684,'San Martin',16),(685,'Oran',16),(686,'Anta',16),(687,'Gral.Jose de San Martin',16),(688,'Guachipas',16),(689,'Rosario de la Frontera',16),(690,'Rivadavia',16),(691,'Metan',16),(692,'San Carlos',16),(693,'Grl.Guemes',16),(694,'Cachi',16),(695,'Rosario de Lerma',16),(696,'Cafayate',16),(697,'Los Andes',16),(698,'Grl.Jose de San Martin',16),(699,'Capital',16),(700,'Grl.San Martin',16),(701,'General Guemes',16),(702,'Cerrillos',16),(703,'Chicoana',16),(704,'La\n Poma',16),(705,'La Capital',16),(706,'Candelaria',16),(707,'Rosario',16),(708,'Iruya',16),(709,'La Caldera',16),(710,'Los Andes',16),(711,'Molinos',16),(712,'Santa Victoria',16),(713,'9 de Julio',17),(714,'Jachal',17),(715,'Albardon',17),(716,'25 de Mayo',17),(717,'Santa Lucia',17),(718,'Angaco',17),(719,'Iglesia',17),(720,'Valle Fertil',17),(721,'Calingasta',17),(722,'Rivadavia',17),(723,'Caucete',17),(724,'Sarmiento',17),(725,'Pocito',17),(726,'San Martin',17),(727,'Chimbas',17),(728,'Iglesias',17),(729,'Ullun',17),(730,'Rawson',17),(731,'Capital',17),(732,'Zonda',17),(733,'Belgrano',18),(734,'Chacabuco',18),(735,'La Capital',18),(736,'Gobernador Dupuy',18),(737,'Grl.Pedernera',18),(738,'Ayacucho',18),(739,'Junin',18),(740,'Gonernador Dupuy',18),(741,'Coronel Pringles',18),(742,'General Pedernera',18),(743,'Gobernador Duval',18),(744,'Iglesia',18),(745,'Libertador Grl. San Martin',18),(746,'Libertador Grl.San Martin',18),(747,'Libertador Gr.San Martin',18),(748,'Caucete',18),(749,'Guer Aike',19),(750,'Deseado',19),(751,'Rio Chico',19),(752,'Magallanes',19),(753,'Lago Argentino',19),(754,'Corpen Aike',19),(755,'Lago Buenos Aires',19),(756,'General Lopez',20),(757,'Rosario',20),(758,'General Obligado',20),(759,'Constitucion',20),(760,'San Lorenzo',20),(761,'San Javier',20),(762,'La Capital',20),(763,'San Cristobal',20),(764,'Iriondo',20),(765,'Castellanos',20),(766,'Nueve de Julio',20),(767,'Caseros',20),(768,'San Jeronimo',20),(769,'Belgrano',20),(770,'Capital',20),(771,'San Justo',20),(772,'Grl.Obligado',20),(773,'La Caputal',20),(774,'Grl.Lopez',20),(775,'Vera',20),(776,'9 de Julio',20),(777,'San Martin',20),(778,'Las Colonias',20),(779,'Garay',20),(780,'Castelanos',20),(781,'Las Colinas',20),(782,'Banda',21),(783,'Moreno',21),(784,'Alberdi',21),(785,'Pellegrini',21),(786,'Ojo de Agua',21),(787,'Rio Hondo',21),(788,'General Taboada',21),(789,'Choya',21),(790,'Capital',21),(791,'Aguirre',21),(792,'Silipica',21),(793,'Belgrano',21),(794,'Figueroa',21),(795,'Salavina',21),(796,'Quebrachos',21),(797,'Robles',21),(798,'Avellaneda',21),(799,'Jimenez',21),(800,'Atamisqui',21),(801,'San Martin',21),(802,'Matara',21),(803,'Salayina',21),(804,'Guasayan',21),(805,'Copo',21),(806,'Brigadier Juan Felipe Ibarra',21),(807,'Dobles',21),(808,'Sarmiento',21),(809,'Loreto',21),(810,'Mitre',21),(811,'Rivadavia',21),(812,'Ushuaia',22),(813,'Islas del Atlantico Sur',22),(814,'Sector Antartico Argentino',22),(815,'Rio Grande',22),(816,'Is.del Atlantico Sur E Is.Malvinas',22),(817,'Antartida Argentina',22),(818,'Burruyacu',23),(819,'Trancas',23),(820,'Monteros',23),(821,'Leales',23),(822,'Cruz Alta',23),(823,'Rio Chico',23),(824,'Chicligasta',23),(825,'Tafi',23),(826,'Graneros',23),(827,'Famailla',23),(828,'Capital',23),(829,'Villa la Angostura',14);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) NOT NULL,
  PRIMARY KEY (`country_id`),
  UNIQUE KEY `country_name_UNIQUE` (`country_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Argentina'),(4,'Bolibia'),(2,'Chile'),(3,'Paraguay');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(600) NOT NULL,
  `item_description` varchar(4000) NOT NULL,
  `city_id` int(11) NOT NULL,
  `item_address` varchar(200) DEFAULT NULL,
  `item_geolocation` varchar(200) DEFAULT NULL,
  `item_conditions` varchar(2000) DEFAULT NULL,
  `item_active` int(10) NOT NULL,
  `item_creationDate` datetime NOT NULL,
  `item_price` int(11) NOT NULL,
  `item_deleted` tinyint(1) NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_dummy` tinyint(4) NOT NULL,
  `item_priceType` int(11) NOT NULL DEFAULT '0',
  `item_aprooved` int(11) DEFAULT '0',
  `rental_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_city_idx` (`city_id`),
  KEY `item_category_idx` (`category_id`),
  KEY `item_rental_idx` (`rental_id`),
  CONSTRAINT `item_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `item_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `item_rental` FOREIGN KEY (`rental_id`) REFERENCES `rental` (`rental_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=851 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (846,'Acanaladora de Muros','Acanaladora de Muros',681,'Av Bustillo 18,300','-41.1195793, -71.3871155','',1,'2013-02-14 01:28:35',0,0,112,0,0,1,4),(848,'Zapatos de seguridad','Zapatos de seguridad talle 40',681,'Otto Goedecke 300','-41.13577129999999, -71.29865860000001','Alquilo por semana o por día',1,'2013-02-14 11:29:07',40,0,122,0,1,1,5),(849,'Escalera plegable 4 metros','Escalera plegable 4 metros para trabajos de construccipon. Excelente calidad.',681,'Bustillo km 8000','-41.1217932, -71.39894279999999','Llamar depués de las 18hs.',1,'2013-02-14 11:44:28',50,0,93,0,1,1,6),(850,'Grampones para nieve','Grampones para caminta en glaciar o nieve',681,'Bustillo 8000','-41.1217932, -71.39894279999999','Llamar después de la 18hs',1,'2013-02-14 12:11:05',5,0,131,0,1,1,6);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemComment`
--

DROP TABLE IF EXISTS `itemComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemComment` (
  `itemComment_id` int(11) NOT NULL AUTO_INCREMENT,
  `itemComment_dateTime` datetime NOT NULL,
  `item_id` int(11) NOT NULL,
  `itemComment_text` varchar(4000) NOT NULL,
  `itemComment_reply` varchar(4000) DEFAULT NULL,
  `itemComment_type` varchar(45) DEFAULT NULL,
  `itemComment_reply_dateTime` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemComment_id`),
  KEY `itemComment_item_idx` (`item_id`),
  KEY `itemComment_user_idx` (`user_id`),
  CONSTRAINT `itemComment_item` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `itemComment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8465 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemComment`
--

LOCK TABLES `itemComment` WRITE;
/*!40000 ALTER TABLE `itemComment` DISABLE KEYS */;
INSERT INTO `itemComment` VALUES (8464,'2013-02-22 21:25:55',849,'La podes alcanzar hasta el centro?\r\nGracias',NULL,'1',NULL,2);
/*!40000 ALTER TABLE `itemComment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemImage`
--

DROP TABLE IF EXISTS `itemImage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemImage` (
  `itemImage_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `itemImage_name` varchar(100) NOT NULL,
  PRIMARY KEY (`itemImage_id`),
  KEY `itemImage_Item_idx` (`item_id`),
  CONSTRAINT `itemImage_Item` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemImage`
--

LOCK TABLES `itemImage` WRITE;
/*!40000 ALTER TABLE `itemImage` DISABLE KEYS */;
INSERT INTO `itemImage` VALUES (51,846,'SG1250-SG.png'),(52,848,'P1050482.jpg'),(53,848,'P1050483.jpg'),(54,849,'16012013378.jpg'),(55,849,'16012013379.jpg'),(56,849,'16012013380.jpg'),(57,850,'16012013385.jpg'),(58,850,'16012013386.jpg'),(59,850,'16012013387.jpg'),(60,850,'16012013388.jpg');
/*!40000 ALTER TABLE `itemImage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `province_name` varchar(100) NOT NULL,
  PRIMARY KEY (`province_id`),
  UNIQUE KEY `province_name_UNIQUE` (`province_name`),
  KEY `country_id_idx` (`country_id`),
  KEY `province_country_idx` (`country_id`),
  KEY `province_country_idx1` (`country_id`),
  CONSTRAINT `province_country` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `province`
--

LOCK TABLES `province` WRITE;
/*!40000 ALTER TABLE `province` DISABLE KEYS */;
INSERT INTO `province` VALUES (1,1,'Buenos Aires'),(2,1,'Catamarca'),(3,1,'Chaco'),(4,1,'Chubut'),(5,1,'Cordoba'),(6,1,'Corrientes'),(7,1,'Entre Rios'),(8,1,'Formosa'),(9,1,'Jujuy'),(10,1,'La Pampa'),(11,1,'La Rioja'),(12,1,'Mendoza'),(13,1,'Misiones'),(14,1,'Neuquen'),(15,1,'Rio Negro'),(16,1,'Salta'),(17,1,'San Juan'),(18,1,'San Luis'),(19,1,'Santa Cruz'),(20,1,'Santa Fe'),(21,1,'Santiago del Estero'),(22,1,'Tierra del Fuego'),(23,1,'Tucuman');
/*!40000 ALTER TABLE `province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rental`
--

DROP TABLE IF EXISTS `rental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rental` (
  `rental_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rental_name` varchar(500) NOT NULL,
  `rental_description` varchar(2000) DEFAULT NULL,
  `rental_hour` varchar(200) DEFAULT NULL,
  `rental_website` varchar(200) DEFAULT NULL,
  `rental_selfAdmin` int(11) DEFAULT '0',
  `rental_dni` varchar(45) DEFAULT NULL,
  `rental_cuit` varchar(45) DEFAULT NULL,
  `rental_type` int(11) NOT NULL DEFAULT '1',
  `rental_confirmed` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rental_id`),
  KEY `rental_user_idx` (`user_id`),
  CONSTRAINT `rental_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rental`
--

LOCK TABLES `rental` WRITE;
/*!40000 ALTER TABLE `rental` DISABLE KEYS */;
INSERT INTO `rental` VALUES (4,96,'Alquilo Todo','AlquiloTodo ® es una empresa dedicada al alquiler y venta de máquinas y andamios para empresas y usuario particular cuyo servicio especializado es único en la República Argentina. Es decir, brinda servicios de asesoramiento en alquiler y venta tanto a las industrias de la construcción, empresas de servicios, entes públicos o privados, el agro, como a los usuarios particulares. Y a todas aquellas personas que desarrollen tareas de pintura, albañilería, jardinería, limpieza, etc. A quienes le ofrece herramientas de trabajo adecuadas que les proporciona rapidez, eficacia y economía. El alquiler es sin operario. Es ágil porque sólo es necesario presentar el documento personal y dos facturas (original y fotocopia); es económico porque únicamente se paga el alquiler de la máquina que se lleva, no hay que preocuparse por el costo del mantenimiento y hay importantes descuentos por semana o por mes.','','www.alquilo-todo.com.ar',0,NULL,NULL,2,1),(5,97,'Annelise Wenk','','','',0,NULL,NULL,1,1),(6,98,'Julian','','','',0,NULL,NULL,1,1),(7,99,'Momentos Fiestas y Eventos','Organización de eventos.','','',0,NULL,NULL,2,1),(8,100,'Circuito Chico Mountain Bike','Mountain Bike Rental. 24 velocidades, frenos a disco, suspensión delantera regulable y con bloqueo, elementos de seguridad, asistencia en ruta y mucho más!','','www.circuitochicobikes.com',0,NULL,NULL,2,1),(9,101,'Cordillera Bikes','\r\nSi estás pensando en disfrutar Bariloche desde una bicicleta mientras observas paisajes increíbles, nosotros te podemos ayudar. Si sos un apasionado y tu intención es alquilar una bicicleta para sacarte las ganas, tenemos la  bici justa para que lo puedas hacer. Y si tu idea es pasar un día diferente y entender qué es eso de la bicicleta de montaña, te ofrecemos una buena oportunidad para vivirlo a través de un paseo guiado. Bariloche está rodeada por el Parque Nacional Nahuel Huapi,  el primero de Sudamérica,  uno de los más bellos y que contiene riquezas desde sus paisajes hasta su particular flora y fauna.\r\nSin embargo nuestro objetivo va más allá de un simple servicio de bicicletas. Nos sentiremos realmente satisfechos cuando al finalizar tu día, tu sensación sea la haber pasado un día en la naturaleza, disfrutando de paisajes y además haber recibido la atención que esperabas.\r\nAdelante... Te invitamos a empezar por acá.','9:30 am - 7:30 pm','www.cordillerabike.com/',0,NULL,NULL,2,1),(10,102,'RAID','Raid, tienda de montaña. Alquiler y venta de equipos de ski, snowboard, escalada, y todo lo que necesitas para salir a la montaña.','','',0,NULL,NULL,2,1),(11,103,'Arum-Co','Alquiler de kayacs y escuela de buceo.','','http://www.facebook.com/arumco.buceo',0,NULL,NULL,2,1),(12,104,'Ana Valderrama','Organización integral y alquiler de equipamiento para fiestas y eventos.','','www.anavalderrama.com.ar',0,NULL,NULL,2,1),(16,108,'Cabalgatas Tom Wesley','Somos una familia dedicada al turismo ecuestre y de aventura desde 1980.','','http://www.cabalgatastomwesley.com/',0,NULL,NULL,2,1),(17,109,'Kraal','Kraal es el lugar de encuentro para que todos nos pasemos informacion y consejos sobre actividades de montaña, combinado con un showroom de equipo de montaña y un rental. Para los que empiezan a experimentar este mundo de aventura.','','www.kraalbariloche.com',0,NULL,NULL,2,1),(18,110,'Bayo Abajo','Alquiles de bicicletas y roller.\r\nTaller de reparación.','','',0,NULL,NULL,2,1),(19,111,'Yeti','Alquiler de kayak.\r\nSalidas guiadas en Mountain Bikes\r\nIndumentaria','','www.yetipatagonia.com.ar',0,NULL,NULL,2,1),(20,112,'Taquari','Local Bici shop\r\nAlquiler / Venta / Accesorios / Bici partes / Paseos Guiados','','',0,NULL,NULL,2,1),(21,118,'Elixir Bariloche','Bienestar y renovación para el cuidado del cuerpo.','Abierto de lunes a lunes de 10 a 20 hs.','www.elixirbariloche.com.ar',0,NULL,NULL,2,1),(22,119,'Dyme','Centro de Dermatología y Medicina Estética.','','www.dymeargentina.com.ar',0,NULL,NULL,2,1),(23,120,'Dirty Bikes','Tours de Ciclismo y alquiler de bicicletas.','','www.dirtybikes.com.a',0,NULL,NULL,2,1),(27,124,'Tienda de eventos','Organizaci?n de eventos - Carpas - Catering - equipamiento para eventos.','','www.tiendadeeventos.com.ar',0,NULL,NULL,2,1),(28,126,'Bari Diversion','Alquiler de castillos inflables ( exterior e interior) y metegoles.  Organizaci?n de eventos.','','www.facebook.com/pages/Bari-diversion/184686608234371',0,NULL,NULL,2,1),(29,127,'Multifiestas (inflables)','Alquiler de castillos inflables, plaza banda y metegoles.','','http://multifiestasinflablesbariloche.webnode.com.ar/home/',0,NULL,NULL,2,1),(32,130,'Inflables Arco Iris','Alquiler de castillos inflables, varias medidas y metegol.','','https://www.facebook.com/pages/Inflables-ARCO-IRIS/111272979045414?ref=stream						',0,NULL,NULL,2,1),(33,131,'Bodoque entretenimiento','Alquiler de inflables tem?ticos con personajes favoritos. Metegoles.','','https://www.facebook.com/pages/Bodoque-entretenimientos/163743627096850?fref=ts						',0,NULL,NULL,2,1),(34,132,'BDQ Sonido e Iluminacion','Alquiler de sonido, iluminacion, video, maquina de humo y burbujas.Proyecciones.','-','www.facebook.com/BdqSonidoEIluminacion',0,NULL,NULL,2,1),(35,133,'Ayelen Disfraces ','Alquiler de Disfraces y Cotillon.','','www.facebook.com/ayelen.disfracesbariloche?fref=ts				',0,NULL,NULL,2,1),(36,134,'Gazebos Bariloche','Alquiler de Gazebos estructurales plegadizos.','','www.facebook.com/pages/Gazebos-Bariloche-GAB/296460410464139',0,NULL,NULL,2,1),(37,135,'Manteleria Ariadna','Alquiler de Manteleria para Eventos. Decoraci?n.','','www.facebook.com/pages/MANTELERIA-ARIADNA/396130877101358',0,NULL,NULL,2,1),(38,136,'Bajo Cero ','Escuela de ski y Rental','','www.bajocerobariloche.com.ar',0,NULL,NULL,2,1),(39,138,'Nestor Ski','Alquiler de ropa de Ski.Alquiler de Ski y Snowboard.','','',0,NULL,NULL,2,1),(40,139,'Alegre Ski','Alquiler y Clases de Ski.\r\nTaller de reparaciones y Traslado.','','www.alegreski.com.ar',0,NULL,NULL,2,1),(41,141,'La Colina Rental','Alquiler de equipos de Ski y Snowboard. Cascos, botas, fijaciones, trineos.','','www.lacolinarentalski.com.ar',0,NULL,NULL,2,1),(42,142,'Taos','Alquiler de ropa para nieve. ','','www.taosbariloche.com.ar',0,NULL,NULL,2,1),(43,143,'Lagos Rent a Car','Alquiler de veh?culos. Modelos standar, monovolumenes, vans, unidades 4x4 y autom?ticos.','','www.lagosrentacar.com.ar',0,NULL,NULL,2,1),(44,144,'Thrifty','Alquiler de autos.','','www.thriftybariloche.com.ar',0,NULL,NULL,2,1),(45,145,'Budget Bariloche','Alquiler de autos.','','www.budgetbariloche.com.ar',0,NULL,NULL,2,1),(46,146,'Bannex','Baños portátiles.','','',0,NULL,NULL,2,1),(47,147,'Ecosistemas cordilleranos','Alquiler de baños ecológicos.','','www.ecosistemascordilleranossrl.com',0,NULL,NULL,2,1),(48,148,'Cebra y cebron','Equipos para nieve - buzos y remeras.','','www.cebraycebron.com',0,NULL,NULL,2,1),(49,149,'Modena','Alquiler de autos.','','www.modenapatagonia.com',0,NULL,NULL,2,1),(50,150,'Febus','Ferretería. Alquiler de Herramientas.','','',0,NULL,NULL,2,1);
/*!40000 ALTER TABLE `rental` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rentalCategory`
--

DROP TABLE IF EXISTS `rentalCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rentalCategory` (
  `rental_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`rental_id`,`category_id`),
  KEY `rentalCategory_rental_idx` (`rental_id`),
  KEY `rentalCategory_category_idx` (`category_id`),
  CONSTRAINT `rentalCategory_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `rentalCategory_rental` FOREIGN KEY (`rental_id`) REFERENCES `rental` (`rental_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rentalCategory`
--

LOCK TABLES `rentalCategory` WRITE;
/*!40000 ALTER TABLE `rentalCategory` DISABLE KEYS */;
INSERT INTO `rentalCategory` VALUES (4,93),(4,113),(4,117),(5,94),(5,101),(5,148),(6,93),(6,100),(6,101),(7,97),(7,101),(7,140),(8,97),(8,130),(9,97),(9,130),(10,131),(10,132),(10,133),(11,96),(11,97),(11,166),(12,97),(12,101),(12,161),(16,96),(16,97),(16,131),(17,96),(17,131),(17,132),(18,97),(18,130),(18,131),(19,130),(19,133),(19,166),(20,96),(20,97),(20,130),(21,97),(21,121),(21,142),(22,97),(22,121),(22,142),(23,96),(23,130),(27,101),(27,140),(28,141),(28,158),(29,141),(29,158),(32,101),(32,158),(33,101),(33,158),(34,101),(34,160),(35,101),(35,159),(36,101),(36,164),(37,101),(37,164),(38,96),(38,133),(39,96),(39,133),(40,96),(40,133),(41,96),(41,133),(42,94),(42,133),(43,38),(43,143),(44,38),(44,143),(45,38),(45,143),(46,38),(46,118),(47,38),(47,118),(48,38),(48,94),(49,38),(49,143),(50,38),(50,113);
/*!40000 ALTER TABLE `rentalCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rentalImage`
--

DROP TABLE IF EXISTS `rentalImage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rentalImage` (
  `rentalImage_id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_id` int(11) NOT NULL,
  `rentalImage_name` varchar(200) NOT NULL,
  PRIMARY KEY (`rentalImage_id`),
  KEY `rentalImage_rental_idx` (`rental_id`),
  CONSTRAINT `rentalImage_rental` FOREIGN KEY (`rental_id`) REFERENCES `rental` (`rental_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rentalImage`
--

LOCK TABLES `rentalImage` WRITE;
/*!40000 ALTER TABLE `rentalImage` DISABLE KEYS */;
/*!40000 ALTER TABLE `rentalImage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rentalTag`
--

DROP TABLE IF EXISTS `rentalTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rentalTag` (
  `rentalTag_id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_id` int(11) NOT NULL,
  `rentalTag_name` varchar(100) NOT NULL,
  PRIMARY KEY (`rentalTag_id`),
  KEY `rental_id_idx` (`rental_id`),
  CONSTRAINT `rentalTag_rental` FOREIGN KEY (`rental_id`) REFERENCES `rental` (`rental_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rentalTag`
--

LOCK TABLES `rentalTag` WRITE;
/*!40000 ALTER TABLE `rentalTag` DISABLE KEYS */;
/*!40000 ALTER TABLE `rentalTag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_facebookId` int(30) NOT NULL,
  `user_registerDate` datetime NOT NULL,
  `user_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `user_lastname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `user_birthday` date DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `user_address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_geolocation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_deleted` tinyint(1) NOT NULL,
  `user_phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_isAdmin` int(11) DEFAULT '0',
  `user_token` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_city_idx` (`city_id`),
  CONSTRAINT `user_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,0,'2013-01-05 12:17:25','Ivan Fernando','Roth Rolnik','ivanfr26@gmail.com','12',NULL,681,'Otto Goedecke 300','-41.1334722, -71.3102778',0,'4356-4322',1,'done'),(95,0,'2013-02-13 20:31:34','Federico','Tula Rovaletti','fedetula@gmail.com','alquila',NULL,681,'Salta 128',NULL,0,'2944783687',1,'done'),(96,0,'0000-00-00 00:00:00','Alquilo Todo','','bariloche@alquilotodo.com.ar','libre-baal-08',NULL,681,'Castañares 140','-41.1334722, -71.3102778',0,'(0294) 442-6902',0,'done'),(97,0,'0000-00-00 00:00:00','Anne','Wenk','anne_gretel@yahoo.com.ar','Racha6',NULL,681,'Otto Goedecke 300','-41.13577129999999, -71.29865860000001',0,'',1,'done'),(98,0,'0000-00-00 00:00:00','Julian','Schtutman','julian@hotmail.com','cocococo',NULL,681,'Bustillo km 8000','-41.1217932, -71.39894279999999',0,'',1,'done'),(99,0,'0000-00-00 00:00:00','Majo','','majoyemi@gmail.com','libre-magm-08',NULL,681,'Mitre 100','-41.1336678, -71.30841709999999',0,'',0,'done'),(100,0,'0000-00-00 00:00:00','Circuito Chico','','ccmtbike@yahoo.com.ar','libre-ccya-08',NULL,681,'Bustillo 18300','-41.0926635, -71.44996689999999',0,'0294 15-436-1917',0,'done'),(101,0,'0000-00-00 00:00:00','',NULL,'info@cordillerabike.com','libre-inco-08',NULL,681,'Bustillo 18600','-41.0926635, -71.44996689999999',0,'0294 452-4828',0,'done'),(102,0,'0000-00-00 00:00:00','',NULL,'ramos1157@hotmail.com','libre_57ra',NULL,681,'Mitre 900','-41.1347572, -71.29716989999997',0,'02944 - 522178',0,'done'),(103,0,'0000-00-00 00:00:00','',NULL,'arumcobuceo@bariloche.com.ar','libre_eoar',NULL,681,'Hua Huan 7831','-41.1215814, -71.39502110000001',0,'4523122    15-604306',0,'done'),(104,0,'0000-00-00 00:00:00','Ana Valderrama',NULL,'contacto@anavalderrama.com.ar','libre-coan',NULL,681,'Mitre 100','-41.1336678, -71.30841709999999',0,' (0294) 15 441 4752',0,'done'),(108,0,'0000-00-00 00:00:00','Cabalgatas Tom Wesley',NULL,'info@cabalgatastomwesley.com','libre-inca',NULL,681,'Bustillo 15500','-41.0926635, -71.44996689999999',0,'2944 448193',0,'done'),(109,0,'0000-00-00 00:00:00','Kraal',NULL,'info@kraalbariloche.com','libre_krfo',NULL,681,'Av. de los pioneros 8500','-41.1221565, -71.40431289999998',0,'0294 4528858',0,'done'),(110,0,'0000-00-00 00:00:00','Bayo Abajo',NULL,'bayoabajo@argentina.com','libre_arjo',NULL,829,'','-40.7634201, -71.64614110000002',0,'4488383',0,'done'),(111,0,'0000-00-00 00:00:00','Yeti',NULL,'','libre_tiye',NULL,829,'','-40.7634201, -71.64614110000002',0,'0294-4495987',0,'done'),(112,0,'0000-00-00 00:00:00','Taquari',NULL,'fabioangostura@yahoo.com','libre_yara',NULL,829,'','-40.7634201, -71.64614110000002',0,'02944 154558385',0,'done'),(113,0,'2013-02-20 20:16:36','test','test','julianscht@hotmail.com','MIPERRACALA01',NULL,681,NULL,NULL,0,NULL,0,'done'),(114,0,'2013-02-21 22:06:01','Guille ','Tula','fafa@gmail.com','ttt',NULL,681,NULL,NULL,0,NULL,0,'woMTbPKwjHUBtFQdVnNAOcFy0ajEUCUgQwZSczel6ZNputtgH7Gl9bK0bUuVme13BQLDfQPcFss0ML6jICvIE6yFQSq2XiWpZxT5dy7JRpzu1wDzZZ8tVwYBffu2nghdE18Hp5h7lGrc2UCRKAbvWZX15iUjo2mTTkr8fy6r5ntX8VFIlH38'),(115,1455521249,'2013-02-21 22:13:41','Federico','Rovaletti','undefined','',NULL,681,NULL,NULL,0,NULL,0,'HqiVknQzkI8m7rH94WqMoAyPUlsqghMOyVAJ8g9jO8vMp2LjO2W3skIdw1uD97hySI7RP70u5m6leIvTAhMTslWOdhhcepBWYzEDwvXsHUDMtYvU58Dokq2nxaqBpRoegTIDevVLgpnAeJkaIOoS5g5sgmUw49BaS9EXvqyCFM2JmdJURXCM'),(116,0,'2013-02-22 15:32:10','Matias','Miguez','matiaspakua@hotmail.com','31552083Matias',NULL,681,'Pioneros 3435','-41.1335728, -71.35023560000002',0,'-',0,'done'),(117,1661967259,'2013-02-22 21:10:36','Ivan','Roth Rolnik','1661967259','',NULL,681,NULL,NULL,0,NULL,0,'yVoUrRPSVFIsI5HHmwvU93Ri7KDWVdjkYx5ffLX1gwjPsQmEcIocB5kzFOmrRwCFTxLZ8zQfWZVeF8IIGXL8SWxoAKGh68NQwpFvPmABbmFHkegR2SPKEdZ4NvcJuQpQ6VbL7Cc9OIHYNNFFvkg0n5U1rXALEQrACsbzUdzyM6npKSV531Wg'),(118,0,'0000-00-00 00:00:00','Elixir Bariloche',NULL,'info@elixirbariloche.com.ar','libre-inel',NULL,681,'Av. Bustillo 20,400','-41.1195793, -71.3871155',0,'(0294) 444 8119',0,'done'),(119,0,'0000-00-00 00:00:00','Dyme',NULL,'info@dymeargentina.com','libre-indy',NULL,681,'Rolando 430','-41.1363986, -71.3060916',0,'(0294) 443 3077',0,'done'),(120,0,'0000-00-00 00:00:00','Dirty Bikes',NULL,'info@dirtybikes.com.ar','libre-indi',NULL,681,'Lonquimay 3908','-41.1312979, -71.35573239999997',0,'(0294) 444 2775       ',0,'done'),(124,0,'0000-00-00 00:00:00','Tienda de eventos',NULL,'www.tiendadeeventos.com.ar','libre-tien',NULL,681,'Mitre 100','-41.1336678, -71.30841709999999',0,'54 (0294) 15 4318 221',0,'done'),(125,0,'2013-02-24 18:35:32','Rose Marie','Priotti','merypriotti@gmail.com','sagitario',NULL,681,NULL,NULL,0,NULL,1,'done'),(126,0,'0000-00-00 00:00:00','Bari Diversion',NULL,'baridiversion','libre_baridiversion',NULL,681,'','-41.1334722, -71.3102778',0,'443-6658 ',0,'done'),(127,0,'0000-00-00 00:00:00','Multifiestas (inflables)',NULL,'multifiestas_inflables@yahoo.com.ar','libre_multifiestasinflables',NULL,681,'','-41.1334722, -71.3102778',0,'0294 15-448-6555',0,'done'),(130,0,'0000-00-00 00:00:00','Inflables Arco Iris',NULL,'InflablesArcoIris','libre_inflablesarcoiris',NULL,681,'','-41.1334722, -71.3102778',0,'02944583223',0,'done'),(131,0,'0000-00-00 00:00:00','Bodoque entretenimiento',NULL,'bodoque_alquiler@hotmail.com ','libre_bodoqueentretenimiento',NULL,681,'','-41.1334722, -71.3102778',0,'15-434-9960    452-7295',0,'done'),(132,0,'0000-00-00 00:00:00','BDQ Sonido e Iluminacion',NULL,'BDQsonidoeiluminacion','libre_bdqsonidoeiluminacion',NULL,681,'','-41.1334722, -71.3102778',0,'294-15434-9960',0,'done'),(133,0,'0000-00-00 00:00:00','Ayelen Disfraces ',NULL,'ayelenxiomara@hotmail.com','libre_ayelendisfraces',NULL,681,'','-41.1334722, -71.3102778',0,' 294-154648609.',0,'done'),(134,0,'0000-00-00 00:00:00','Gazebos Bariloche',NULL,'GazebosBariloche','libre_gazebosbariloche',NULL,681,'','-41.1334722, -71.3102778',0,'15-466-2167',0,'done'),(135,0,'0000-00-00 00:00:00','Manteleria Ariadna',NULL,'ManteleriaAriadna','libre_manteleriaariadna',NULL,681,'','-41.1334722, -71.3102778',0,' 15-424-9282',0,'done'),(136,0,'0000-00-00 00:00:00','Bajo Cero ',NULL,'info@bajocerobariloche.com.ar','libre_bajoceroski',NULL,681,'Edificio Ski Club','-41.1656799, -71.44297599999999',0,'(0294) 15 4586522',0,'done'),(138,0,'0000-00-00 00:00:00','Nestor Ski',NULL,'NestorSki','libre_NestorSki',NULL,681,'Villa Catedral','-41.1688108, -71.43677650000001',0,'446-0114',0,'done'),(139,0,'0000-00-00 00:00:00','Alegre Ski',NULL,'info@alegreski.com.ar','libre_alegreski',NULL,681,'Moreno 237 local 3','-41.1334722, -71.3102778',0,'0294 445-6571',0,'done'),(141,0,'0000-00-00 00:00:00','La Colina Rental',NULL,'lacolinaski@smandes.com.ar','libre_lacolinarental',NULL,625,'Av. San Mart','-40.158232, -71.35447390000002',0,'(02972) 427-414',0,'done'),(142,0,'0000-00-00 00:00:00','Taos',NULL,'taosbariloche@hotmail.com','libre_taosbariloche',NULL,681,'San Martin 283','-41.1339834, -71.314502',0,'(0294)-4434587',0,'done'),(143,0,'0000-00-00 00:00:00','Lagos Rent a Car',NULL,'lagos@bariloche.com.ar ','libre-laba',NULL,681,'Mitre 83','-41.13367760000001, -71.30860259999997',0,'(0294) 442 8880',0,'done'),(144,0,'0000-00-00 00:00:00','Thrifty',NULL,'bariloche@thrifty.com.ar','libre-bath',NULL,681,'Quaglia 352','-41.1352507, -71.30871309999998',0,'(02944) 427904',0,'done'),(145,0,'0000-00-00 00:00:00','Budget Bariloche',NULL,'budget@bariloche.com.ar','libre-buba',NULL,681,'Mitre 717','-41.1345847, -71.29976779999998',0,'(294) 442 2482',0,'done'),(146,0,'0000-00-00 00:00:00','Bannex',NULL,'bannex.bariloche@gmail.com','libre-bagm',NULL,681,'Mitre 100','-41.1336678, -71.30841709999999',0,'(294) 15 4585449',0,'done'),(147,0,'0000-00-00 00:00:00','Ecosistemas cordilleranos',NULL,'ecosistemascordilleranos@speedy.com.ar','libre-ecsp',NULL,681,'12 de Octubre 1573','-41.133487, -71.28627499999999',0,'(294) 15 4326946',0,'done'),(148,0,'0000-00-00 00:00:00','Cebra y cebron',NULL,'cebraycebron@hotmail.com','libre-ceho',NULL,681,'Mitre 281','-41.133966, -71.3058954',0,'(294) 4432597',0,'done'),(149,0,'0000-00-00 00:00:00','Modena',NULL,'rentacar@modenapatagonia.com','libre-remo',NULL,681,'F. P . Moreno 69','-41.1346969, -71.3089986',0,'(+54)(294) 4434 192',0,'done'),(150,0,'0000-00-00 00:00:00','Febus',NULL,'ferreteriafebus@speedy.com.ar','libre-fesp',NULL,681,'Av. Bustillo km 10,500','-41.1326495, -71.31982540000001',0,'(0294) 446 1383',0,'done');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userComment`
--

DROP TABLE IF EXISTS `userComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userComment` (
  `userComment_id` int(11) NOT NULL AUTO_INCREMENT,
  `userComment_dateTime` datetime NOT NULL,
  `userCommented_id` int(11) NOT NULL,
  `userComment_text` varchar(4000) NOT NULL,
  `userComment_reply` varchar(4000) DEFAULT NULL,
  `userComment_type` varchar(45) DEFAULT NULL,
  `userComment_reply_dateTime` datetime DEFAULT NULL,
  `userCommenter_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`userComment_id`),
  KEY `itemComment_item_idx` (`userCommented_id`),
  KEY `itemComment_user_idx` (`userCommenter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userComment`
--

LOCK TABLES `userComment` WRITE;
/*!40000 ALTER TABLE `userComment` DISABLE KEYS */;
INSERT INTO `userComment` VALUES (4,'2013-02-15 22:21:21',96,'Buenos días, tienen moto guadañas para alquilar?',NULL,'1',NULL,2);
/*!40000 ALTER TABLE `userComment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userImage`
--

DROP TABLE IF EXISTS `userImage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userImage` (
  `userImage_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `userImage_name` varchar(100) NOT NULL,
  PRIMARY KEY (`userImage_id`),
  KEY `itemImage_Item_idx` (`user_id`),
  KEY `userImage_user_idx` (`user_id`),
  CONSTRAINT `userImage_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userImage`
--

LOCK TABLES `userImage` WRITE;
/*!40000 ALTER TABLE `userImage` DISABLE KEYS */;
INSERT INTO `userImage` VALUES (8,95,'kayac.jpg'),(9,96,'download (1).jpg'),(10,96,'alq1.jpg'),(11,96,'alq2.jpg'),(12,97,'29915_1421893997202_939118_n.jpg'),(13,99,'photo.jpg'),(14,100,'181661_194046717283559_2963853_n.jpg'),(15,100,'18254_497011536987074_1496579718_n.jpg'),(16,100,'262117_497012806986947_183869852_n.jpg'),(17,100,'522177_497011630320398_628691515_n.jpg'),(18,101,'216662_10150399978678018_7324248_a.jpg'),(19,101,'75077_10151678467973018_1719067909_n.jpg'),(20,101,'226609_10151678466773018_520423622_n.jpg'),(21,101,'253946_10151678466028018_814300684_n.jpg'),(22,102,'raid2.jpg'),(23,102,'raid.jpg'),(24,103,'134579_100618030015254_4374320_o.jpg'),(25,103,'250682_415500528527001_486405981_n.jpg'),(26,104,'plato.png'),(27,104,'sillas.jpg'),(28,104,'puff.jpg'),(29,104,'bco.jpg'),(30,104,'verde.jpg'),(31,108,'Tom Wesley.png'),(32,108,'cabalgatas.jpg'),(33,109,'kraal.png'),(34,109,'kraal2.png'),(35,110,'images.jpg'),(36,111,'yeti.png'),(37,111,'yeti2.png'),(38,111,'yeti3.png'),(39,111,'yeti4.png'),(40,112,'taquiari.jpg'),(41,118,'logo.jpg'),(42,118,'cara.jpg'),(43,118,'piernas.jpg'),(44,118,'local.jpg'),(45,119,'dyme1.jpg'),(46,119,'Dyme4.jpg'),(47,119,'Dyme3.jpg'),(48,119,'Dyme 2.jpg'),(49,120,'lo.jpg'),(50,120,'8a.jpg'),(51,120,'descarga.jpg'),(52,120,'images3.jpg'),(53,124,'tienda_eventos.jpg'),(54,124,'6.jpg'),(55,124,'7.jpg'),(56,124,'2.jpg'),(57,124,'4.jpg'),(58,124,'3.jpg'),(61,126,'Bari diversion 1.jpg'),(62,126,'Bari diversion 2.jpg'),(63,127,'Multifiestas inflables 1.jpg'),(64,127,'multifiestas inflables 2.jpg'),(65,130,'Inflables Arco Iris 1.jpg'),(66,131,'Bodoque entretenimientos portada.jpg'),(67,131,'Bodoque entretenimientos 1.jpg'),(68,131,'Bodoque entretenimientos 2.jpg'),(69,132,'BDQ sonido e iluminacion 1.jpg'),(70,133,'Alquiler de disfraces ayelen 1.jpg'),(71,133,'Ayelen disfraces 2.jpg'),(72,133,'Ayelen Disfraces 4.jpg'),(73,133,'Disfraces Ayelen 3.jpg'),(74,134,'Gazebos Bariloche 1.jpg'),(75,134,'Gazebos Bariloche 2.jpg'),(76,134,'Gazebos Bariloche 3.jpg'),(77,135,'Manteleria Ariadna 1.jpg'),(78,135,'Manteleria Ariadna 2.jpg'),(79,136,'bajo cero ski 1.jpg'),(80,136,'bajo cero ski 2.jpg'),(81,136,'bajo cero ski 3.jpg'),(82,138,'Nestor Ski 1.jpg'),(83,139,'Alegre Ski Portada.jpg'),(84,139,'alegre ski 1.jpg'),(85,139,'Alegre ski 2.jpg'),(88,141,'la colina ski 3.jpg'),(89,141,'la colina ski 4.jpg'),(90,141,'la colina ski 1.jpg'),(91,141,'la colina ski 2.jpg'),(93,142,'Taos 1.jpg'),(94,142,'taos 2.jpg'),(95,142,'Taos 3.jpg'),(96,142,'Taos 4.jpg'),(97,143,'aveo.jpg'),(98,143,'hilux.jpg'),(99,144,'autoimages.jpg'),(100,145,'logo.jpg'),(101,146,'baño.jpg'),(102,147,'Maxim2000connombre.gif'),(103,147,'especial2connombre.gif'),(104,147,'obraPJconnombre.gif'),(105,148,'quienes_somos_02.jpg'),(106,149,'descarga.jpg'),(107,150,'herramientas.jpg');
/*!40000 ALTER TABLE `userImage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userInfoRequest`
--

DROP TABLE IF EXISTS `userInfoRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userInfoRequest` (
  `user_requester` int(11) NOT NULL,
  `item_requested` int(11) NOT NULL,
  `userInfoRequest_dateTime` datetime DEFAULT NULL,
  `userInfoRequestcol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_requester`,`item_requested`),
  KEY `userInfoRequest_user_idx` (`user_requester`),
  KEY `userInfoRequest_item_idx` (`item_requested`),
  CONSTRAINT `userInfoRequest_item` FOREIGN KEY (`item_requested`) REFERENCES `item` (`item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userInfoRequest_user` FOREIGN KEY (`user_requester`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userInfoRequest`
--

LOCK TABLES `userInfoRequest` WRITE;
/*!40000 ALTER TABLE `userInfoRequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `userInfoRequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'rentimus_db_01'
--
/*!50003 DROP FUNCTION IF EXISTS `f_category_path` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 FUNCTION `f_category_path`(cat_id integer) RETURNS varchar(200) CHARSET latin1
BEGIN
DECLARE result varchar(400);

SELECT  group_concat(concat(parent.category_name, '.', parent.category_id)) into result
FROM category AS node,
        category AS parent
WHERE node.category_l BETWEEN parent.category_l AND parent.category_r
        AND node.category_id = cat_id
ORDER BY node.category_l;


RETURN result;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `f_item_comments_per_Type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 FUNCTION `f_item_comments_per_Type`(_type integer, _item_id integer) RETURNS int(11)
BEGIN
DECLARE result integer;

select count(*) into result
from item i inner join itemComment ic on i.item_id = _item_id and i.item_id = ic.item_id
where itemComment_type = _type or _type = 4;
RETURN result;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `f_rentalTag_string` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 FUNCTION `f_rentalTag_string`(_rental_id int) RETURNS varchar(500) CHARSET latin1
BEGIN
DECLARE result varchar(500);

select group_concat(rentalTag_name) into result
FROM rentalTag
WHERE rental_id = _rental_id;

RETURN result;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `f_user_is_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 FUNCTION `f_user_is_rental`(_user_id integer) RETURNS int(11)
BEGIN
DECLARE result integer;

SELECT rental_id into result from rental where user_id = _user_id;

set result = ifnull(result,0);

RETURN result;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_business_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_business_rental`(
IN _user_id int,
IN _rental_description varchar(2000),		
IN _rental_cuit varchar(45),	
IN _rental_website varchar(200),
IN _rental_hour varchar(200),
IN _rental_selfAdmin int
)
BEGIN
INSERT INTO rental(
		user_id,
		rental_description,
		rental_cuit,
		rental_website,
		rental_hour,
		rental_selfAdmin,
		rental_type
) 
VALUES(
		_user_id,
		_rental_description,
		_rental_cuit,
		_rental_website,
		_rental_hour,
		_rental_selfAdmin,
		1 
);


select LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_category` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_category`(IN _category_father_id integer, IN _category_name varchar(100))
BEGIN

  DECLARE r integer;


SELECT category_r into r FROM category WHERE category_id = _category_father_id;

UPDATE category SET category_l = category_l + '2' WHERE category_l >= r;
UPDATE category SET category_r = category_r + '2' WHERE category_r >= r;
 
INSERT INTO category (category_name, category_l, category_r) 
VALUES (_category_name, r, 1+r);

select LAST_INSERT_ID();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_item`(
		IN _item_name varchar(600),
		IN _item_description varchar(4000),
		IN _rental_id integer,
		IN _city_id integer,
 IN _item_address varchar(200),
 IN _item_geolocation varchar(200),
 IN _item_conditions varchar(2000),
		IN _item_price integer,
		IN _item_priceType integer,
		IN _category_id integer,
		IN _item_dummy integer
	   )
BEGIN
INSERT INTO item (
		item_name,
		item_description,
		rental_id,
		city_id,
		item_address,
		item_geolocation,
		item_conditions,
		item_active,
		item_creationDate,
		item_price,
		item_priceType,
		item_deleted,
		category_id,
		item_dummy,
		item_aprooved
		)
	VALUES
		(
		_item_name,
		_item_description,
		_rental_id,
		_city_id,
		_item_address,
		_item_geolocation,
		_item_conditions,
		1, 
		now(),
		_item_price,
		_item_priceType,
		0, 
		_category_id,
		_item_dummy,
		1 
	   );

select LAST_INSERT_ID();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_itemComment_reply` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_itemComment_reply`(IN _itemComment_id integer, IN _text varchar(4000))
BEGIN

UPDATE itemComment 
SET  itemComment_reply = _text,
	 itemComment_reply_dateTime = now()
WHERE itemComment_id = _itemComment_id;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_itemComment_text` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_itemComment_text`(IN _item_id integer, IN _text varchar(4000), IN _user_id int, IN _type integer)
BEGIN

INSERT INTO itemComment (item_id, itemComment_text, user_id, itemComment_type, itemComment_dateTime)
			values (_item_id, _text, _user_id, _type, now());
select LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_itemImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_itemImage`(IN `_item_id` INT, IN `_itemImage_name` VARCHAR(200))
BEGIN
insert into itemImage(item_id, itemImage_name) values(_item_id, _itemImage_name);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_personal_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_personal_rental`(
IN _user_id int,
IN _rental_description varchar(2000),		
IN _rental_dni varchar(45),	
IN _rental_website varchar(200),
IN _rental_hour varchar(200)
)
BEGIN
INSERT INTO rental(
		user_id,
		rental_description,
		rental_dni,
		rental_website,
		rental_type,
		rental_hour
) 
VALUES(
		_user_id,
		_rental_description,
		_rental_dni,
		_rental_website,
		2, 
		_rental_hour
);


select LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_rental`(
IN _user_id int,
IN _rental_description varchar(2000),		
IN _rental_bussinesHour varchar(200),		
IN _rental_website varchar(200),		
IN _rental_selfAdmin integer)
BEGIN
INSERT INTO rental(		user_id,		rental_description,		rental_bussinesHour,		rental_website,		rental_selfAdmin) 
VALUES(		_user_id,		_rental_description,		_rental_bussinesHour,		_rental_website,		_rental_selfAdmin);select LAST_INSERT_ID();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_rentalCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_rentalCategory`(IN _rental_id integer,IN _category_id integer)
BEGIN INSERT INTO rentalCategory(	rental_id,	category_id) VALUES(		_rental_id,		_category_id);END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_rentalImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_rentalImage`(in _rental_id integer, in _rentalImage_name varchar(200))
BEGIN

INSERT INTO rentalImage(

	rental_id,

	rentalImage_name

)

VALUES(

	_rental_id,

	_rentalImage_name

);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_rentalTag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_rentalTag`(IN _rental_id integer,IN _rentalTag_name varchar(100))
BEGIN INSERT INTO rentalTag(	rental_id,	rentalTag_name) VALUES(	_rental_id,	_rentalTag_name);select LAST_INSERT_ID();END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_user`(IN `_user_name` VARCHAR(200), IN `_user_lastname` VARCHAR(200), IN `_user_email` VARCHAR(100), IN `_user_password` VARCHAR(1000), IN `_city_id` INT, IN `_user_facebookId` INT(30), IN `_user_token` VARCHAR(200))
BEGIN

INSERT INTO user (
					user_registerDate,
                                        user_name,
					user_lastname,
					user_email,
					user_password,
					city_id,
                                        user_facebookId,
                                        user_token
				)
VALUES
				(
					now(),
					_user_name,
					_user_lastname,
					_user_email,
					_user_password,
					_city_id,
                                        _user_facebookId,
                                        _user_token
				);

select LAST_INSERT_ID();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_userComment_reply` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_userComment_reply`(
	IN _userComment_id integer, 
	IN _text varchar(2000))
BEGIN

UPDATE userComment 
SET  userComment_reply = _text,
	 userComment_reply_dateTime = now()
WHERE userComment_id = _userComment_id;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_userComment_text` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_userComment_text`(IN `_userCommented_id` INT, IN `_text` VARCHAR(2000), IN `_userCommenter_id` INT, IN `_type` INT)
BEGIN



INSERT INTO userComment (userCommented_id, userComment_text, userCommenter_id, userComment_type, userComment_dateTime)

			values (_userCommented_id, _text, _userCommenter_id, _type, now());

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_add_userImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_add_userImage`(in _user_id integer, in _userImage_name varchar(200))
BEGIN

INSERT INTO userImage(
	user_id,
	userImage_name
)

VALUES(
	_user_id,
	_userImage_name
);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_aproove_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_aproove_item`(in _item_id integer)
BEGIN
	update item set item_aprooved = 1 where item_id = _item_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_confirm_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_confirm_user`(IN _user_id int)
BEGIN
 update user set user_confirmed = 1 where user_id = _user_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_del_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_del_item`(IN _item_id integer)
BEGIN
UPDATE item
SET item_deleted = 1
WHERE item_id = _item_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_del_itemImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_del_itemImage`(IN `_itemImage_id` INT)
BEGIN

 delete from itemImage where itemImage_id = _itemImage_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_del_rentalCategory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_del_rentalCategory`(in _rental_id integer, in _category_id integer)
BEGIN

	DELETE FROM rentalCategory where category_id = _category_id and rental_id = _rental_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_del_rentalImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_del_rentalImage`(in _rentalImage_id integer)
BEGIN

	DELETE from rentalImage WHERE rentalImage_id = _rentalImage_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_del_rentalTag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_del_rentalTag`(IN _rentalTag_id integer)
BEGIN

	DELETE FROM rentalTag WHERE rentalTag_id = _rentalTag_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_del_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_del_user`(IN _user_id integer)
BEGIN
 DELETE FROM user WHERE user_id = _user_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_del_userImage` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_del_userImage`(in _userImage_id integer)
BEGIN

	DELETE from userImage WHERE userImage_id = _userImage_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_business_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_business_rental`(IN _rental_id integer)
BEGIN
SELECT 
	r.rental_id,
	r.rental_description,
	r.rental_website,
	r.rental_cuit,
	r.rental_selfAdmin,
	r.rental_hour,
	u.user_id,
	user_registerDate,
	user_name,
	user_lastname,
	user_email,
	user_password,
	user_birthday,
	c.city_id,
	c.city_name,
	p.province_id,
	p.province_name,
	co.country_id,
	co.country_name,
	user_address,
	user_geolocation,
	user_deleted,
	user_phone,
	user_confirmed,
	(select count(*) from item i inner join rental re on re.rental_id = _rental_id and i.rental_id = re.rental_id
					 inner join itemComment ic on i.item_id = ic.item_id
					 where ic.itemComment_type = 0 ) as comment_good,
	(select count(*) from item i inner join rental re on re.rental_id = _rental_id and i.rental_id = re.rental_id
					 inner join itemComment ic on i.item_id = ic.item_id
					 where ic.itemComment_type = 1 ) as comment_neutral,
	(select count(*) from item i inner join rental re on re.rental_id = _rental_id and i.rental_id = re.rental_id
					 inner join itemComment ic on i.item_id = ic.item_id
					 where ic.itemComment_type = 2 ) as comment_bad

FROM rental r inner join user u on r.user_id = u.user_id 
	inner join city c on u.city_id = c.city_id
	inner join province p on p.province_id = c.province_id
	inner join country co on co.country_id = p.country_id
WHERE r.rental_id = _rental_id;





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_categories_with_items` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_categories_with_items`()
BEGIN

select * from category where category_id  in (select category_id from item);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_child_categories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_child_categories`(in _category_id int)
BEGIN

DECLARE dParent integer;
DECLARE lParent integer;
DECLARE rParent integer;


select c.depth into dParent from (
							SELECT node.category_id, (COUNT(parent.category_id) - 1) AS depth
							FROM category AS node,
							category AS parent
							WHERE node.category_l BETWEEN parent.category_l AND parent.category_r
							GROUP BY node.category_name
							) as c
where category_id = _category_id;

select category_r into rParent from category where category_id = _category_id;
select category_l into lParent from category where category_id = _category_id;

SELECT c.category_name, c.category_id
FROM category c inner join (
							SELECT node.category_id, node.category_l, (COUNT(parent.category_id) - 1) AS depth
							FROM category AS node,
							category AS parent
							WHERE node.category_l BETWEEN parent.category_l AND parent.category_r
							GROUP BY node.category_name
							ORDER BY node.category_l
							) as c2
				on c.category_id = c2.category_id 
WHERE c2.category_l > lParent and c2.category_l < rParent and depth = (dParent + 1);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_father_categories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_father_categories`(IN catId INT)
BEGIN

SELECT  parent.category_name
FROM category AS node,
        category AS parent
WHERE node.category_l BETWEEN parent.category_l AND parent.category_r
        AND node.category_id = catId
ORDER BY node.category_l;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_item_comments` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_item_comments`(IN _item_id INT)
BEGIN

SELECT 
	itemComment_id,
	itemComment_dateTime,
	itemComment_text,
	itemComment_reply,
	itemComment_reply_dateTime,
	itemComment_type,
	u.user_id,
	u.user_name,
	u.user_lastname

FROM itemComment i inner join user u on i.user_id = u.user_id
WHERE item_id = _item_id
ORDER BY itemComment_dateTime;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_item_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_item_data`(IN `itemId` INT)
BEGIN
SELECT 
	i.item_id,
	i.item_name,
	i.item_description,
	i.item_address,
	i.item_geolocation,
	i.item_conditions,
	i.item_creationDate,
	i.item_price,
	i.item_priceType,
	i.rental_id,
	city_name,
	c.city_id,
	province_name,
	p.province_id,
	i.category_id,
	i.item_aprooved,
        r.user_id,
	f_category_path(i.category_id) as category_path,
	f_item_comments_per_type(0, i.item_id) as comments_good,
	f_item_comments_per_type(1, i.item_id) as comments_neutral, 
	f_item_comments_per_type(2, i.item_id) as comments_bad, 
	f_item_comments_per_type(4, i.item_id) as comments_all

FROM item i 
	inner join rental r on r.rental_id = i.rental_id 
	inner join city c on i.city_id = c.city_id 
	inner join province p on c.province_id = p.province_id
WHERE i.item_id = itemId;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_item_images` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_item_images`(IN _item_id int)
BEGIN

 select 
	itemImage_id as image_id,
	item_id,
	itemImage_name as image_name
 from itemImage where item_id = _item_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_personal_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_personal_rental`(IN _rental_id integer)
BEGIN
SELECT 
	r.rental_id,
	r.rental_description,
	r.rental_website,
	r.rental_dni,
	r.rental_hour,
	u.user_id,
	user_registerDate,
	user_name,
	user_lastname,
	user_email,
	user_password,
	user_birthday,
	c.city_id,
	c.city_name,
	p.province_id,
	p.province_name,
	co.country_id,
	co.country_name,
	user_address,
	user_geolocation,
	user_deleted,
	user_phone,
	user_confirmed,
	(select count(*) from item i inner join rental re on re.rental_id = _rental_id and i.rental_id = re.rental_id
					 inner join itemComment ic on i.item_id = ic.item_id
					 where ic.itemComment_type = 0 ) as comment_good,
	(select count(*) from item i inner join rental re on re.rental_id = _rental_id and i.rental_id = re.rental_id
					 inner join itemComment ic on i.item_id = ic.item_id
					 where ic.itemComment_type = 1 ) as comment_neutral,
	(select count(*) from item i inner join rental re on re.rental_id = _rental_id and i.rental_id = re.rental_id
					 inner join itemComment ic on i.item_id = ic.item_id
					 where ic.itemComment_type = 2 ) as comment_bad

FROM rental r inner join user u on r.user_id = u.user_id 
	inner join city c on u.city_id = c.city_id
	inner join province p on p.province_id = c.province_id
	inner join country co on co.country_id = p.country_id
WHERE r.rental_id = _rental_id;





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_rental_categories` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_rental_categories`(IN `_rental_id` INT)
BEGIN

select r.category_id, c.category_name, c.category_l, c.category_r
 from rentalCategory r inner join category c on r.category_id = c.category_id 
where rental_id = _rental_id;
 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_rental_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_rental_data`(IN `_rental_id` INT)
    NO SQL
SELECT 
		r.rental_id,
        r.rental_name,
		r.rental_website,
        r.rental_description,
        r.rental_confirmed,
        r.rental_hour,
	u.user_id,
	u.user_registerDate,
	u.user_name,
	u.user_lastname,
	u.user_email,
	u.user_password,
	u.user_birthday,
	c.city_id,
	c.city_name,
	p.province_id,
	p.province_name,
	u.user_address,
	u.user_geolocation,
	u.user_deleted,
	u.user_phone,
	u.user_isAdmin

FROM rental r 
	inner join user u on r.user_id = u.user_id
	inner join city c on u.city_id = c.city_id
	inner join province p on p.province_id = c.province_id
WHERE r.rental_id = _rental_id */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_rental_items_for_rent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_rental_items_for_rent`(IN `_rental_id` INT, IN `_active_items` INT)
BEGIN


SELECT 
	i.item_id,
        i.rental_id,
	i.item_name,
	i.item_description,
	i.rental_id,
        r.user_id,
	c.city_id,
	c.city_name,
	p.province_id,
	p.province_name,
	i.item_address,
	i.item_geolocation,
	i.item_conditions,
	i.item_active,
	i.item_creationDate,
	i.item_price,
	i.item_priceType,
	f_category_path(i.category_id) as category_path
        
FROM item i
	inner join rental r on r.rental_id = i.rental_id
	inner join city c on c.city_id = i.city_id
	inner join province p on c.province_id = p.province_id
	
WHERE i.rental_id = _rental_id AND i.item_deleted = FALSE
	 and (_active_items = 0 or item_active = 1);
	


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_rental_tags` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_rental_tags`(IN _rental_id integer)
BEGIN
	select rentalTag_name from rentalTag where rental_id = _rental_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_search_items` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_search_items`(IN `what` VARCHAR(100), IN `city_id` VARCHAR(100), IN `province_id` VARCHAR(100), IN `minPrice` VARCHAR(50), IN `maxPrice` VARCHAR(50), IN `category_id` VARCHAR(50), IN `orderBy` VARCHAR(10), IN `pageNum` VARCHAR(10))
BEGIN


DECLARE _allWhat 		VARCHAR(100);
DECLARE _city_id		INT;
DECLARE _province_id	INT;
DECLARE _minPrice		INT;
DECLARE _maxPrice		INT;
DECLARE _category_id	INT;
DECLARE _order			INT;
DECLARE _pageNum		INT;
DECLARE _pagin			INT;



SET _allWhat = CONCAT('%',LOWER(TRIM(what)),'%');
SET _city_id = CAST(city_id as DECIMAL);
SET _province_id = CAST(province_id as DECIMAL);
SET _minPrice = CAST(minPrice as DECIMAL);
SET _maxPrice = CAST(maxPrice as DECIMAL);
SET _category_id = CAST(category_id as DECIMAL);
SET _order = CAST(orderBy as DECIMAL);
SET _pagin = 30;
SET _pageNum = CAST(pageNum as DECIMAL) * _pagin;



select 		i.item_id, 
		item_name,
		item_description,
		item_creationDate,
		i.city_id,
		i.rental_id,
               	r.user_id,
		city_name, 
		p.province_id,
		province_name, 
		item_geolocation, 
		item_address, 
		item_price, 
		item_priceType,
		f_category_path(i.category_id) as category_path,
		f_category_path(i.category_id) as category_path,
		f_item_comments_per_type(0, i.item_id) as comments_good,
		f_item_comments_per_type(1, i.item_id) as comments_neutral, 
		f_item_comments_per_type(2, i.item_id) as comments_bad, 
		f_item_comments_per_type(4, i.item_id) as comments_all,
		i.item_dummy

from item i 
	inner join rental r on r.rental_id = i.rental_id 
	inner join city c on i.city_id = c.city_id 
	inner join province p on c.province_id = p.province_id
where
	(i.item_deleted = FALSE and i.item_active = TRUE and i.item_aprooved = TRUE) and
	((_province_id = 0) or (p.province_id = _province_id)) and
	((_city_id = 0) or (c.city_id = _city_id)) and
	(item_name like _allWhat or item_description like _allWhat) and
	(item_price >= _minPrice) and
	(item_price <= _maxPrice or _maxPrice = 0) and
	((_category_id = 0) or i.category_id IN (SELECT node.category_id
											 FROM category AS node, category AS parent
											 WHERE node.category_l BETWEEN parent.category_l AND parent.category_r
											 AND parent.category_id = _category_id
											 ))
ORDER BY
	CASE WHEN _order = 0 then i.item_id END DESC,
	CASE WHEN _order = 1 then i.item_creationDate END,
	CASE WHEN _order = 2 then i.item_price END ASC,
	CASE WHEN _order = 3 then i.item_price END DESC

LIMIT _pageNum,_pagin;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_search_items_count` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_search_items_count`(IN `what` VARCHAR(100), IN `city_id` VARCHAR(100), IN `province_id` VARCHAR(100), IN `minPrice` VARCHAR(50), IN `maxPrice` VARCHAR(50), IN `category_id` VARCHAR(50), IN `orderBy` VARCHAR(10))
BEGIN


DECLARE _allWhat 		VARCHAR(100);
DECLARE _city_id		INT;
DECLARE _province_id	INT;
DECLARE _minPrice		INT;
DECLARE _maxPrice		INT;
DECLARE _category_id	INT;
DECLARE _order			INT;


SET _allWhat = CONCAT('%',LOWER(TRIM(what)),'%');
SET _city_id = CAST(city_id as DECIMAL);
SET _province_id = CAST(province_id as DECIMAL);
SET _minPrice = CAST(minPrice as DECIMAL);
SET _maxPrice = CAST(maxPrice as DECIMAL);
SET _category_id = CAST(category_id as DECIMAL);
SET _order = CAST(orderBy as DECIMAL);


select 	i.item_id, 
		i.city_id,
		i.rental_id,
		city_name, 
		p.province_id,
		province_name, 
		f_category_path(i.category_id) as category_path,
		f_category_path(i.category_id) as category_path,
		i.item_dummy

from item i 
	inner join city c on i.city_id = c.city_id 
	inner join province p on c.province_id = p.province_id
where
	(i.item_deleted = FALSE and i.item_active = TRUE and i.item_aprooved = TRUE) and
	((_province_id = 0) or (p.province_id = _province_id)) and
	((_city_id = 0) or (c.city_id = _city_id)) and
	(item_name like _allWhat or item_description like _allWhat) and
    (item_price >= _minPrice) and
	(item_price <= _maxPrice or _maxPrice = 0) and
	((_category_id = 0) or i.category_id IN (SELECT node.category_id
											 FROM category AS node, category AS parent
											 WHERE node.category_l BETWEEN parent.category_l AND parent.category_r
											 AND parent.category_id = _category_id
											 ))
ORDER BY
	CASE WHEN _order = 0 then i.item_id END DESC,
	CASE WHEN _order = 1 then i.item_creationDate END,
	CASE WHEN _order = 2 then i.item_price END ASC,
	CASE WHEN _order = 3 then i.item_price END DESC;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_search_Rentals` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_search_Rentals`( 
IN `what` VARCHAR(100), 
IN `city_id` VARCHAR(100), 
IN `province_id` VARCHAR(100), 
IN `category_id` VARCHAR(50) )
BEGIN 
DECLARE _allWhat VARCHAR(100); 
DECLARE _city_id	 INT; 
DECLARE _province_id	INT; 
DECLARE _minPrice	 INT; 
DECLARE _maxPrice	 INT; 
DECLARE _category_id	INT; 
DECLARE _order	 INT; 
DECLARE _pageNum	 INT; 
DECLARE _pagin	 INT; 

SET _allWhat = CONCAT('%',LOWER(TRIM(what)),'%'); 
SET _city_id = CAST(city_id as DECIMAL); 
SET _province_id = CAST(province_id as DECIMAL); 
SET _category_id = CAST(category_id as DECIMAL); 
SELECT *,r.rental_id 
FROM rental r inner join user u on r.user_id = u.user_id 
inner join city c on u.city_id = c.city_id 
inner join province p on c.province_id = p.province_id 
WHERE (u.user_deleted = FALSE and r.rental_confirmed = TRUE and r.rental_type = 2) 
and ((_province_id = 0) or (p.province_id = _province_id)) and 
((_city_id = 0) or (c.city_id = _city_id)) and 
(u.user_name like _allWhat or u.user_lastname like _allWhat or r.rental_description like _allWhat or r.rental_website like _allWhat or f_rentalTag_string(r.rental_id) like _allWhat) and 
((_category_id = 0) or r.rental_id in ( 
	select rc.rental_id 
	FROM category san 
	inner join category par 
	inner join rentalCategory rc on san.category_id = rc.category_id 
	WHERE san.category_l between par.category_l and par.category_r and par.category_id = _category_id))

ORDER BY r.rental_id desc; 


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_user_comments` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_user_comments`(IN _userCommented_id INT)
BEGIN

SELECT 
	userComment_id,
	userComment_dateTime,
	userComment_text,
	userComment_reply,
	userComment_reply_dateTime,
	userComment_type,
	u.user_id,
	u.user_name,
	u.user_lastname

FROM userComment i inner join user u on i.userCommenter_id = u.user_id
WHERE userCommented_id = _userCommented_id
ORDER BY userComment_dateTime;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_user_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_user_data`(IN `_user_id` INT)
BEGIN

SELECT 
	u.user_id,
	user_registerDate,
	user_name,
	user_lastname,
	user_email,
        user_token,
	user_password,
	user_birthday,
	c.city_id,
	c.city_name,
	p.province_id,
	p.province_name,
	co.country_id,
	co.country_name,
	user_address,
	user_geolocation,
	user_deleted,
	user_phone,
	user_isAdmin

FROM user u inner join city c on u.city_id = c.city_id
	inner join province p on p.province_id = c.province_id
	inner join country co on co.country_id = p.country_id
WHERE u.user_id = _user_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_user_images` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_user_images`(IN _user_id INT)
BEGIN
 select 
	userImage_id as image_id,
	user_id,
	userImage_name as image_name
 from userImage where user_id = _user_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_user_items_for_rent` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_user_items_for_rent`(IN `userId` INT)
BEGIN

SELECT 
	i.item_id,
	i.item_name,
	i.item_description,
        i.item_imgs,
	i.user_id,
	c.city_id,
	c.city_name,
	p.province_id,
	p.province_name,
	i.item_address,
	i.item_geolocation,
	i.item_conditions,
	i.item_active,
	i.item_creationDate,
	i.item_price,
	i.item_priceType,
	f_category_path(i.category_id) as category_path
FROM item i
	inner join user u on u.user_id = i.user_id and u.user_id = userId
	inner join city c on c.city_id = i.city_id
	inner join province p on c.province_id = p.province_id
	
WHERE i.item_deleted = FALSE;
	


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_user_login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_get_user_login`(IN `_user_email` VARCHAR(100), IN `_user_password` VARCHAR(1000))
BEGIN

SELECT 
user_name,
user_id,
user_facebookId,
user_email,
user_token,
user_isAdmin,
f_user_is_rental(user_id) as rental_id,
(select rental_type from rental r where r.user_id = u.user_id) as rental_type,
(select rental_confirmed from rental r where r.user_id = u.user_id) as rental_confirmed
FROM user u 
WHERE user_email = _user_email and user_password = _user_password
			and user_deleted = 0;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rem_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_rem_item`(IN _item_id integer)
BEGIN
 DELETE FROM item 
 WHERE item_id = _item_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_rem_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_rem_user`(IN _user_id integer)
BEGIN
	DECLARE rental integer;

	UPDATE user set user_deleted = 1 WHERE user_id = _user_id;
	SELECT f_user_is_rental into rental;

	UPDATE item set item_deleted = 1 WHERE rental_id = rental;
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_set_item_active` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_set_item_active`(IN _item_id int, IN _item_active int)
BEGIN
 update item set item_active = _item_active where item_id = _item_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_upd_business_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_upd_business_rental`(
	IN _rental_id int,
	IN _rental_description varchar(2000),	
	IN _rental_cuit varchar(45),	
	IN _rental_website varchar(200),
	IN _rental_businessHour varchar(200),
	IN _rental_selfAdmin int

)
BEGIN

DECLARE strQ VARCHAR(1024);
DECLARE stmtp   VARCHAR(1024);
 
SET @strQ = CONCAT('UPDATE rental SET rental_id = ', _rental_id);

IF _rental_description IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_description = "',_rental_description,'"');
END IF;


IF _rental_bussinesHour IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_bussinesHour = "',_rental_bussinesHour,'"');
END IF;

IF _rental_cuit IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_cuit = "',_rental_cuit,'"');
END IF;

IF _rental_website IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_website = "',_rental_website,'"');
END IF;

IF _rental_selfAdmin IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_selfAdmin = ', _rental_selfAdmin);
END IF;


SET @strQ= CONCAT(@strQ, ' WHERE rental_id = ',_rental_id);


select @strQ;

PREPARE  stmtp FROM  @strQ;

EXECUTE  stmtp;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_upd_item` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_upd_item`(
		IN _item_id integer, 
 IN _item_name varchar(600),
 IN _item_description varchar(4000),
 IN _city_id integer,
 IN _item_address varchar(200),
 IN _item_geolocation varchar(200),
 IN _item_active integer,
 IN _item_conditions varchar(2000),
 IN _item_price integer,
 IN _item_priceType integer,
 IN _category_id integer,
 IN _item_dummy integer
)
BEGIN

DECLARE strQ VARCHAR(1024);
DECLARE stmtp   VARCHAR(1024);
 
SET @strQ = CONCAT('UPDATE item SET item_id = ', _item_id);

IF _item_name IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_name = "',_item_name,'"');
END IF;

IF _item_description IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_description = "',_item_description,'"');
END IF;

IF _city_id IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', city_id = ', _city_id);
END IF;

IF _item_address IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_address = "',_item_address,'"');
END IF;

IF _item_geolocation IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_geolocation = "',_item_geolocation,'"');
END IF;

IF _item_active IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_active = ', _item_active);
END IF;

IF _item_conditions IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_conditions = "',_item_conditions,'"');
END IF;

IF _item_price IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_price = ', _item_price);
END IF;

IF _item_priceType IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_priceType = ', _item_priceType);
END IF;

IF _category_id IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', category_id = ', _category_id);
END IF;

IF _item_dummy IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', item_dummy = ', _item_dummy);
END IF;

SET @strQ= CONCAT(@strQ, ' WHERE item_id = ',_item_id);



PREPARE  stmtp FROM  @strQ;
EXECUTE  stmtp;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_upd_personal_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_upd_personal_rental`(
		IN _rental_id int,
		IN _rental_description varchar(2000),
		IN _rental_bussinesHour varchar(200),
		IN _rental_website varchar(200)

)
BEGIN

DECLARE strQ VARCHAR(1024);
DECLARE stmtp   VARCHAR(1024);
 
SET @strQ = CONCAT('UPDATE rental SET rental_id = ', _rental_id);

IF _rental_description IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_description = "',_rental_description,'"');
END IF;

IF _rental_dni IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_dni = "',_rental_dni,'"');
END IF;

IF _rental_website IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', rental_website = "',_rental_website,'"');
END IF;


SET @strQ= CONCAT(@strQ, ' WHERE rental_id = ',_rental_id);


select @strQ;

PREPARE  stmtp FROM  @strQ;

EXECUTE  stmtp;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_upd_rental` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_upd_rental`(

		IN _rental_id int,

		IN _rental_description varchar(2000),

		IN _rental_bussinesHour varchar(200),

		IN _rental_website varchar(200),

		IN _rental_selfAdmin integer

)
BEGIN

DECLARE strQ VARCHAR(1024);

DECLARE stmtp   VARCHAR(1024);

 

SET @strQ = CONCAT('UPDATE rental SET rental_id = ', _rental_id);



IF _rental_description IS NOT NULL THEN

        SET @strQ= CONCAT(@strQ, ', rental_description = "',_rental_description,'"');

END IF;



IF _rental_bussinesHour IS NOT NULL THEN

        SET @strQ= CONCAT(@strQ, ', rental_bussinesHour = "',_rental_bussinesHour,'"');

END IF;



IF _rental_website IS NOT NULL THEN

        SET @strQ= CONCAT(@strQ, ', rental_website = "',_rental_website,'"');

END IF;



IF _rental_selfAdmin IS NOT NULL THEN

        SET @strQ= CONCAT(@strQ, ', rental_selfAdmin = ', _rental_selfAdmin);

END IF;





SET @strQ= CONCAT(@strQ, ' WHERE rental_id = ',_rental_id);





select @strQ;

PREPARE  stmtp FROM  @strQ;

EXECUTE  stmtp;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_upd_rental_type` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_upd_rental_type`(IN _rental_id integer, IN _rental_type integer)
BEGIN
	UPDATE rental SET rental_type = _rental_type WHERE rental_id = _rental_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_upd_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`visita`@`%`*/ /*!50003 PROCEDURE `sp_upd_user`(
					IN _user_id integer,
			IN _user_name varchar(200),
			IN _user_lastname varchar(200),
			IN _user_email varchar(100),
			IN _user_password varchar(1000),
			IN _user_birthday date,
			IN _city_id integer,
			IN _user_address varchar(200),
			IN _user_geolocation varchar(100),
			IN _user_phone varchar(100),
			IN _user_isAdmin integer
)
BEGIN

DECLARE strQ VARCHAR(1024);
DECLARE stmtp   VARCHAR(1024);
 
SET @strQ = CONCAT('UPDATE user SET user_id = ', _user_id);

IF _user_name IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_name = "',_user_name,'"');
END IF;

IF _user_lastname IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_lastname = "',_user_lastname,'"');
END IF;

IF _user_email IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_email = "',_user_email,'"');
END IF;

IF _user_password IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_password = "',_user_password,'"');
END IF;

IF _user_birthday IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_password = "',_user_birthday,'"');
END IF;

IF _city_id IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', city_id = ', _city_id);
END IF;

IF _user_address IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_address = "',_user_address,'"');
END IF;

IF _user_geolocation IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_geolocation = "',_user_geolocation,'"');
END IF;

IF _user_phone IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_phone = "',_user_phone,'"');
END IF;

IF _user_isAdmin IS NOT NULL THEN
        SET @strQ= CONCAT(@strQ, ', user_isAdmin = ', _user_isAdmin);
END IF;


SET @strQ= CONCAT(@strQ, ' WHERE user_id = ',_user_id);


select @strQ;
PREPARE  stmtp FROM  @strQ;
EXECUTE  stmtp;





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-27 18:48:13
