<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include $path .'controllers/database/connectionManager.php';
include $path .'dummies/BaseDummy.php';

$conn = new ConnectionManager();
$link = $conn->getConnection();

$items = new HistoryDummy();

for ($i = 0; $i < 40000; $i++) {
	$query = $items->getQuery('rentalhistory', $items->getDummy1());
	$result = mysqli_query($link, $query);
	
	if($result == false){
		echo $query . "<br>";
		echo mysqli_error($link);	
		die();
	}
}


class HistoryDummy{
	
	var $baseDummy;
	
	function __construct(){
		$this->baseDummy = new BaseDummy();
	}
	
	function getQuery($table, $dummy){
		return $this->baseDummy->getQuery($table, $dummy);
	}

	function getDummy1(){
		$dummy = [];

		$dummy['item_id'] =									rand(603, 802);
		$dummy['rentee_id'] =                               '2';
		$dummy['rentalHistory_date'] =                      '2012-10-03 00:00:00';
		$dummy['rentalHistory_rentee_confirmation'] =       '1';
		$dummy['rentalHistory_rental_confirmation'] =       '1';
		$dummy['rentalHistory_price'] =                     '334';
		
		$day = rand(1, 31);
		$dummy['rentalHistory_date_from'] =                 '2012-10-'.$day.' 00:00:00';
		$dummy['rentalHistory_date_to'] =                   '2012-10-'.($day + rand(1, 10)).' 00:00:00';

		return $dummy;
	}

}

?>