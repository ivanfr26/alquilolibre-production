<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path .'dummies/BaseDummy.php';

$conn = new ConnectionManager();
$link = $conn->getConnection();

$scores = new AddScores();

//generate 3 types of scores
mysqli_query($link, $scores->getQuery('scoreType', $scores->getDummyScores1()));
mysqli_query($link, $scores->getQuery('scoreType', $scores->getDummyScores2()));
mysqli_query($link, $scores->getQuery('scoreType', $scores->getDummyScores3()));

//fill all 40000 rental history with these types of scores
for ($i = 0; $i < 40000; $i++) {
	mysqli_query($link, $scores->getQuery('rentalHistoryScore', $scores->getDummyRentalHistoryScore(1,$i)));
	mysqli_query($link, $scores->getQuery('rentalHistoryScore', $scores->getDummyRentalHistoryScore(2,$i)));
	mysqli_query($link, $scores->getQuery('rentalHistoryScore', $scores->getDummyRentalHistoryScore(3,$i)));
}

echo mysqli_error($link);

class AddScores{
	var $baseDummy;
	
	function __construct(){
		$this->baseDummy = new BaseDummy();
	}
	
	function getQuery($table, $dummy){
		return $this->baseDummy->getQuery($table, $dummy);
	}
	
	function getDummyScores1(){
		$dummy = array();

		$dummy['scoreType_id'] = 1;
		$dummy['scoreType_name'] = "Calidad";

		return $dummy;
	}
	function getDummyScores2(){
		$dummy = array();
	
		$dummy['scoreType_id'] = 2;
		$dummy['scoreType_name'] = "Estado";
	
		return $dummy;
	}
	function getDummyScores3(){
		$dummy = array();
	
		$dummy['scoreType_id'] = 3;
		$dummy['scoreType_name'] = "Uso";
	
		return $dummy;
	}

	
	function getDummyRentalHistoryScore($id_scoreType,$rentalHistory_id){
		$dummy = array();
	
		$dummy['scoreType_id'] = $id_scoreType;
		$dummy['rentalHistory_id'] = $rentalHistory_id;
		$dummy['rentalHistoryScore_score'] = rand(1,10);
	
		return $dummy;
	}
	
	

}

?>