<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path .'controllers/database/connectionManager.php';
include_once $path .'dummies/BaseDummy.php';

$conn = new ConnectionManager();
$link = $conn->getConnection();

$tags = new AddItemTags();

for ($i = 0; $i < 4000; $i++) {
	mysqli_query($link, $tags->getQuery('itemTag', $tags->getDummy1()));
	mysqli_query($link, $tags->getQuery('itemTag', $tags->getDummy2()));
	mysqli_query($link, $tags->getQuery('itemTag', $tags->getDummy3()));
}

echo mysqli_error($link);

class AddItemTags{
	var $baseDummy;
	
	function __construct(){
		$this->baseDummy = new BaseDummy();
	}
	
	function getQuery($table, $dummy){
		return $this->baseDummy->getQuery($table, $dummy);
	}
	
	function getDummy1(){
		$dummy = array();

		$dummy['item_id'] = rand(603, 803);
		$dummy['itemTag_name'] = "Azul";

		return $dummy;
	}
	
	function getDummy2(){
		$dummy = array();
	
		$dummy['item_id'] = rand(603, 803);
		$dummy['itemTag_name'] = "Sin agua";
	
		return $dummy;
	}
	
	function getDummy3(){
		$dummy = array();
	
		$dummy['item_id'] = rand(603, 803);
		$dummy['itemTag_name'] = "Madera";
	
		return $dummy;
	}
	

}

?>