<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include $path .'controllers/database/connectionManager.php';
include $path .'dummies/BaseDummy.php';

$conn = new ConnectionManager();
$link = $conn->getConnection();

$items = new FeedbackDummy();

for ($i = 0; $i < 4000; $i++) {
	$result1 = mysqli_query($link, $items->getQuery('question', $items->getDummy1()));
	$result2 = mysqli_query($link, $items->getQuery('question', $items->getDummy2()));
	
	if(!$result1 || !$result2){
		echo mysqli_error($link) . "<br>";
	}
}

/**
 * 
 * @author ivan
 *
 */
class FeedbackDummy{

	var $baseDummy;
	
	function __construct(){
		$this->baseDummy = new BaseDummy();
	}
	
	function getQuery($table, $dummy){
		return $this->baseDummy->getQuery($table, $dummy);
	}
	
	function getDummy1(){
		$dummy = Array();

		$dummy['user_id'] = "2";
		$dummy['item_id'] = rand(603, 803);
		$dummy['question_comment'] = "Como hago para alquilar?";
		$dummy['question_reply'] = "Tirando monedas al aire";
				
		return $dummy;
	}

	function getDummy2(){
		$dummy = Array();

		$dummy['user_id'] = "2";
		$dummy['item_id'] = rand(603, 803);
		$dummy['question_comment'] = "Cuanto sale el tema?";
		$dummy['question_reply'] = "Un monton de nubes.";
		
		return $dummy;
	}


}

?>