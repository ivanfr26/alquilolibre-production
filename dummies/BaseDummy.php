<?php


class BaseDummy{
	
	function getQuery($table, $dummy){
		//	INSERT INTO `table`(`field`, `field`, ... ) VALUES ('value', 'value', ....)
	
		$fields = "";
		$values = "";
	
		foreach ($dummy as $field => $value) {
			$fields .= 	"`$field`,";
			$values .= 	"'$value',";
		}
	
		$fields .= 	"END";
		$values .= 	"END";
		$fields = str_replace(',END', '', $fields);
		$values = str_replace(',END', '', $values);
	
		$query = "INSERT INTO `$table`($fields) VALUES ($values)";
	
		return $query;
	}
}

?>