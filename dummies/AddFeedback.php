<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include $path .'controllers/database/connectionManager.php';
include $path .'dummies/BaseDummy.php';

$conn = new ConnectionManager();
$link = $conn->getConnection();

$items = new FeedbackDummy();

for ($i = 0; $i < 40000; $i++) {
	$result1 = mysqli_query($link, $items->getQuery('rentalhistoryfeedback', $items->getDummy1()));
	$result2 = mysqli_query($link, $items->getQuery('rentalhistoryfeedback', $items->getDummy2()));
	
	if(!$result1 || !$result2){
		echo mysqli_error($link) . "<br>";
	}
}

/**
 * 
 * @author ivan
 *
 */
class FeedbackDummy{

	var $baseDummy;
	
	function __construct(){
		$this->baseDummy = new BaseDummy();
	}
	
	function getQuery($table, $dummy){
		return $this->baseDummy->getQuery($table, $dummy);
	}
	
	function getDummy1(){
		$dummy = [];

		$dummy['rentalHistory_id'] = rand(1, 40260);
		$dummy['rentalHistoryFeedback_renteeFeedback'] = "Todo muy bueno, estaba genial.";
		$dummy['rentalHistoryFeedback_rentalFeedback'] = "Pago en efectivo. Todo copado.";
		$dummy['rentalHistoryFeedback_renteeScore'] = rand(1, 10);
		$dummy['rentalHistoryFeedback_rentalScore'] = rand(1, 10);
				
		return $dummy;
	}

	function getDummy2(){
		$dummy = [];

		$dummy['rentalHistory_id'] = rand(1, 40260);
		$dummy['rentalHistoryFeedback_renteeFeedback'] = "La cosa anda de maravilla.";
		$dummy['rentalHistoryFeedback_rentalFeedback'] = "Buena onda, me pago en yenes. ";
		$dummy['rentalHistoryFeedback_renteeScore'] = rand(1, 10);
		$dummy['rentalHistoryFeedback_rentalScore'] = rand(1, 10);
		
		return $dummy;
	}


}

?>