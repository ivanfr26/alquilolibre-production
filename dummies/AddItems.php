<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include $path .'controllers/database/connectionManager.php';
include $path .'dummies/BaseDummy.php';

$conn = new ConnectionManager();
$link = $conn->getConnection();

$items = new AddItems();

for ($i = 0; $i < 100; $i++) {
	mysqli_query($link, $items->getQuery('item', $items->getDummy1()));
	mysqli_query($link, $items->getQuery('item', $items->getDummy2()));
}

echo mysqli_error($link);

class AddItems{
	
	var $baseDummy;
	
	function __construct(){
		$this->baseDummy = new BaseDummy();
	}
	
	function getQuery($table, $dummy){
		return $this->baseDummy->getQuery($table, $dummy);
	}

	function getDummy1(){
		$dummy = [];
		$dummy['user_id'] = "2";

		$dummy['category_id'] = rand(2, 31);
		$dummy['item_name'] = "Bicicleta Cleta";
		$dummy['item_description'] = "La bicicleta es un vehículo de transporte personal de propulsión humana, es decir por el propio viajero. Sus componentes básicos son dos ruedas, generalmente de igual diámetro y dispuestas en línea, un sistema de transmisión a pedales, un cuadro metálico que le da la estructura e integra los componentes, un manillar para controlar la dirección y un sillín para sentarse. El desplazamiento se obtiene al girar con las piernas la caja de los pedales que a través de una cadena hace girar un piñón que a su vez hace girar la rueda trasera sobre el pavimento. El diseño y configuración básico de la bicicleta ha cambiado poco desde el primer modelo de transmisión de cadena desarrollado alrededor de 1885.";
		$dummy['city_id'] = rand(256, 828);
		$dummy['item_address'] = "";
		$dummy['item_geolocation'] = "0";
		$dummy['item_conditions'] = "No romper. No morder. Correr si.";
		$dummy['item_active'] = "1";
		$dummy['item_expirationDate'] = "0";
		$dummy['item_dailyPrice'] = "30";
		$dummy['item_weeklyPrice'] = "150";
		$dummy['item_monthlyPrice'] = "600";
		$dummy['item_deleted'] = "0";

		return $dummy;
	}

	function getDummy2(){
		$dummy = [];

		$dummy['user_id'] = "2";
		
		$dummy['category_id'] = rand(2, 31);
		$dummy['item_name'] = "Martillo Neumatico";
		$dummy['item_description'] = "Un martillo mecánico, también denominado martillo rompepavimentos, es una máquina, generalmente de uso profesional, que es utilizada con objeto de demoler pavimentos, realizar agujeros de grandes dimensiones o demoler construcciones de diversa índole. Existen tres tipos, neumáticos, hidráulicos y eléctricos.";
		$dummy['city_id'] = rand(256, 828);
		$dummy['item_address'] = "";
		$dummy['item_geolocation'] = "0";
		$dummy['item_conditions'] = "No romper. No morder. Correr si.";
		$dummy['item_active'] = "1";
		$dummy['item_expirationDate'] = "0";
		$dummy['item_dailyPrice'] = "300";
		$dummy['item_weeklyPrice'] = "1500";
		$dummy['item_monthlyPrice'] = "6020";
		$dummy['item_deleted'] = "0";

		return $dummy;
	}


}

?>