<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . "/";
	include_once $path . 'controllers/server/tools.php'; 
	controlUserLogguedIn();
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		
		<title>Mis publicaciones</title>
		<link rel="icon" type="image/png" href="/lib/images/favicon.png"/>
		
		<link rel="stylesheet" type="text/css" href="/css/header.css" />
		<link rel="stylesheet" type="text/css" href="/css/profile.css" />
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		
		<script src="/controllers/user/profile/profileViewController.js"></script>
	</head>
	<body>
		<div id="main_body">
			<?php
			//Base include
			include_once $path . 'global.php';
			include_once $path . 'languages/es.php';
			include_once $path . 'controllers/server/tools.php';
			include_once $path . 'templates/blocks/header.tpl.php';
			
			//Include first the controller, the templates used them, and no viceversa.
			include_once $path . 'controllers/server/profile/profileController.php';
			include_once $path . 'templates/pages/myitems.tpl.php';
			
			include_once $path.'templates/blocks/foot.tpl.php';
			
			
			?>
		</div>
	</body>
</html>
