<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		
		<title>Equipo Fundador</title>
		<link rel="icon" type="image/png" href="/lib/images/favicon.png"/>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		
		<link rel="stylesheet" type="text/css" href="/css/header.css" />
	</head>
	<body>
		<div id="main_body">
			<?php
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			include $path.'languages/es.php';
			
			include_once $path . 'controllers/server/tools.php';
			include $path . 'templates/blocks/header.tpl.php';	
			
			include $path . 'templates/pages/team.tpl.php';
			
			include_once $path.'templates/blocks/foot.tpl.php';
			
			
			?>
		</div>
	</body>
</html>