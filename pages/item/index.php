<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'global.php';

include_once $path . 'languages/es.php';
include_once $path . 'controllers/server/tools.php';

include_once $path . 'controllers/database/itemManager.php';
include_once $path . 'controllers/database/userManager.php';
include_once $path . 'controllers/server/item/itemController.php';
include_once $path . 'controllers/server/category/categoryController.php';
include_once $path . 'controllers/server/question/questionController.php';

$raw = key($_REQUEST);

$item = getItem(strstr($raw, "/", true));
$imagesArray = getItemImages($item->item_id, $item->user_id);

?>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		
		<link rel="icon" type="image/png" href="/lib/images/favicon.png" />
		
		<title><?php echo getItemName($raw)?></title>
		
		<meta property="og:title" content="<?php echo $item->item_name?>"/>
	    <meta property="og:site_name" content="AlquiloLibre"/>
	    <meta property="og:description" content="<?php echo $item->item_description ?>"/>
	    <meta property="og:image" content="<?php echo @$imagesArray[0]->image_fullpath?>"/>
	    <meta property="og:url" content="http://alquilolibre.com/pages/item/?item_id=<?php echo $item->item_id?>&item_name=<?php echo $item->item_name?>"/>
	    <meta property="fb:admins" content="1661967259"/>
		
		<link rel="stylesheet" type="text/css" href="/css/header.css" />
		<link rel="stylesheet" type="text/css" href="/css/item.css?version=9.2" />
		<link rel="stylesheet" type="text/css" href="/css/question.css?version=9.2" />
		<link rel="stylesheet" type="text/css" href="/css/search.css?version=9.2" />
		<link rel="stylesheet" type="text/css" href="/css/images.css?version=9.2" />
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		
		
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnsb9yCAMRHQt6gjWbbUwiAQO9d1BSWVY&sensor=false"></script>
		<script	src="/controllers/user/MapController.js"></script>
	</head>
	<body>
		<div id="main_body">
			<?php
			include_once $path . 'templates/blocks/header.tpl.php';
			include_once $path . 'templates/pages/item.tpl.php';
			include_once $path.'templates/blocks/foot.tpl.php';
			
			?>
		</div>
	</body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38798421-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</html>