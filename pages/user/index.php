<?php

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'global.php';

include_once $path . 'languages/es.php';
include_once $path . 'controllers/server/tools.php';

include_once $path . 'controllers/database/itemManager.php';
include_once $path . 'controllers/database/userManager.php';
include_once $path . 'controllers/server/item/itemController.php';
include_once $path . 'controllers/server/category/categoryController.php';
include_once $path . 'controllers/server/question/questionController.php';

$raw = key($_REQUEST);

$userManager = new UserManager();
$user = $userManager->getUser(strstr($raw, "/", true));

$userImagesArray = getUserImages($user->user_id);
?>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		
		<link rel="icon" type="image/png" href="/lib/images/favicon.png" />
		
		<title><?php echo getItemName($raw);?></title>
		
		<meta property="og:title" content="<?php echo $user->rental_name?>"/>
	    <meta property="og:site_name" content="AlquiloLibre"/>
	    <meta property="og:description" content="<?php echo $user->rental_description ?>"/>
	    <meta property="og:image" content="<?php echo @$userImagesArray[0]->image_fullpath?>"/>
	    <meta property="og:url" content="http://alquilolibre.com/pages/rental/?rental_id=<?php echo $user->rental_id?>&rental_name=<?php echo $user->rental_name?>"/>
	    <meta property="fb:admins" content="1661967259"/>
		
		<link rel="stylesheet" type="text/css" href="/css/header.css" />
		<link rel="stylesheet" type="text/css" href="/css/item.css" />
		<link rel="stylesheet" type="text/css" href="/css/question.css" />
		<link rel="stylesheet" type="text/css" href="/css/search.css" />
		<link rel="stylesheet" type="text/css" href="/css/images.css" />
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		
		
		
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnsb9yCAMRHQt6gjWbbUwiAQO9d1BSWVY&sensor=false"></script>
		<script	src="/controllers/user/MapController.js"></script>
	</head>
	<body>
		<div id="main_body">
			<?php
			include_once $path . 'templates/blocks/header.tpl.php';
			include_once $path . 'templates/pages/userView.tpl.php';
			include_once $path . 'templates/blocks/foot.tpl.php';
			include_once $path . 'templates/blocks/feedback.tpl.php';
			?>
		</div>
	</body>
</html>