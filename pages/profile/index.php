<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . "/";
	include_once $path . 'controllers/server/tools.php'; 
	controlUserLogguedIn();
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		
		<title>Perfil</title>
		<link rel="icon" type="image/png" href="/lib/images/favicon.png"/>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		<script src="/controllers/user/location/LocationController.js?version=9.2"></script>
		
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnsb9yCAMRHQt6gjWbbUwiAQO9d1BSWVY&sensor=false"></script>
		<script src="/controllers/user/MapController.js"></script>
		
		<link rel="stylesheet" type="text/css" href="/css/header.css" />
		<link rel="stylesheet" type="text/css" href="/css/profile.css" />
		<link rel="stylesheet" type="text/css" href="/css/editItem.css" />
		
		<script src="/controllers/user/profile/profileViewController.js"></script>
	</head>
	<body>
		<div id="main_body">
			<?php
			//Base include
			include_once $path . 'global.php';
			include_once $path . 'languages/es.php';
			include_once $path . 'controllers/server/tools.php';
			include_once $path . 'templates/blocks/header.tpl.php';
			
			//Include first the controller, the templates used them, and no viceversa.
			include_once $path . 'controllers/server/item/updateController.php';
			include_once $path . 'controllers/server/profile/profileController.php';
			include_once $path.'controllers/server/location/locationController.php';
			
			if($_SESSION['rental_type'] != BASIC_USER){
				include_once $path . 'templates/pages/rentalProfile.tpl.php';
			}
			
			include_once $path . 'templates/pages/profile.tpl.php';
			include_once $path.'templates/blocks/foot.tpl.php';
			?>
		</div>
	</body>
</html>