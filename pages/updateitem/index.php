<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
	
		<title>Editar publicación</title>
		<link rel="icon" type="image/png" href="/lib/images/favicon.png"/>
	
		<link rel="stylesheet" type="text/css" href="/css/header.css" />
		<link rel="stylesheet" type="text/css" href="/css/item.css" />
		<link rel="stylesheet" type="text/css" href="/css/editItem.css" />
		<link rel="stylesheet" type="text/css" href="/css/question.css" />
		<link rel="stylesheet" type="text/css" href="/css/search.css" />
		<link rel="stylesheet" type="text/css" href="/css/images.css" />
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		
		
		
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnsb9yCAMRHQt6gjWbbUwiAQO9d1BSWVY&sensor=false"></script>
		<script	src="/controllers/user/MapController.js"></script>
		
		<script src="/controllers/user/location/LocationController.js"></script>
	</head>
	<body>
		<div id="main_body">
			<?php
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			include_once $path . 'global.php';
			
			include_once $path . 'languages/es.php';
			include_once $path . 'controllers/server/tools.php';
			include_once $path . 'templates/blocks/header.tpl.php';
			
			include_once $path . 'controllers/server/item/itemController.php';
			include_once $path . 'controllers/database/itemManager.php';
			
			include_once $path . 'controllers/server/item/updateController.php';
			
			include_once $path . 'controllers/server/location/locationController.php';
			
			include_once $path . 'controllers/server/category/categoryController.php';
			include_once $path . 'controllers/server/question/questionController.php';
			include_once $path . 'controllers/server/profile/profileController.php';
			
			include_once $path . 'templates/pages/updateitem.tpl.php';
			
			include_once $path.'templates/blocks/foot.tpl.php';
			?>
		</div>
	</body>
</html>