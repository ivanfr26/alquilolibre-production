<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		
		<title>Buscar</title>
		<link rel="icon" type="image/png" href="/lib/images/favicon.png" />
		
		<link rel="stylesheet" type="text/css" href="/css/header.css?version=9.2" />
		<link rel="stylesheet" type="text/css" href="/css/results.css?version=9.2" />
		<link rel="stylesheet" type="text/css" href="/css/filters.css?version=9.2" />
		<link rel="stylesheet" type="text/css" href="/css/search.css?version=9.2" />
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		
		
		
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnsb9yCAMRHQt6gjWbbUwiAQO9d1BSWVY&sensor=false"></script>
		<script	src="/controllers/user/MapController.js?version=9.2"></script>
	</head>
	<body>
		<div id="main_body">
			<?php
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			include_once $path . 'languages/es.php';
			
			include_once $path . 'controllers/server/tools.php';
			include_once $path . 'controllers/server/resultsController.php';
			
			include_once $path . 'templates/blocks/header.tpl.php';

			include_once $path . 'templates/pages/resultsRentals.tpl.php';
			
			include_once $path.'templates/blocks/foot.tpl.php';
			?>
		</div>
	</body>
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-38798421-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>
</html>