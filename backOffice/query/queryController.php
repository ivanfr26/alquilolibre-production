<?php 

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'controllers/database/connectionManager.php';

$controller = new QueryController();
$controller->event(@$_REQUEST['event']);


class QueryController{

	function event($event) {

		if($event == 'execute'){
			$this->executeQuery($_REQUEST['input_query']);
			return;
		}
	}

	/**
	 * Executes a query agains the DB
	 * @param unknown $query
	 */
	function executeQuery($query) {
		$connectionManager = new ConnectionManager();
		$connection = $connectionManager->getConnection();

		$result = mysqli_query($connection, $query);

		echo "<div><b>$query</b></div>";
		if(!$result)
		{
			echo mysqli_error($connection);
		}
		else
		{
			echo "Query: OK";
			$this->showQueryResult($result);
				
		}
	}


	function showQueryResult($result){
		$printTitles = true;
		
		echo "<div id='bo_query_count'>" . mysqli_num_rows($result) . " resultados </div>";
		echo "<div id='bo_query_resutls'>";
		
		while(($arr = mysqli_fetch_assoc($result)) != false){
			
			if($printTitles){
				echo "<div class='bo_query_row'>";
					foreach ($arr as $key => $value) {
						echo "<div class='bo_query_title'>$key</div>";
					}
				echo "</div>";
				$printTitles = false;
			}
			
			echo "<div class='bo_query_row'>";
				foreach ($arr as $key => $value) {
					echo "<div class='bo_query_data'>$value</div>";
				}
			echo "</div>";
			
		}
		echo "</div>";
	}


}

?>






