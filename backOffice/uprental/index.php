<?php
 
$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'global.php';
include_once $path . 'backOffice/header.tpl.php';
include_once $path . 'controllers/database/connectionManager.php';
include_once $path . 'controllers/server/tools.php';

function getLink(){
	$manager = new ConnectionManager();
	return $manager->getConnection();
}

function showProvinces(){
	$query = "SELECT * FROM `province` ORDER BY  `province`.`province_name` ASC";
	$result = mysqli_query(getLink(), $query);
	
	printOptions($result, "province_name", "province_id", "NONE", "Rio Negro");
}




function showCities(){
	$query = "SELECT * FROM `city` ORDER BY `city`.`city_name` ASC";
	$result = mysqli_query(getLink(), $query);
	
	printOptions($result, 'city_name', 'city_id', 'NONE', "Bariloche");
}

?>


<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
	<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
	
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnsb9yCAMRHQt6gjWbbUwiAQO9d1BSWVY&sensor=false"></script>
	<script src="/controllers/user/MapController.js"></script>
	
	<link rel="stylesheet" type="text/css" href="/backOffice/css/header.css" />
	<link rel="stylesheet" type="text/css" href="/css/header.css" />
	<link rel="stylesheet" type="text/css" href="/css/search.css" />
	<link rel="stylesheet" type="text/css" href="/css/editItem.css" />
	
</head>


<body>


	<div id="uprental_form"><br>
	<form id="uprental_form" action="/backOffice/uprental/uprentalController.php?event=registerNewRental" method="POST" target="new_rental"><br>

		<div style="text-align: center;">Rental Data</div>
		<br><br>
		
		<label for="rental_type">Personal Rental</label>
		<input style="width: 20px" name="rental_type" type="radio" value="<?php echo PERSONAL_RENTAL_USER?>"><br>
		
		<label for="rental_type">Business Rental</label>
		<input style="width: 20px" name="rental_type" type="radio" value="<?php echo BUSINESS_RENTAL_USER?>" checked><br>
		<br>
		<br>
		
		<label for="rental_cat_1">Category 1</label>
		<select name="rental_cat_1">
			<?php printCats('Bicicletas', getLink())?>
		</select>
		<br>
		
		<label for="rental_cat_2">Category 2</label>
		<select name="rental_cat_2">
			<?php printCats('Eventos', getLink())?>
		</select>
		<br>
		
		<br><br>
		
		<label  for="rental_name"><br>rental_name</label>
		<input  name="rental_name"><br>
		
		<label  for="rental_description"><br>rental_description</label>
		<textarea rows="5" cols="50" name="rental_description"></textarea>
		<br>
		
		<label  for="rental_hour"><br>rental_hour</label>
		<input  name="rental_hour"><br>
		
		<label  for="rental_website"><br>rental_website</label>
		<input  name="rental_website"><br>
	
	
		<br><br>
		<div style="text-align: center;">User Data</div>
		<br><br>
		
		<label  for="user_email"><br>user_email</label>
		<input  name="user_email"><br>
		
		<label  for="user_password"><br>user_password</label>
		<input  name="user_password"><br>

		<label  for="user_phone"><br>user_phone</label>
		<input  name="user_phone"><br>
		
		
		<br><br>
		<div style="text-align: center;">User Location</div>
		<br><br>
		
		<label for="city_id">City</label>
		<select id="city_id" name="city_id">
			<?php showCities(1)?>
		</select>
		<br>
		
		<label for="province_id">Provincia</label>
		<select id="province_id" name="province_id" onchange="updateCities()">
			<?php showProvinces()?>
		</select>
		<br>
		
		<script type="text/javascript">
			function codeNewAddress(){
				var street = document.getElementById("user_address").value;
				
				var citySelect = document.getElementById("city_id");
				var city_name = citySelect.options[citySelect.selectedIndex].text;

				var provinceSelect = document.getElementById("province_id");
				var province_name = provinceSelect.options[provinceSelect.selectedIndex].text;
				
				var address = street + "," + city_name + "," + province_name + ",Argentina";
				 
				codeAddress(address, "user");
			}
		</script>
		
		<label  for="user_address"><br>user_address</label>
		<input  id="user_address" name="user_address"> 
		<br>
		
		<div class="button_green" onclick="codeNewAddress()" style="margin-left: 300px;">Get GeoCode</div>
		
		<input id="user_geolocation" name="user_geolocation" hidden="hidden">
		
		<div class="item_map" id="map_canvas"></div>
		
		<br><br>
		
		<br><br>
		<input class="button_green" style="float: right;" type="submit" value="Mandar">
		<br><br>
	</form>
	</div>
	
</body>

</html>

