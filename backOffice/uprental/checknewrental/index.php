<?php
	$path = $_SERVER['DOCUMENT_ROOT'] . "/";
	include_once $path . 'controllers/server/tools.php'; 
	controlUserLogguedIn();
?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<title>Check New Rental</title>
<link rel="icon" type="image/png" href="/lib/images/favicon.png"/>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnsb9yCAMRHQt6gjWbbUwiAQO9d1BSWVY&sensor=false"></script>
<script src="/controllers/user/MapController.js"></script>

<link rel="stylesheet" type="text/css" href="/css/header.css" />
<link rel="stylesheet" type="text/css" href="/css/profile.css" />
<link rel="stylesheet" type="text/css" href="/css/editItem.css" />

<script src="/controllers/user/profile/profileViewController.js"></script>

</head>

<?php
//Base include
include_once $path . 'global.php';
include_once $path . 'languages/es.php';
include_once $path . 'controllers/server/tools.php';
include_once $path . 'backOffice/header.tpl.php';

include_once $path . 'controllers/server/location/locationController.php';
include_once $path . 'controllers/server/item/updateController.php';
include_once $path . 'controllers/server/profile/profileController.php';

include_once $path . 'templates/pages/rentalProfile.tpl.php';
include_once $path . 'templates/pages/profile.tpl.php';
?>














