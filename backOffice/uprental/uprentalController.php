<?php
if(!isset($_SESSION)){
	session_start();
}

$path = $_SERVER['DOCUMENT_ROOT'] . "/";
include_once $path . 'global.php';
include_once $path . 'controllers/database/connectionManager.php';
include_once $path . 'controllers/database/MailManager.php';
include_once $path . 'controllers/server/tools.php';

new UprentalController(@$_REQUEST['event']);

class UprentalController{

	function __construct($event){
		if($event == 'registerNewRental'){
			$this->registerNewRental();	
		}
		
	}	
	
	
	function registerNewRental(){
		$user_email = $_REQUEST['user_email'];
		$user_password = $_REQUEST['user_password'];
		$city_id = $_REQUEST['city_id'];
		$user_address = $_REQUEST['user_address'];
		$user_phone = $_REQUEST['user_phone'];
		
		$user_geolocation = $_REQUEST['user_geolocation'];
		$user_token = "done";
		
		$rental_name = $_REQUEST['rental_name'];
		$rental_description = cleanStringForDb($_REQUEST['rental_description']);
		$rental_hour = $_REQUEST['rental_hour'];
		$rental_website = $_REQUEST['rental_website'];

		//Insert the USER
		$query = "INSERT INTO `user`(`user_email`, `user_name`, `user_password`, `city_id`, `user_address`, `user_geolocation`, `user_phone`, `user_token`) 
							VALUES  ('$user_email', '$rental_name', '$user_password', '$city_id', '$user_address', '$user_geolocation', '$user_phone', '$user_token')";
		$user_id = $this->sendQuery($query);

		
		//Then insert the RENTAL
		$rental_type = $_REQUEST['rental_type'];
		$rental_confirmed = RENTAL_CONFIRMED;
				
		$query = "INSERT INTO `rental`(`user_id`, `rental_name`, `rental_description`, `rental_hour`, `rental_website`, `rental_type`, `rental_confirmed`) 
							   VALUES ('$user_id', '$rental_name', '$rental_description', '$rental_hour', '$rental_website', '$rental_type', '$rental_confirmed')";
		$rental_id = $this->sendQuery($query);
		

		//Then insert the RentalCategories 1 to 2
		for ($i = 1; $i < 3; $i++) {
			$cat = $_REQUEST['rental_cat_' . $i];
			$this->sendQuery("call sp_add_rentalCategory('$rental_id', '$cat')");
		}
		
		
		//Then redirect to the edit page
		$_SESSION['user_id'] = $user_id;
		$_SESSION['rental_id'] = $rental_id;
		
		header("location: /backOffice/uprental/checknewrental/");
	}
	
	
	function sendQuery($query){
		echo $query . "<br>";
		
		$linkManager = new ConnectionManager();
		$link = $linkManager->getConnection();

		$result = mysqli_query($link, $query);
		
		if(!$result){
			echo "<b>$query</b><br>" . mysqli_error($link);
			die();
		}
		
		return mysqli_insert_id($link);
	}	
	
	
}

?>


