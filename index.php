<?php
	if(isset($_REQUEST['token'])){
		header("location: /controllers/server/profile/loginController.php?event=token&user_id=" . $_REQUEST['user_id'] . "&user_token=" . $_REQUEST['token']);
		die();
	}
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
		
		<title>AlquiloLibre!</title>
		<link rel="icon" type="image/png" href="/lib/images/favicon.png?version=9.2" />
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
		<script src="/controllers/user/GeneralViewController.js?version=9.2"></script>
		
		
		<link rel="stylesheet" type="text/css" href="/css/header.css?version=9.2" />
		<link rel="stylesheet" type="text/css" href="/css/index.css?version=9.2" />
	</head>

	<body>
		<div id="main_body">
			<?php
			$path = $_SERVER['DOCUMENT_ROOT'] . "/";
			
			include_once $path.'global.php';
			include_once $path.'languages/es.php';
			include_once $path.'controllers/server/tools.php';
			
			include_once $path . 'templates/blocks/header.tpl.php';
			
			include_once $path.'controllers/server/indexController.php';
			include_once $path.'templates/pages/index.tpl.php';
			
			include_once $path.'templates/blocks/foot.tpl.php';
			
			
			?>
		</div>
	</body>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38798421-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</html>


