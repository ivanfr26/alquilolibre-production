<?php
$lang_search = "Buscar";
$lang_filters = "Herraminetas de busqueda";

$lang_login = "INICIAR SESION";
$lang_logout = "Cerrar sesi&oacute;n";

$lang_user = "Correo electronico";
$lang_password = "Contrase&ntilde;a";
$lang_confirm_password = "Confirma la contrase&ntildea";
$lang_email = "Correo electronico";
$lang_new_account = "Crear una cuenta nueva";
$lang_regsiter = "Crear una cuenta";
$lang_noCategory = "Sin categor&iacute;a";

$lang_save = "Guardar";
$lang_edit = "Editar";
$lang_cancel = "Cancelar";
$lang_saveImgs = "Guardar fotos";
$lang_upImgs = "Subir fotos";
$lang_change = "Cambiar";

$lang_name = "Nombre";
$lang_lastname = "Apellido";
$lang_birthday = "Fecha de nacimiento";
$lang_address = "Direcci&oacute;n";
$lang_geolocation = "Coordenadas";

$lang_account = "Mi cuenta";
$lang_profile = "Perfil";
$lang_rent_yours = "Alquila tus cosas";

$lang_yourItems = "Mis cosas en alquiler";

//Items
$lang_rent_prices = "Precios";
$lang_tags = "Caracter&iacute;sticas";
$lang_score = "Opini&oacute;n general";
$lang_Feedback = "Opiniones";
$lang_feedback = "opiniones";
$lang_qa = "Comentarios";

$lang_add_new_item = "Agregar una publicaci&oacute;n";
$lang_myItems = "Mis publicaciones";

$lang_item_edit = "Editar este item";
$lang_item_edit_finish = "Terminar edici&oacute;n";
$lang_item_name = "T&iacute;tulo";
$lang_item_description = "Descripci&oacute;n";
$lang_item_address = "Calle";
$lang_item_city = "Ciudad";
$lang_item_province = "Provincia";
$lang_item_country = "Pa&iacute;s";
$lang_item_conditions = "Condiciones de alquiler";
$lang_item_expirationDate = "Fecha limite del item";


$lang_item_hour = "Por hora";
$lang_item_day = "Por día";
$lang_item_week = "Por semana";
$lang_item_month = "Por mes";
$lang_consult = "Consultar";

$lang_item_weeklyPrice = "Precio semanal";
$lang_item_monthlyPrice = "Precio mensual";
$lang_item_create = "Agregar Item";
$lang_item_location = "Ubicaci&oacute;n";
$lang_rent_reputation = "Reputaci&oacuten del due&ntildeo";
$lang_owner_data = "Datos del due&ntilde;o";

$lang_home = "Home";
$lang_help = "AYUDA";

$lang_moreFeedback = "Ver m&aacute;s opiniones";
$lang_moreQuestions = "Ver m&aacute;s preguntas";
$lang_moreComments = "Ver m&aacute;s comentarios";
$lang_seeResponse = "Ver respuesta del due&ntilde;o";
$lang_writeQ = "Escribe tus comentarios o consultas...";
$lang_ask = "Enviar";
$lang_seeComments = "Ver comentarios";

$lang_categories = "Categor&iacute;as";
$lang_price = " Precio";

$lang_min = "Min.";
$lang_max = "Max.";

$lang_what = "Qu&eacute; buscas?";
$lang_where = "D&oacute;nde?";

$lang_dailyPrice = "por d&iacute;a";

$lang_noResults= "Que macana! No hay resultados para tu b&uacute;squeda.";
$lang_resultsLabel = "resultados";
$lang_aprox = "Cerca de";

$lang_references = "referencias";

$lang_logInToContinue = "Ingresa a tu cuenta para continuar, Gracias!";
$lang_loginIncorrect = "Los datos ingresados no son correctos.";

$lang_registerOk = "Bienvenido";
$lang_registerFail = "Error al crear tu cuenta, por favor, intenta de nuevo.";

$lang_cities = "Ciudades";
$lang_in = "en";
$lang_filteredBy = "Filtrado por";
$lang_scores = "Puntajes";
$lang_rentCount = "veces alquilado";

$lang_magic = "Magicamente";
$lang_mostExpensives = "Menor precio";
$lang_lessExpensives = "Mayor precio";
$lang_sortBy = "Ordenar por:";

$lang_upload = "Agregar";
$lang_imgs = "fotos";


$lang_help_itemTitle = "Usa palabras claves para mejorar la busqueda";
$lang_updateItem_describe = "Describe tu producto";
$lang_item_conditions_label = "Horario y plazos";
$lang_help_itemConditions = "Horarios para retirar y devolver, tiempo m&iacute;nimo o m&aacute;ximo de alquiler, etc...";
$lang_item_contact = "Datos de contacto";
$lang_help_itemAddress = "Ingresa solo la calle y el n&uacute;mero";

$lang_help_selecteCountryFirst = "Selecciona el Pa&iacute;s primero.";
$lang_help_selecteProvinceFirst	= "Selecciona la Provincia primero.";

$showCalifications = "Ver calificaciones";
$lang_moreFromOwner = "M&aacute;s publicaciones del due&ntilde;o";
		
$lang_reply = "Responder";
$lang_phone = "Tel&eacute;fono";


$lang_previous = "Anterior";
$lang_next = "Siguiente";

$lang_dataSavedOK = "Los datos fueron gurdados correctamente.";
$lang_dataSavedFail = "Error al guardar los datos.";

$lang_imgsSavedOK = "Las fotos fueron gurdadas correctamente.";
$lang_imgsSavedFail = "Ocurri&oacute; un error al guardar las fotos.";

$lang_intro_title = "En tres simples pasos:";
$lang_step_1 = "Crea una cuenta.";
$lang_step_2 = "Sube tus productos o servicios.";
$lang_step_3 = "Comienza a ganar dinero.";


$lang_question = Array();
$lang_response = Array(); 


$lang_question[1] = "¿Qu&eacute; es AlquiloLibre?";
$lang_response[1] = "<b>AlquiloLibre</b> es una web social que conecta a aquellas personas que buscan alg&uacute;n producto o servicio para alquilar, 
					con quienes lo ofrecen. A trav&eacute;s de sus experiencias en <b>AlquiloLibre</b>, 
					due&ntilde;os y usuarios crean conexiones reales bajo una cultura de consumo colaborativo.";
                
$lang_question[3] = "¿Qui&eacute;n publica los productos?";
$lang_response[3] = "Algunos son comercios especializados, mientras que otros productos son publicados por personas independientes.";
                
$lang_question[4] = "¿Qu&eacute; medios de pagos aceptan?";
$lang_response[4] = "<b>AlquiloLibre</b> ofrece una gran variedad de medios de pagos, pero depende de cada due&ntilde;o, algunos aceptan solo efectivo mientras 
					que otros aceptan medios de pago electr&oacute;nicos.";
                
$lang_question[5] = "¿C&oacute;mo se realiza la entrega y devoluci&oacute;n de los productos?";
$lang_response[5] = "Cada due&ntilde;o arregla la entrega y devoluci&oacute;n con sus clientes.";
                
$lang_question[6] = "¿Qu&eacute; pasa si me rompen o roban mis productos?";
$lang_response[6] = "Estamos analizando diferentes opciones de seguros contra robo y roturas que estar&aacute;n disponibles a la brevedad.";
                
$lang_question[7] = "¿Qu&eacute; beneficios tengo si publico mis cosas para alquilar?";
$lang_response[7] = "El principal beneficio es que tu cosas trabajan por t&iacute;, se promocionan las 24hs y tus clientes pueden consultarte los precios 
					y disponibilidad de tus productos desde cualquier lugar. 
					<b>AlquiloLibre</b> es una gran plataforma para administrar tus alquileres y conectar con tus clientes.";


$lang_alwaysFree = "Publicar tus productos o servicios es gratis.";

$lang_newitemOk = "Publicacion creadada con exito!";


$lang_map = "Mapa";

$lang_myitems_Acitve = "Mis publicaciones activas";
$lang_myitems_NoAcitve = "Mis publicaciones no activas";

$lang_delete = "Eliminar";
$lang_no_desc = "Estas publicaciones no son visibles para ning&uacute;n usuario";

$lang_verif = "Verificaciones";
$lang_basicInfo = "Informaci&oacute;n b&aacute;sica";


$lang_loginFacebook = "Facebook";

$lang_index_moto = "Alquila de todo, en todos lados.";

?>
